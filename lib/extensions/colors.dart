import 'package:flutter/material.dart';

const Color testResultColorsHonest = Color(0xff01C7FC);
const Color testResultColorsCalm = Color(0xff982ABD);
const Color testResultColorsAmbivert = Color(0xff99244F);
const Color testResultColorsIndependent = Color(0xff242424);
const Color testResultColorsPlayful = Color(0xff5E30EB);
const Color testResultColorsCreative = Color(0xff38571A);
const Color whiteType = Color(0xffFFFFFF);
const Color primaryColor = Color(0xFF9B8AFE);
