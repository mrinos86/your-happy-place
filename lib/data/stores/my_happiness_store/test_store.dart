import 'dart:convert';

import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:happy_place/api_utils/api_parser.dart';
import 'package:happy_place/api_utils/api_provider.dart';
import 'package:happy_place/extensions/double_extensions.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/models/big_five_request.dart';
import 'package:injectable/injectable.dart';
import 'package:mobx/mobx.dart';

part 'test_store.g.dart';

@injectable
class TestStore = _TestStore with _$TestStore;

abstract class _TestStore with Store {
  final ApiProvider apiProvider;

  _TestStore(this.apiProvider);

  String? errorMessage;
  String? profileImagePath;
  PageController? pageController = PageController();

  @observable
  int pageViewPage = 1;

  @observable
  bool loading = false;

  @observable
  String message = "";

  @observable
  String status = '';

  @observable
  int error = 1;

  @observable
  List<Question> listQuestions = [];

  @observable
  List<Answer> listAnswer = [];

  @observable
  Question? selectedQuestion;

  @observable
  int pageMode = 0; // 0 for new layout, 1 for test layout

  @observable
  int testId = 1; // 1 for Big Five, 2 for HEXACO, 3 for Make a Happiness Entry

  @observable
  List<BigFiveAnswerRequest> listBigFiveAnswers = [];

  @observable
  List<BigFiveTestResult> listBigFiveTestResult = [];

  @observable
  List<HexacoTestResult> listHexacoTestResult = [];

  @observable
  List<HappinessTestResult> listHappinessTestResult = [];

  @observable
  List<HappinessTestResult> listHappinessCategoryTestResult = [];

  init() async {
    // await getPosts();
  }

  @action
  nextPageViewPage() {
    if (pageViewPage < listQuestions.length) {
      pageViewPage++;
      pageController?.nextPage(
        duration: const Duration(milliseconds: 100),
        curve: Curves.linear,
      );
    }
  }

  @action
  previousPageViewPage() {
    if (pageViewPage > 1) {
      pageViewPage--;
      pageController?.previousPage(
        duration: const Duration(milliseconds: 100),
        curve: Curves.linear,
      );
    }
  }

  @action
  getTestQuestions(HappynessTest test) async {
    try {
      loading = true;
      testId = int.parse(test.id!);
      Response<HappynessTestsQuestionsResponse>? response =
          await apiProvider.apiClient.happynessTestsIdQuestionsGet(id: test.id);

      if (loading) loading = false;
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        listQuestions = ApiSuccessParser.payloadOrThrow(response);
        for (var q in listQuestions) {
          debugPrint('main question answer: ${q.answer?.toJson()}');
          for (var s in q.subQuestions!) {
            debugPrint('sub question answer: ${s.answer?.toJson()}');
          }
        }
        if (testId == 1) {
          // it means the test is big five or make a happiness entry. so we get the answers list ready.
          for (int i = 0; i < listQuestions.length; i++) {
            if (listQuestions[i].subQuestions != null) {
              for (int j = 0; j < listQuestions[i].subQuestions!.length; j++) {
                listBigFiveAnswers.add(
                  BigFiveAnswerRequest(
                    questionId: listQuestions[i].subQuestions![j].id!,
                    answer: '0',
                  ),
                );
              }
            }
          }
        } else if (testId == 2) {
          // hexaco test
          debugPrint(
              'sub questions length: ${listQuestions[0].subQuestions?.length}');
          if (listQuestions.isNotEmpty) {
            if (listQuestions[0].subQuestions != null) {
              for (int j = 0; j < listQuestions[0].subQuestions!.length; j++) {
                listBigFiveAnswers.add(
                  BigFiveAnswerRequest(
                    questionId: listQuestions[0].subQuestions![j].id!,
                    answer: '1',
                  ),
                );
              }
            }
          }
        }
        debugPrint('questions length: ${listQuestions.length}');
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (loading) loading = false;
      debugPrint('getTestDetails exception caught : $e');
      rethrow;
    }
  }

  @action
  getHappinessTestQuestions(HappynessTest test) async {
    try {
      loading = true;
      testId = int.parse(test.id!);
      Response<HappynessTestsQuestionsResponse>? response =
          await apiProvider.apiClient.happynessTestsIdQuestionsGet(id: test.id);

      if (loading) loading = false;
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        listQuestions = ApiSuccessParser.payloadOrThrow(response);
        for (int i = 0; i < listQuestions.length; i++) {
          if (listQuestions[i].subQuestions != null) {
            for (int j = 0; j < listQuestions[i].subQuestions!.length; j++) {
              listBigFiveAnswers.add(
                BigFiveAnswerRequest(
                  questionId: listQuestions[i].subQuestions![j].id!,
                  answer: '0',
                ),
              );
            }
          }
        }
        updateHappinessLatestResultAnswer(); //updating happiness latest answer
        debugPrint('happiness entry questions length: ${listQuestions.length}');
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (loading) loading = false;
      debugPrint('getHappinessEntryTestDetails exception caught : $e');
      rethrow;
    }
  }

  @action
  getTestAvailabilityStatus(String title) async {
    try {
      loading = true;
      var response = await apiProvider.apiClient
          .happynessTestsTestAvailableGet(testTitle: title);

      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        if (loading) loading = false;
        status = response.body!.message!;
      } else {
        if (loading) loading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (loading) loading = false;
      debugPrint('getTestAvailabilityStatus exception caught : $e');
      rethrow;
    }
  }

  @action
  getBigFiveTestResult() async {
    try {
      loading = true;
      Response<HappynessTestsGetBigFiveTestResultsResponse>? response =
          await apiProvider.apiClient.happynessTestsBigfiveResultsGet();

      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        if (loading) loading = false;
        listBigFiveTestResult = ApiSuccessParser.payloadOrThrow(response);
      } else {
        if (loading) loading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (loading) loading = false;
      debugPrint('getBigFiveTestResult exception caught : $e');
      rethrow;
    }
  }

  @action
  getHexacoTestResult() async {
    try {
      loading = true;
      Response<HappynessTestsGetHEXACOTestResultsResponse>? response =
          await apiProvider.apiClient.happynessTestsHexacoResultsGet();

      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        if (loading) loading = false;
        listHexacoTestResult = ApiSuccessParser.payloadOrThrow(response);
      } else {
        if (loading) loading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (loading) loading = false;
      debugPrint('getHEXATestResult exception caught : $e');
      rethrow;
    }
  }

  @action
  getHappinessTestResult() async {
    try {
      loading = true;
      Response<HappynessTestsGetHappinessTestResultsResponse>? response =
          await apiProvider.apiClient.happynessTestsHappinessResultsGet();

      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        if (loading) loading = false;
        listHappinessTestResult = ApiSuccessParser.payloadOrThrow(response);
      } else {
        if (loading) loading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (loading) loading = false;
      debugPrint('getHappinessTestResult exception caught : $e');
      rethrow;
    }
  }

  @action
  getHappinessCategoryTestResult(String title) async {
    try {
      loading = true;
      Response<HappynessTestsGetHappinessResultsByTitleResponse>? response =
          await apiProvider.apiClient
              .happynessTestsHappinessResultsTitleGet(title: title);

      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        if (loading) loading = false;
        listHappinessCategoryTestResult =
            ApiSuccessParser.payloadOrThrow(response);
      } else {
        if (loading) loading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (loading) loading = false;
      debugPrint('getHappinessCategoryTestResult exception caught : $e');
      rethrow;
    }
  }

  @action
  getHappynessTestLatestResult(String type) async {
    try {
      loading = true;
      Response<HappynessTestsGetHappinessTestLatestAnswersResponse>? response =
          await apiProvider.apiClient
              .happynessTestsHappinessLatestAnswersTitleGet(title: type);

      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        listAnswer = ApiSuccessParser.payloadOrThrow(response);

        updateHappinessLatestResultAnswer(); //updating happiness latest answer
        if (loading) loading = false;
      } else {
        if (loading) loading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (loading) loading = false;
      debugPrint('getTestDetails exception caught : $e');
      rethrow;
    }
  }

  @action
  submitTest() async {
    try {
      var json = jsonEncode(listBigFiveAnswers.map((e) => e.toJson()).toList());
      debugPrint('answers list of big five: $json');

      loading = true;
      var response =
          await apiProvider.apiClient.answersPost(questionAndAnswers: json);

      if (loading) loading = false;
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        message = response.body!.message.toString();
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
        message = response.body!.message.toString();
      }
    } catch (e) {
      if (loading) loading = false;
      debugPrint('submitTest exception caught : $e');
      rethrow;
    }
  }

  @action
  updateBigFiveAnswerValue(SubQuestion subQuestion, double value) {
    debugPrint('updating value $value');
    listBigFiveAnswers
        .firstWhere((answer) => answer.questionId == subQuestion.id)
        .answer = value.toPrecision(2).toString();
    listBigFiveAnswers = listBigFiveAnswers;
  }

  updateHappinessLatestResultAnswer() {
    loading = true;

    for (int i = 0; i < listAnswer.length; i++) {
      for (var element in listBigFiveAnswers) {
        if (element.questionId == listAnswer[i].questionId) {
          listBigFiveAnswers.removeWhere(
              (element) => element.questionId == listAnswer[i].questionId);
          listBigFiveAnswers.add(BigFiveAnswerRequest(
              questionId: listAnswer[i].questionId!,
              answer: listAnswer[i].answer!.toString()));
        }
      }
    }
    if (loading) loading = false;
  }
}
