// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$TestStore on _TestStore, Store {
  final _$pageViewPageAtom = Atom(name: '_TestStore.pageViewPage');

  @override
  int get pageViewPage {
    _$pageViewPageAtom.reportRead();
    return super.pageViewPage;
  }

  @override
  set pageViewPage(int value) {
    _$pageViewPageAtom.reportWrite(value, super.pageViewPage, () {
      super.pageViewPage = value;
    });
  }

  final _$loadingAtom = Atom(name: '_TestStore.loading');

  @override
  bool get loading {
    _$loadingAtom.reportRead();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.reportWrite(value, super.loading, () {
      super.loading = value;
    });
  }

  final _$messageAtom = Atom(name: '_TestStore.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$statusAtom = Atom(name: '_TestStore.status');

  @override
  String get status {
    _$statusAtom.reportRead();
    return super.status;
  }

  @override
  set status(String value) {
    _$statusAtom.reportWrite(value, super.status, () {
      super.status = value;
    });
  }

  final _$errorAtom = Atom(name: '_TestStore.error');

  @override
  int get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(int value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$listQuestionsAtom = Atom(name: '_TestStore.listQuestions');

  @override
  List<Question> get listQuestions {
    _$listQuestionsAtom.reportRead();
    return super.listQuestions;
  }

  @override
  set listQuestions(List<Question> value) {
    _$listQuestionsAtom.reportWrite(value, super.listQuestions, () {
      super.listQuestions = value;
    });
  }

  final _$listAnswerAtom = Atom(name: '_TestStore.listAnswer');

  @override
  List<Answer> get listAnswer {
    _$listAnswerAtom.reportRead();
    return super.listAnswer;
  }

  @override
  set listAnswer(List<Answer> value) {
    _$listAnswerAtom.reportWrite(value, super.listAnswer, () {
      super.listAnswer = value;
    });
  }

  final _$selectedQuestionAtom = Atom(name: '_TestStore.selectedQuestion');

  @override
  Question? get selectedQuestion {
    _$selectedQuestionAtom.reportRead();
    return super.selectedQuestion;
  }

  @override
  set selectedQuestion(Question? value) {
    _$selectedQuestionAtom.reportWrite(value, super.selectedQuestion, () {
      super.selectedQuestion = value;
    });
  }

  final _$pageModeAtom = Atom(name: '_TestStore.pageMode');

  @override
  int get pageMode {
    _$pageModeAtom.reportRead();
    return super.pageMode;
  }

  @override
  set pageMode(int value) {
    _$pageModeAtom.reportWrite(value, super.pageMode, () {
      super.pageMode = value;
    });
  }

  final _$testIdAtom = Atom(name: '_TestStore.testId');

  @override
  int get testId {
    _$testIdAtom.reportRead();
    return super.testId;
  }

  @override
  set testId(int value) {
    _$testIdAtom.reportWrite(value, super.testId, () {
      super.testId = value;
    });
  }

  final _$listBigFiveAnswersAtom = Atom(name: '_TestStore.listBigFiveAnswers');

  @override
  List<BigFiveAnswerRequest> get listBigFiveAnswers {
    _$listBigFiveAnswersAtom.reportRead();
    return super.listBigFiveAnswers;
  }

  @override
  set listBigFiveAnswers(List<BigFiveAnswerRequest> value) {
    _$listBigFiveAnswersAtom.reportWrite(value, super.listBigFiveAnswers, () {
      super.listBigFiveAnswers = value;
    });
  }

  final _$listBigFiveTestResultAtom =
      Atom(name: '_TestStore.listBigFiveTestResult');

  @override
  List<BigFiveTestResult> get listBigFiveTestResult {
    _$listBigFiveTestResultAtom.reportRead();
    return super.listBigFiveTestResult;
  }

  @override
  set listBigFiveTestResult(List<BigFiveTestResult> value) {
    _$listBigFiveTestResultAtom.reportWrite(value, super.listBigFiveTestResult,
        () {
      super.listBigFiveTestResult = value;
    });
  }

  final _$listHexacoTestResultAtom =
      Atom(name: '_TestStore.listHexacoTestResult');

  @override
  List<HexacoTestResult> get listHexacoTestResult {
    _$listHexacoTestResultAtom.reportRead();
    return super.listHexacoTestResult;
  }

  @override
  set listHexacoTestResult(List<HexacoTestResult> value) {
    _$listHexacoTestResultAtom.reportWrite(value, super.listHexacoTestResult,
        () {
      super.listHexacoTestResult = value;
    });
  }

  final _$listHappinessTestResultAtom =
      Atom(name: '_TestStore.listHappinessTestResult');

  @override
  List<HappinessTestResult> get listHappinessTestResult {
    _$listHappinessTestResultAtom.reportRead();
    return super.listHappinessTestResult;
  }

  @override
  set listHappinessTestResult(List<HappinessTestResult> value) {
    _$listHappinessTestResultAtom
        .reportWrite(value, super.listHappinessTestResult, () {
      super.listHappinessTestResult = value;
    });
  }

  final _$listHappinessCategoryTestResultAtom =
      Atom(name: '_TestStore.listHappinessCategoryTestResult');

  @override
  List<HappinessTestResult> get listHappinessCategoryTestResult {
    _$listHappinessCategoryTestResultAtom.reportRead();
    return super.listHappinessCategoryTestResult;
  }

  @override
  set listHappinessCategoryTestResult(List<HappinessTestResult> value) {
    _$listHappinessCategoryTestResultAtom
        .reportWrite(value, super.listHappinessCategoryTestResult, () {
      super.listHappinessCategoryTestResult = value;
    });
  }

  final _$getTestQuestionsAsyncAction =
      AsyncAction('_TestStore.getTestQuestions');

  @override
  Future getTestQuestions(HappynessTest test) {
    return _$getTestQuestionsAsyncAction
        .run(() => super.getTestQuestions(test));
  }

  final _$getHappinessTestQuestionsAsyncAction =
      AsyncAction('_TestStore.getHappinessTestQuestions');

  @override
  Future getHappinessTestQuestions(HappynessTest test) {
    return _$getHappinessTestQuestionsAsyncAction
        .run(() => super.getHappinessTestQuestions(test));
  }

  final _$getTestAvailabilityStatusAsyncAction =
      AsyncAction('_TestStore.getTestAvailabilityStatus');

  @override
  Future getTestAvailabilityStatus(String title) {
    return _$getTestAvailabilityStatusAsyncAction
        .run(() => super.getTestAvailabilityStatus(title));
  }

  final _$getBigFiveTestResultAsyncAction =
      AsyncAction('_TestStore.getBigFiveTestResult');

  @override
  Future getBigFiveTestResult() {
    return _$getBigFiveTestResultAsyncAction
        .run(() => super.getBigFiveTestResult());
  }

  final _$getHexacoTestResultAsyncAction =
      AsyncAction('_TestStore.getHexacoTestResult');

  @override
  Future getHexacoTestResult() {
    return _$getHexacoTestResultAsyncAction
        .run(() => super.getHexacoTestResult());
  }

  final _$getHappinessTestResultAsyncAction =
      AsyncAction('_TestStore.getHappinessTestResult');

  @override
  Future getHappinessTestResult() {
    return _$getHappinessTestResultAsyncAction
        .run(() => super.getHappinessTestResult());
  }

  final _$getHappinessCategoryTestResultAsyncAction =
      AsyncAction('_TestStore.getHappinessCategoryTestResult');

  @override
  Future getHappinessCategoryTestResult(String title) {
    return _$getHappinessCategoryTestResultAsyncAction
        .run(() => super.getHappinessCategoryTestResult(title));
  }

  final _$getHappynessTestLatestResultAsyncAction =
      AsyncAction('_TestStore.getHappynessTestLatestResult');

  @override
  Future getHappynessTestLatestResult(String type) {
    return _$getHappynessTestLatestResultAsyncAction
        .run(() => super.getHappynessTestLatestResult(type));
  }

  final _$submitTestAsyncAction = AsyncAction('_TestStore.submitTest');

  @override
  Future submitTest() {
    return _$submitTestAsyncAction.run(() => super.submitTest());
  }

  final _$_TestStoreActionController = ActionController(name: '_TestStore');

  @override
  dynamic nextPageViewPage() {
    final _$actionInfo = _$_TestStoreActionController.startAction(
        name: '_TestStore.nextPageViewPage');
    try {
      return super.nextPageViewPage();
    } finally {
      _$_TestStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic previousPageViewPage() {
    final _$actionInfo = _$_TestStoreActionController.startAction(
        name: '_TestStore.previousPageViewPage');
    try {
      return super.previousPageViewPage();
    } finally {
      _$_TestStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic updateBigFiveAnswerValue(SubQuestion subQuestion, double value) {
    final _$actionInfo = _$_TestStoreActionController.startAction(
        name: '_TestStore.updateBigFiveAnswerValue');
    try {
      return super.updateBigFiveAnswerValue(subQuestion, value);
    } finally {
      _$_TestStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
pageViewPage: ${pageViewPage},
loading: ${loading},
message: ${message},
status: ${status},
error: ${error},
listQuestions: ${listQuestions},
listAnswer: ${listAnswer},
selectedQuestion: ${selectedQuestion},
pageMode: ${pageMode},
testId: ${testId},
listBigFiveAnswers: ${listBigFiveAnswers},
listBigFiveTestResult: ${listBigFiveTestResult},
listHexacoTestResult: ${listHexacoTestResult},
listHappinessTestResult: ${listHappinessTestResult},
listHappinessCategoryTestResult: ${listHappinessCategoryTestResult}
    ''';
  }
}
