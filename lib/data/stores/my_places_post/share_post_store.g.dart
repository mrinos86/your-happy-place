// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'share_post_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$SharePostStore on _SharePostStore, Store {
  final _$friendOrFamilyAtom = Atom(name: '_SharePostStore.friendOrFamily');

  @override
  User? get friendOrFamily {
    _$friendOrFamilyAtom.reportRead();
    return super.friendOrFamily;
  }

  @override
  set friendOrFamily(User? value) {
    _$friendOrFamilyAtom.reportWrite(value, super.friendOrFamily, () {
      super.friendOrFamily = value;
    });
  }

  final _$sharingGroupAtom = Atom(name: '_SharePostStore.sharingGroup');

  @override
  Group? get sharingGroup {
    _$sharingGroupAtom.reportRead();
    return super.sharingGroup;
  }

  @override
  set sharingGroup(Group? value) {
    _$sharingGroupAtom.reportWrite(value, super.sharingGroup, () {
      super.sharingGroup = value;
    });
  }

  final _$shareTypeAtom = Atom(name: '_SharePostStore.shareType');

  @override
  ShareTabType get shareType {
    _$shareTypeAtom.reportRead();
    return super.shareType;
  }

  @override
  set shareType(ShareTabType value) {
    _$shareTypeAtom.reportWrite(value, super.shareType, () {
      super.shareType = value;
    });
  }

  final _$postSharedAtom = Atom(name: '_SharePostStore.postShared');

  @override
  bool get postShared {
    _$postSharedAtom.reportRead();
    return super.postShared;
  }

  @override
  set postShared(bool value) {
    _$postSharedAtom.reportWrite(value, super.postShared, () {
      super.postShared = value;
    });
  }

  final _$isLoadingAtom = Atom(name: '_SharePostStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$sharePostAsyncAction = AsyncAction('_SharePostStore.sharePost');

  @override
  Future sharePost() {
    return _$sharePostAsyncAction.run(() => super.sharePost());
  }

  final _$sharePostWithGroupAsyncAction =
      AsyncAction('_SharePostStore.sharePostWithGroup');

  @override
  Future sharePostWithGroup() {
    return _$sharePostWithGroupAsyncAction
        .run(() => super.sharePostWithGroup());
  }

  final _$_SharePostStoreActionController =
      ActionController(name: '_SharePostStore');

  @override
  dynamic setPost(Entry p) {
    final _$actionInfo = _$_SharePostStoreActionController.startAction(
        name: '_SharePostStore.setPost');
    try {
      return super.setPost(p);
    } finally {
      _$_SharePostStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setFriendOrFamilyUser(User u) {
    final _$actionInfo = _$_SharePostStoreActionController.startAction(
        name: '_SharePostStore.setFriendOrFamilyUser');
    try {
      return super.setFriendOrFamilyUser(u);
    } finally {
      _$_SharePostStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setGroupforShare(Group u) {
    final _$actionInfo = _$_SharePostStoreActionController.startAction(
        name: '_SharePostStore.setGroupforShare');
    try {
      return super.setGroupforShare(u);
    } finally {
      _$_SharePostStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
friendOrFamily: ${friendOrFamily},
sharingGroup: ${sharingGroup},
shareType: ${shareType},
postShared: ${postShared},
isLoading: ${isLoading}
    ''';
  }
}
