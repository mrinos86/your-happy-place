// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'view_comment_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ViewCommentStore on _ViewCommentStore, Store {
  final _$isLoadingAtom = Atom(name: '_ViewCommentStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$commentPostedAtom = Atom(name: '_ViewCommentStore.commentPosted');

  @override
  bool get commentPosted {
    _$commentPostedAtom.reportRead();
    return super.commentPosted;
  }

  @override
  set commentPosted(bool value) {
    _$commentPostedAtom.reportWrite(value, super.commentPosted, () {
      super.commentPosted = value;
    });
  }

  final _$reportedAtom = Atom(name: '_ViewCommentStore.reported');

  @override
  bool get reported {
    _$reportedAtom.reportRead();
    return super.reported;
  }

  @override
  set reported(bool value) {
    _$reportedAtom.reportWrite(value, super.reported, () {
      super.reported = value;
    });
  }

  final _$commentListAtom = Atom(name: '_ViewCommentStore.commentList');

  @override
  List<Comment> get commentList {
    _$commentListAtom.reportRead();
    return super.commentList;
  }

  @override
  set commentList(List<Comment> value) {
    _$commentListAtom.reportWrite(value, super.commentList, () {
      super.commentList = value;
    });
  }

  final _$getPostCommentsAsyncAction =
      AsyncAction('_ViewCommentStore.getPostComments');

  @override
  Future getPostComments({required int postId}) {
    return _$getPostCommentsAsyncAction
        .run(() => super.getPostComments(postId: postId));
  }

  final _$commentPostingAsyncAction =
      AsyncAction('_ViewCommentStore.commentPosting');

  @override
  Future commentPosting({required int postId, required String comment}) {
    return _$commentPostingAsyncAction
        .run(() => super.commentPosting(postId: postId, comment: comment));
  }

  final _$reportCommentedUserAsyncAction =
      AsyncAction('_ViewCommentStore.reportCommentedUser');

  @override
  Future reportCommentedUser(
      {required int userId,
      String? reason,
      String? message,
      required int commentId}) {
    return _$reportCommentedUserAsyncAction.run(() => super.reportCommentedUser(
        userId: userId,
        reason: reason,
        message: message,
        commentId: commentId));
  }

  final _$reportCommentAsyncAction =
      AsyncAction('_ViewCommentStore.reportComment');

  @override
  Future<bool> reportComment({required int commentId}) {
    return _$reportCommentAsyncAction
        .run(() => super.reportComment(commentId: commentId));
  }

  @override
  String toString() {
    return '''
isLoading: ${isLoading},
commentPosted: ${commentPosted},
reported: ${reported},
commentList: ${commentList}
    ''';
  }
}
