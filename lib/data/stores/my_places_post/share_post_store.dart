import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:happy_place/api_utils/api_parser.dart';
import 'package:happy_place/api_utils/api_provider.dart';
import 'package:happy_place/api_utils/http_exception.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/utils/enums.dart';

import 'package:injectable/injectable.dart';
import 'package:mobx/mobx.dart';

part 'share_post_store.g.dart';

@injectable
class SharePostStore = _SharePostStore with _$SharePostStore;

abstract class _SharePostStore with Store {
  final ApiProvider apiProvider;
  _SharePostStore({
    required this.apiProvider,
  });

  Entry? post;
  TextEditingController postCaptionTextEditingController =
      TextEditingController();

  // String? message;

  @observable
  User? friendOrFamily;

  @observable
  Group? sharingGroup;

  @observable
  ShareTabType shareType = ShareTabType.friends;

  @observable
  bool postShared = false;

  @observable
  bool isLoading = false;

  @action
  setPost(Entry p) {
    post = p;
  }

  @action
  setFriendOrFamilyUser(User u) {
    friendOrFamily = u;
  }

  @action
  setGroupforShare(Group u) {
    sharingGroup = u;
  }

  @action
  sharePost() async {
    try {
      isLoading = true;
      Response<EntriesShareAPostsResponse> response =
          await apiProvider.apiClient.entriesSharePost(
              sharedTo: int.parse(friendOrFamily!.id!),
              postId: int.parse(post!.id!),
              description: postCaptionTextEditingController.text);
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        if (isLoading == true) isLoading = false;

        postShared = true;
        HttpExceptionNotifyUser.showMessage(response.body!.message.toString());
      } else {
        if (isLoading == true) isLoading = false;

        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (isLoading == true) isLoading = false;

      debugPrint('sharePost exception: $e');
      rethrow;
    }
  }

  @action
  sharePostWithGroup() async {
    try {
      isLoading = true;
      Response<SuccessResponse> response = await apiProvider.apiClient
          .entriesShareInGroupPost(
              groupId: int.parse(sharingGroup!.id!),
              postId: int.parse(post!.id!),
              description: postCaptionTextEditingController.text);
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        if (isLoading == true) isLoading = false;
        // message = response.body!.message;
        postShared = true;
        HttpExceptionNotifyUser.showMessage(response.body!.message.toString());
      } else {
        if (isLoading == true) isLoading = false;

        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (isLoading == true) isLoading = false;

      debugPrint('sharePost exception: $e');
      rethrow;
    }
  }
}
