import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:happy_place/api_utils/api_parser.dart';
import 'package:happy_place/api_utils/api_provider.dart';
import 'package:happy_place/api_utils/http_exception.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';

import 'package:injectable/injectable.dart';
import 'package:mobx/mobx.dart';

part 'view_comment_store.g.dart';

@injectable
class ViewCommentStore = _ViewCommentStore with _$ViewCommentStore;

abstract class _ViewCommentStore with Store {
  final ApiProvider apiProvider;
  _ViewCommentStore({
    required this.apiProvider,
  });

  @observable
  bool isLoading = false;

  @observable
  bool commentPosted = false;

  @observable
  bool reported = false;

  @observable
  List<Comment> commentList = [];

  @action
  getPostComments({required int postId}) async {
    isLoading = true;
    try {
      commentList.clear();
      Response<CommentsAllCommentsByPostIdResponse> response =
          await apiProvider.apiClient.commentsPostIdGet(postId: postId);
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        if (isLoading == true) isLoading = false;
        commentList = ApiSuccessParser.payloadOrThrow(response);
        debugPrint('comment length: ${commentList.length}');
      } else {
        if (isLoading == true) isLoading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (isLoading == true) isLoading = false;
      debugPrint(e.toString());
      rethrow;
    }
  }

  @action
  commentPosting({required int postId, required String comment}) async {
    try {
      isLoading = true;
      commentPosted = false;
      Response<CommentsStoreResponse> response = await apiProvider.apiClient
          .commentsStorePost(postId: postId, comment: comment);
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        if (isLoading == true) isLoading = false;
        commentPosted = true;
      } else {
        if (isLoading == true) isLoading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (isLoading == true) isLoading = false;
      debugPrint('sharePost exception: $e');
      rethrow;
    }
  }

  @action
  reportCommentedUser(
      {required int userId,
      String? reason,
      String? message,
      required int commentId}) async {
    isLoading = true;
    reported = false;
    try {
      Response<SuccessResponse> response = await apiProvider.apiClient
          .usersReportPost(
              reportedUserId: userId, reason: reason, message: message);
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        bool result =
            await reportComment(commentId: commentId); // hiding comment
        if (result) {
          if (isLoading == true) isLoading = false;
          HttpExceptionNotifyUser.showMessage("Comment reported");
          reported = true;
        } else {
          if (isLoading == true) isLoading = false;
          HttpExceptionNotifyUser.showError("Something went wrong, Try again");
        }
      } else {
        if (isLoading == true) isLoading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (isLoading == true) isLoading = false;
      debugPrint('report comment exception: $e');
      rethrow;
    }
  }

  @action
  Future<bool> reportComment({required int commentId}) async {
    try {
      Response<CommentsReportResponse> response =
          await apiProvider.apiClient.commentsReportIdPost(id: commentId);
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        debugPrint(' comment hidden');
        return true;
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
        return false;
      }
    } catch (e) {
      debugPrint('report comment exception: $e');
      return false;
    }
  }
}
