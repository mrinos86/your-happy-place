import 'dart:convert';

import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_place/google_place.dart';
import 'package:happy_place/api_utils/api_parser.dart';
import 'package:happy_place/api_utils/api_provider.dart';
import 'package:happy_place/api_utils/http_exception.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/models/happy_place.dart';
import 'package:happy_place/ui/add_new_happy_place/add_new_happy_place.dart';
import 'package:image_picker/image_picker.dart';
import 'package:injectable/injectable.dart';
import 'package:geolocator/geolocator.dart' as geo_loc;
import 'package:intl/intl.dart';
import 'package:mobx/mobx.dart';
import 'package:http/http.dart' as http;

part 'add_place_store.g.dart';

@injectable
class AddPlaceStore = _AddPlaceStore with _$AddPlaceStore;

abstract class _AddPlaceStore with Store {
  final ApiProvider apiProvider;

  final ImagePicker _picker = ImagePicker();

  _AddPlaceStore({
    required this.apiProvider,
  }) {
    getMoods();
  }

  TextEditingController locationController = TextEditingController();
  TextEditingController titleController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController noteController = TextEditingController();

  Entry? postEntry; // used in editing mode
  geo_loc.Position? locationData;

  @observable
  bool editingMode = false;

  @observable
  List<HappyPlace> placesAutoCompleteList = [];

  @observable
  HappyPlace? finalPlaceSelected;

  @observable
  List<Mood> moods = [];

  @observable
  bool privacyPrivate = false;

  // @observable
  // bool privacypublic = false;

  // @observable
  // bool privacyFamily = false;

  // @observable
  // bool privacyFriends = false;

  @observable
  Mood? selectedMood;

  @observable
  bool loadingPlaces = false;

  @observable
  bool showPlacesLayout = false;

  @observable
  int selectedTab = 0;

  @observable
  bool isLoading = false;

  @observable
  bool isUploadCompleted = false;

  @observable
  bool moodsLoading = false;

  @observable
  bool placeAdded = false;

  @observable
  bool uploading = false;

  @observable
  int moreThan5ImagesPicked = 0;

  @observable
  TextEditingController controller = TextEditingController();

  @observable
  List<String> images = [];

  @action
  setPostDetials(Entry post) {
    postEntry = post;
    finalPlaceSelected = HappyPlace(
      description: post.location!,
      latitude: double.parse(post.latitude!),
      longitude: double.parse(post.longitude!),
    );
    locationController.text = post.location!;
    titleController.text = post.title!;

    DateTime dt = DateFormat('y-M-d').parse(post.postDate!);
    dateController.text = DateFormat.yMMMMd('en_US').format(dt);

    noteController.text = post.note ?? '';
    if (post.privacyLevel == '1') {
      privacyPrivate = true;
      // privacypublic = false;
      // privacyFriends = false;
      // privacyFamily = false;
    } else if (post.privacyLevel == '2') {
      privacyPrivate = false;
      // privacypublic = true;
      // privacyFriends = false;
      // privacyFamily = false;
    } else if (post.privacyLevel == '3') {
      privacyPrivate = false;
      // privacypublic = false;
      // privacyFriends = true;
      // privacyFamily = false;
    } else {
      privacyPrivate = false;
      // privacypublic = false;
      // privacyFriends = false;
      // privacyFamily = true;
    }

    for (var m in moods) {
      debugPrint('post mood id : ${post.moodId} ---- mood id: ${m.id}');
      if (m.id == post.moodId) {
        selectedMood = m;
      }
    }
    if (post.images != null) {
      for (var v in post.images!) {
        debugPrint('image path: ${v.imagePath}');
        images.add(v.imagePath!);
      }
      images = images;
    }
  }

  @action
  choosePhoto({required bool isCamera}) async {
    if (isCamera) {
      XFile? _pickedFiles = await _picker.pickImage(
        source: ImageSource.camera,
        imageQuality: 70,
      );
      if (_pickedFiles != null) {
        images.add(_pickedFiles.path);
      }
    } else if (!isCamera) {
      List<XFile?>? _pickedFiles = await _picker.pickMultiImage(
        imageQuality: 70,
      );
      debugPrint('images picked: ${_pickedFiles?.length}');

      if (_pickedFiles != null) {
        if (images.length + _pickedFiles.length > 5) {
          await HttpExceptionNotifyUser.showError(
              'You can not select more than 5 images');
        } else {
          for (var file in _pickedFiles) {
            if (file?.path != null) {
              images.add(file!.path);
            }
          }
        }
      }
    }
    images = images;
  }

  @action
  uploadPlace() async {
    try {
      isLoading = true;
      // upload images first
      List<String> files = [];
      if (images.isNotEmpty) {
        debugPrint('adding images');

        for (var element in images) {
          var s = await updatePicture(element);
          if (s != null) {
            files.add(s);
          }
        }
      }
      // carry on the rest of stuff
      int privacyLevel = 2;
      if (privacyPrivate) {
        privacyLevel = 1;
      } /*  else if (privacypublic) {
        privacyLevel = 2;
      } else if (privacyFriends) {
        privacyLevel = 3;
      } else if (privacyFamily) {
        privacyLevel = 4;
      } */
      DateTime dt = DateFormat.yMMMMd('en_US').parse(dateController.text);
      Response<EntriesStoreResponse> response =
          await apiProvider.apiClient.entiresPost(
        location: finalPlaceSelected?.description,
        latitude: finalPlaceSelected?.latitude.toString(),
        longitude: finalPlaceSelected?.longitude.toString(),
        privacyLevel: privacyLevel,
        title: titleController.text,
        postDate: DateFormat('y-M-d').format(dt),
        moodId: selectedMood!.id,
        note: noteController.text,
        imagePaths: jsonEncode(files),
      );
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        // saved. go back;
        if (isLoading) isLoading = false;

        placeAdded = true;
      } else {
        if (isLoading) isLoading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (isLoading) isLoading = false;
      debugPrint('uploadPlace exception: $e');
      rethrow;
    }
  }

  @action
  getMoods() async {
    try {
      moodsLoading = true;
      Response<MoodsListResponse> response =
          await apiProvider.apiClient.moodsGet();

      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        moods = ApiSuccessParser.payloadOrThrow(response);
        selectedMood = moods.first;
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }

      if (editingMode) {
        // setting moods now
        for (var m in moods) {
          debugPrint(
              'post mood id : ${postEntry!.moodId} ---- mood id: ${m.id}');
          if (m.id == postEntry!.moodId) {
            selectedMood = m;
          }
        }
      }

      moodsLoading = false;
    } catch (e) {
      moodsLoading = false;
      rethrow;
    }
  }

  @action
  searchAddressWithAutoComplete(String value) async {
    placesAutoCompleteList.clear();
    showPlacesLayout = true;
    loadingPlaces = true;

    var googlePlace = GooglePlace(kGoogleApiKey);
    AutocompleteResponse? result = await googlePlace.autocomplete.get(value);

    // // for check error msg
    // var details = await googlePlace.autocomplete.getJson(value);
    // debugPrint("search error");
    // debugPrint(details.toString());
    //
    // GoogleMapsPlaces _places = GoogleMapsPlaces(
    //   apiKey: kGoogleApiKey,
    //   apiHeaders: await GoogleApiHeaders().getHeaders(),
    // );
    // PlacesAutocompleteResponse placesAutoCompleteResponse =
    //     await _places.autocomplete(value);

    if (result != null) {
      if (result.status == 'OK') {
        if (result.predictions?.isNotEmpty ?? false) {
          debugPrint('predictions are: ${result.predictions?.length}');

          await Future.forEach(result.predictions!,
              (AutocompletePrediction prediction) async {
            if (prediction.description != null) {
              debugPrint('\n');
              debugPrint('single prediction: ${prediction.description}');
              // String? state, city, postalCode, country;
              double? latitude, longitude;

              if (prediction.placeId != null) {
                DetailsResponse? placeDetails =
                    await googlePlace.details.get(prediction.placeId!);
                latitude = placeDetails?.result?.geometry?.location?.lat;
                longitude = placeDetails?.result?.geometry?.location?.lng;

                if (prediction.description != null &&
                    latitude != null &&
                    longitude != null) {
                  placesAutoCompleteList.add(
                    HappyPlace(
                        description: prediction.description!,
                        latitude: latitude,
                        longitude: longitude),
                  );
                }
              }
              // PlacesDetailsResponse placeDetails =
              //     await _places.getDetailsByPlaceId(prediction.placeId!);

            }
          });
          placesAutoCompleteList = placesAutoCompleteList;
          loadingPlaces = false;
        }
      }
    }

    // if (placesAutoCompleteResponse.status == 'OK') {

    // }
  }

  @action
  Future<String?> updatePicture(String path) async {
    debugPrint('adding image: $path');

    if (path.contains('http')) {
      return path;
    }
    //swagger -> dart does not currently work with multipart file uploads
    var request = http.MultipartRequest('POST',
        Uri.parse(apiProvider.apiClient.client.baseUrl + '/image-thumb'));
    request.files.add(await http.MultipartFile.fromPath('image', path));
    request.headers['x-access-token'] =
        apiProvider.userProvider.user!.accessToken!;
    request.headers['x-api-key'] = apiProvider.apiSettings.apiKey;
    var response = await apiProvider.apiClient.client.httpClient.send(request);
    if (response.statusCode == 200) {
      final body = await response.stream.bytesToString();
      final Map<String, dynamic> bodyJson = json.decode(body);

      var url = bodyJson['payload']['url'];
      debugPrint('url got is: $url');

      return url;
    } else {
      final body = await response.stream.bytesToString();
      final Map<String, dynamic> bodyJson = json.decode(body);
      final message = bodyJson['message'] ?? '';
      debugPrint('message is : $message');

      return null;
    }
  }

  @action
  updatePost() async {
    try {
      isLoading = true;
      // upload images first
      List<String> files = [];
      if (images.isNotEmpty) {
        debugPrint('adding images');

        for (var element in images) {
          var s = await updatePicture(element);
          if (s != null) {
            files.add(s);
          }
        }
      }
      // carry on the rest of stuff
      int privacyLevel = 2;
      if (privacyPrivate) {
        privacyLevel = 1;
      } /*  else if (privacypublic) {
        privacyLevel = 2;
      } else if (privacyFriends) {
        privacyLevel = 3;
      } else if (privacyFamily) {
        privacyLevel = 4;
      } */

      Response<EntriesUpdateResponse> response =
          await apiProvider.apiClient.entriesIdPut(
        id: postEntry!.id,
        location: finalPlaceSelected?.description,
        latitude: finalPlaceSelected?.latitude.toString(),
        longitude: finalPlaceSelected?.longitude.toString(),
        privacyLevel: privacyLevel,
        title: titleController.text,
        postDate: dateController.text,
        moodId: selectedMood!.id,
        note: noteController.text,
        imagePaths: jsonEncode(files),
      );
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        // saved. go back;
        if (isLoading) isLoading = false;

        placeAdded = true;
      } else {
        if (isLoading) isLoading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      if (isLoading) isLoading = false;
      debugPrint('updatePost exception: $e');
      rethrow;
    }
  }

  @action
  getMyCurrentLocation() async {
    try {
      bool serviceEnabled;
      geo_loc.LocationPermission permission;

      serviceEnabled = await geo_loc.Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        return Future.error('Location services are disabled.');
      }

      permission = await geo_loc.Geolocator.checkPermission();
      if (permission == geo_loc.LocationPermission.denied) {
        permission = await geo_loc.Geolocator.requestPermission();
        if (permission == geo_loc.LocationPermission.denied) {
          return Future.error('Location permissions are denied');
        }
      }

      if (permission == geo_loc.LocationPermission.deniedForever) {
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }

      locationData = await geo_loc.Geolocator.getCurrentPosition(
          desiredAccuracy: geo_loc.LocationAccuracy.high);

      String _address = await getCurrentAddress(locationData!);

      finalPlaceSelected = HappyPlace(
          description: _address,
          latitude: locationData!.latitude,
          longitude: locationData!.longitude);
    } catch (e) {
      rethrow;
    }
  }

  getCurrentAddress(geo_loc.Position position) async {
    List<Placemark> placemarks = await placemarkFromCoordinates(
        locationData!.latitude, locationData!.longitude);

    String? area = placemarks[0].name != null ? "${placemarks[0].name}, " : "";
    String? district = placemarks[0].subAdministrativeArea != null
        ? "${placemarks[0].subAdministrativeArea}, "
        : "";
    String? country =
        placemarks[0].country != null ? "${placemarks[0].country}" : "";

    String _address = area + district + country;
    return _address;
  }
}
