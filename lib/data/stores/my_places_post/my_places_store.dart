import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:happy_place/api_utils/api_parser.dart';
import 'package:happy_place/api_utils/api_provider.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/ui/widgets/map_marker.dart';
import 'package:injectable/injectable.dart';
import 'package:mobx/mobx.dart';

part 'my_places_store.g.dart';

@injectable
class MyPlacesStore = _MyPlacesStore with _$MyPlacesStore;

abstract class _MyPlacesStore with Store {
  final ApiProvider apiProvider;

  _MyPlacesStore({
    required this.apiProvider,
  }) {
    init();
  }

  init() async {
    // await getLocationPermission();
    getPosts();
  }

  String? errorMessage;
  bool alreadyFocused = false;

  Position? locationData;

  @observable
  bool isLoading = false;

  @observable
  int currentTab = 0;

  @observable
  int focusMyCurrentPosition = 0;

  @observable
  int markersUpdated = 0;

  @observable
  List<Entry> myPosts = [];

  @observable
  List<MapMarker> markers = [];

  @observable
  Set<Marker> customMarkers = {};

  @action
  getPosts() async {
    try {
      Response<EntriesListResponse> response;

      response = await apiProvider.apiClient.entriesGet();

      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        myPosts = ApiSuccessParser.payloadOrThrow(response);
        markers.clear();
        for (var p in myPosts) {
          markers.add(MapMarker(p));
        }

        markers = markers;
        markersUpdated++;
        debugPrint('myposts length: ${myPosts.length}');
      } else {
        if (isLoading) isLoading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
      debugPrint(response.toString());
    } catch (e) {
      debugPrint(e.toString());
      rethrow;
    }
  }

  @action
  getLocationPermission() async {
    debugPrint('starting getLocationPermission');
    try {
      bool serviceEnabled;
      LocationPermission permission;

      // Test if location services are enabled.
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        // Location services are not enabled don't continue
        // accessing the position and request users of the
        // App to enable the location services.
        return Future.error('Location services are disabled.');
      }

      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          // Permissions are denied, next time you could try
          // requesting permissions again (this is also where
          // Android's shouldShowRequestPermissionRationale
          // returned true. According to Android guidelines
          // your App should show an explanatory UI now.
          return Future.error('Location permissions are denied');
        }
      }

      if (permission == LocationPermission.deniedForever) {
        // Permissions are denied forever, handle appropriately.
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }

      // When we reach here, permissions are granted and we can
      // continue accessing the position of the device.
      locationData = await Geolocator.getCurrentPosition();

      if (!alreadyFocused) {
        alreadyFocused = true;
        focusMyCurrentPosition++;
      }
    } catch (e) {
      rethrow;
    }
  }

  @action
  checkSharePost() async {
    try {
      Response<EntriesGetSharedPostsResponse> response =
          await apiProvider.apiClient.entriesSharedPostsGet();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        return ApiSuccessParser.payloadOrThrow(response);
      } else {
        return [];
      }
    } catch (e) {
      return [];
    }
  }

  @action
  checkFeedPosts() async {
    try {
      Response<EntriesListResponse> response =
          await apiProvider.apiClient.entriesGet(filter: "feed");
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        return ApiSuccessParser.payloadOrThrow(response);
      } else {
        return [];
      }
    } catch (e) {
      return [];
    }
  }
}
