import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart' as geo_loc;
import 'package:google_place/google_place.dart';
import 'package:happy_place/api_utils/api_parser.dart';
import 'package:happy_place/api_utils/api_provider.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/models/happy_place.dart';
import 'package:happy_place/ui/add_new_happy_place/add_new_happy_place.dart';
import 'package:injectable/injectable.dart';
import 'package:mobx/mobx.dart';

part 'add_new_happy_place_store.g.dart';

@injectable
class AddNewHappyPlaceStore = _AddNewHappyPlaceStore
    with _$AddNewHappyPlaceStore;

abstract class _AddNewHappyPlaceStore with Store {
  final ApiProvider apiProvider;
  _AddNewHappyPlaceStore({
    required this.apiProvider,
  }) {
    init();
  }

  init() async {
    isLoading = true;
    await getMoods();
    await getPlaces();
    await getActivities();
    await getFeelings();
    isLoading = false;
  }

  geo_loc.Position? locationData;
  String? errorMessage;
  @observable
  bool isLoading = false;
  String finalNoteFeeling = '';
  int? finalFeelingRating;

  @observable
  int error = 1;

  @observable
  int totalPages = 4;

  @observable
  int currentPage = 0;

  @observable
  List<Mood>? moods;

  @observable
  Mood? finalMoodSelected;

  @observable
  MoodType? finalMoodTypeSelected;

  @observable
  List<Place>? places;

  @observable
  Place? finalPlaceSelected;

  @observable
  PlaceType? finalPlaceTypeSelected;

  @observable
  List<Activity>? activities;

  @observable
  Activity? finalActivitySelected;

  @observable
  ActivityType? finalActivityTypeSelected;

  @observable
  List<Feeling>? feelings;

  @observable
  Feeling? finalFeelingSelected;

  @observable
  String? finalFeelingTypeSelected;

  @observable
  List<HappyPlace> placesAutoCompleteList = [];

  @observable
  bool loadingPlaces = false;

  @observable
  HappyPlace? finalLocationSelected;

  @observable
  bool savingPlace = false;

  @action
  getMoods() async {
    try {
      Response<MoodsListResponse> response =
          await apiProvider.apiClient.moodsGet();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        moods = ApiSuccessParser.payloadOrThrow(response);
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      debugPrint('get Feelings exception: $e');
    }
  }

  @action
  getPlaces() async {
    try {
      Response<PlacesListResponse> response =
          await apiProvider.apiClient.placesGet();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        places = ApiSuccessParser.payloadOrThrow(response);
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      debugPrint('get Places exception: $e');
    }
  }

  @action
  getActivities() async {
    try {
      Response<ActivitiesListResponse> response =
          await apiProvider.apiClient.activitiesGet();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        activities = ApiSuccessParser.payloadOrThrow(response);
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      debugPrint('get Activities exception: $e');
    }
  }

  @action
  getFeelings() async {
    try {
      Response<FeelingsListResponse> response =
          await apiProvider.apiClient.feelingsGet();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        feelings = ApiSuccessParser.payloadOrThrow(response);
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      debugPrint('get Feelings exception: $e');
    }
  }

  @action
  completeAHappyPlace() async {
    try {
      savingPlace = true;
      Response<VisitedPlacesSaveResponse> response =
          await apiProvider.apiClient.visitedPlacesPost(
        location: finalLocationSelected!.description,
        latitude: finalLocationSelected!.latitude.toString(),
        longitude: finalLocationSelected!.longitude.toString(),
        moodId: finalMoodSelected?.id,
        moodTypeId: finalMoodTypeSelected?.id,
        placeId: finalPlaceSelected?.id,
        placeTypeId: finalPlaceTypeSelected?.id,
        activityId: int.parse(finalActivitySelected!.id!),
        activityTypeId: finalActivityTypeSelected?.id,
        feelingId: finalFeelingSelected?.id,
        feelingRatingId: finalFeelingRating,
        feelingNote: finalNoteFeeling,
      );
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        VisitedPlace? visitedPlace = ApiSuccessParser.payloadOrThrow(response);
        debugPrint('visited place created: ${visitedPlace?.id}');
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
      if (savingPlace) savingPlace = false;
    } catch (e) {
      if (savingPlace) savingPlace = false;
      debugPrint('complete happy place exception: $e');
      rethrow;
    }
  }

  nextPage() {
    if (currentPage < totalPages) currentPage++;
  }

  previousPage() {
    if (currentPage > 0) currentPage--;
  }

  @action
  getMyCurrentLocation() async {
    try {
      bool serviceEnabled;
      geo_loc.LocationPermission permission;

      serviceEnabled = await geo_loc.Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        return Future.error('Location services are disabled.');
      }

      permission = await geo_loc.Geolocator.checkPermission();
      if (permission == geo_loc.LocationPermission.denied) {
        permission = await geo_loc.Geolocator.requestPermission();
        if (permission == geo_loc.LocationPermission.denied) {
          return Future.error('Location permissions are denied');
        }
      }

      if (permission == geo_loc.LocationPermission.deniedForever) {
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }

      locationData = await geo_loc.Geolocator.getCurrentPosition(
          desiredAccuracy: geo_loc.LocationAccuracy.high);

      String _address = await getCurrentAddress(locationData!);

      finalLocationSelected = HappyPlace(
          description: _address,
          latitude: locationData!.latitude,
          longitude: locationData!.longitude);
    } catch (e) {
      rethrow;
    }
  }

  getCurrentAddress(geo_loc.Position position) async {
    List<Placemark> placemarks = await placemarkFromCoordinates(
        locationData!.latitude, locationData!.longitude);

    String? area = placemarks[0].name != null ? "${placemarks[0].name}, " : "";
    String? district = placemarks[0].subAdministrativeArea != null
        ? "${placemarks[0].subAdministrativeArea}, "
        : "";
    String? country =
        placemarks[0].country != null ? "${placemarks[0].country}" : "";

    String _address = area + district + country;
    return _address;
  }

  @action
  searchAddressWithAutoComplete(String value) async {
    placesAutoCompleteList.clear();
    loadingPlaces = true;

    var googlePlace = GooglePlace(kGoogleApiKey);
    AutocompleteResponse? result = await googlePlace.autocomplete.get(value);

    // GoogleMapsPlaces _places = GoogleMapsPlaces(
    //   apiKey: kGoogleApiKey,
    //   apiHeaders: await GoogleApiHeaders().getHeaders(),
    // );
    // PlacesAutocompleteResponse placesAutoCompleteResponse =
    //     await _places.autocomplete(value);

    if (result != null) {
      if (result.status == 'OK') {
        if (result.predictions?.isNotEmpty ?? false) {
          debugPrint('predictions are: ${result.predictions?.length}');

          if (result.predictions != null) {
            await Future.forEach(result.predictions!,
                (AutocompletePrediction prediction) async {
              if (prediction.description != null) {
                debugPrint('\n');
                debugPrint('single prediction: ${prediction.description}');
                // String? state, city, postalCode, country;
                double? latitude, longitude;

                if (prediction.placeId != null) {
                  DetailsResponse? placeDetails =
                      await googlePlace.details.get(prediction.placeId!);
                  latitude = placeDetails?.result?.geometry?.location?.lat;
                  longitude = placeDetails?.result?.geometry?.location?.lng;

                  if (latitude != null && longitude != null) {
                    placesAutoCompleteList.add(
                      HappyPlace(
                          description: prediction.description!,
                          latitude: latitude,
                          longitude: longitude),
                    );
                  }
                }

                // PlacesDetailsResponse placeDetails =
                //     await _places.getDetailsByPlaceId(prediction.placeId!);
                // latitude = placeDetails.result.geometry?.location.lat;
                // longitude = placeDetails.result.geometry?.location.lng;

              }
            });
            placesAutoCompleteList = placesAutoCompleteList;
            loadingPlaces = false;
          }
        }
      }
    }
  }
}
