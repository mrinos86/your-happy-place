import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:happy_place/api_utils/user_provider.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/injection.dart';
import 'package:injectable/injectable.dart';
import 'package:mobx/mobx.dart';

import '../../api_utils/api_parser.dart';
import '../../api_utils/api_provider.dart';

part 'navigation_store.g.dart';

@singleton
class NavigationStore = _NavigationStore with _$NavigationStore;

abstract class _NavigationStore with Store {
  // final ApiProvider apiProvider;

  _NavigationStore() {
    getProfile();
  }

  @observable
  int selectedPage = 0;

  @observable
  bool isLoading = false;

  @observable
  bool dontShowAgain = false;

  @action
  selectPage(int page) {
    debugPrint('homepage - updating select Page $page');

    selectedPage = page;
  }

  @action
  updateDontShowAgain() async {
    try {
      debugPrint('getting dont show again');
      Response<ProfileHideProfileUpdateMessageResponse> response =
          await getIt<ApiProvider>()
              .apiClient
              .profileCompleteProfileRememberPut();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        dontShowAgain = true;
        ApiSuccessParser.payloadOrThrow(response);
        debugPrint('updateDontShowAgain: $dontShowAgain');
      }
    } catch (e) {
      debugPrint('exception updateDontShowAgain: $e');
      rethrow;
    }
  }

  @action
  getProfile() async {
    isLoading = true;
    await Future.delayed(const Duration(seconds: 2));
    debugPrint(
        'user access token homepage: ${getIt<UserProvider>().user?.accessToken}');
    debugPrint(
        'user access token homepage: ${getIt<UserProvider>().user?.email}');
    try {
      Response<ProfileMyProfileResponse> response =
          await getIt<ApiProvider>().apiClient.profileGet();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        User? user = ApiSuccessParser.payloadOrThrow(response);
        dontShowAgain = user?.completeProfileRemember ?? false;
        debugPrint(
            'user complete profile bool got: ${user?.completeProfileRemember}');
      }
      isLoading = false;
    } catch (e) {
      debugPrint('get Profile exception in navigation store: $e');
      isLoading = false;
      rethrow;
    }
  }

  @action
  reset() {
    dontShowAgain = false;
  }
}
