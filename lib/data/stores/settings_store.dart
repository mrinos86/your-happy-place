import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:happy_place/api_utils/api_parser.dart';
import 'package:happy_place/api_utils/api_provider.dart';
import 'package:happy_place/api_utils/http_exception.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/injection.dart';
import 'package:happy_place/main.dart';
import 'package:happy_place/ui/authentication/login_page.dart';
import 'package:injectable/injectable.dart';
import 'package:mobx/mobx.dart';

part 'settings_store.g.dart';

@injectable
class SettingsStore = _SettingsStore with _$SettingsStore;

abstract class _SettingsStore with Store {
  final ApiProvider apiProvider;
  _SettingsStore({
    required this.apiProvider,
  }) {
    getSettings();
  }

  @observable
  bool keepPrivate = false;

  @observable
  bool makePublic = false;

  @observable
  bool family = false;

  @observable
  bool friends = false;

  @observable
  bool visibleUNameAll = false;

  @observable
  bool visibleUNameFamily = false;

  @observable
  bool visibleUNameFriends = false;

  @observable
  int visibleUNameValue = 0;

  @observable
  bool savingSettings = false;

  @observable
  bool isLoading = false;

  @action
  getSettings() async {
    try {
      isLoading = true;
      Response<ProfileMyProfileResponse> response =
          await apiProvider.apiClient.profileGet();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        User? user = ApiSuccessParser.payloadOrThrow(response);
        if (user != null) {
          keepPrivate = user.postsKeepPrivate == '0' ? false : true;
          makePublic = user.postsWithPublic == '0' ? false : true;
          family = user.postsWithFamily == '0' ? false : true;
          friends = user.postsWithFriends == '0' ? false : true;
          // visibleUNameAll = user.visibleUsername == '0' ? false : true;
          fromVisibleUserNameValue(user);
        }
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
      isLoading = false;
    } catch (e) {
      isLoading = false;
      debugPrint('complete happy place exception: $e');
      rethrow;
    }
  }

  @action
  updateSettings() async {
    try {
      Response<ProfileUpdateMyProfileSettingsResponse> response =
          await apiProvider.apiClient.profileSettingsPut(
        postsKeepPrivate: keepPrivate ? 1 : 0,
        postsWithPublic: makePublic ? 1 : 0,
        postsWithFamily: family ? 1 : 0,
        postsWithFriends: friends ? 1 : 0,
        // visibleUsername: visibleUNameAll ? 1 : 0,
        visibleUsername: toVisibleUserNameValue(),
      );
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        debugPrint('settings updated');
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      debugPrint('updateSettings exception: $e');
      rethrow;
    }
  }

  @action
  deleteProfile() async {
    try {
      isLoading = true;
      Response<SuccessResponse> response =
          await apiProvider.apiClient.profileDeleteAccountDelete();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        debugPrint('deleted: $response');
        await HttpExceptionNotifyUser.showError("Your account deleted");
        await Navigator.push(navigatorKey.currentContext!,
            MaterialPageRoute(builder: (context) => getIt<LoginPage>()));
        isLoading = false;
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
        isLoading = false;
      }
    } catch (e) {
      isLoading = false;
      debugPrint('profile delete exception: $e');
      rethrow;
    }
  }

  void fromVisibleUserNameValue(User user) {
    if (user.visibleUsername == "0") {
      visibleUNameAll = false;
      visibleUNameFamily = false;
      visibleUNameFriends = false;
    } else if (user.visibleUsername == "1") {
      visibleUNameAll = true;
      visibleUNameFamily = false;
      visibleUNameFriends = false;
    } else if (user.visibleUsername == "2") {
      visibleUNameAll = false;
      visibleUNameFamily = true;
      visibleUNameFriends = false;
    } else if (user.visibleUsername == "3") {
      visibleUNameAll = false;
      visibleUNameFamily = false;
      visibleUNameFriends = true;
    } else if (user.visibleUsername == "4") {
      visibleUNameAll = false;
      visibleUNameFamily = true;
      visibleUNameFriends = true;
    }
  }

  int toVisibleUserNameValue() {
    int value = 0;

    if (visibleUNameAll == true) {
      value = 1;
    } else if (visibleUNameAll == false &&
        visibleUNameFamily == true &&
        visibleUNameFriends == false) {
      value = 2;
    } else if (visibleUNameAll == false &&
        visibleUNameFamily == false &&
        visibleUNameFriends == true) {
      value = 3;
    } else if (visibleUNameAll == false &&
        visibleUNameFamily == true &&
        visibleUNameFriends == true) {
      value = 4;
    }
    return value;
  }
}
