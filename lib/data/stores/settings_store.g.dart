// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'settings_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$SettingsStore on _SettingsStore, Store {
  final _$keepPrivateAtom = Atom(name: '_SettingsStore.keepPrivate');

  @override
  bool get keepPrivate {
    _$keepPrivateAtom.reportRead();
    return super.keepPrivate;
  }

  @override
  set keepPrivate(bool value) {
    _$keepPrivateAtom.reportWrite(value, super.keepPrivate, () {
      super.keepPrivate = value;
    });
  }

  final _$makePublicAtom = Atom(name: '_SettingsStore.makePublic');

  @override
  bool get makePublic {
    _$makePublicAtom.reportRead();
    return super.makePublic;
  }

  @override
  set makePublic(bool value) {
    _$makePublicAtom.reportWrite(value, super.makePublic, () {
      super.makePublic = value;
    });
  }

  final _$familyAtom = Atom(name: '_SettingsStore.family');

  @override
  bool get family {
    _$familyAtom.reportRead();
    return super.family;
  }

  @override
  set family(bool value) {
    _$familyAtom.reportWrite(value, super.family, () {
      super.family = value;
    });
  }

  final _$friendsAtom = Atom(name: '_SettingsStore.friends');

  @override
  bool get friends {
    _$friendsAtom.reportRead();
    return super.friends;
  }

  @override
  set friends(bool value) {
    _$friendsAtom.reportWrite(value, super.friends, () {
      super.friends = value;
    });
  }

  final _$visibleUNameAllAtom = Atom(name: '_SettingsStore.visibleUNameAll');

  @override
  bool get visibleUNameAll {
    _$visibleUNameAllAtom.reportRead();
    return super.visibleUNameAll;
  }

  @override
  set visibleUNameAll(bool value) {
    _$visibleUNameAllAtom.reportWrite(value, super.visibleUNameAll, () {
      super.visibleUNameAll = value;
    });
  }

  final _$visibleUNameFamilyAtom =
      Atom(name: '_SettingsStore.visibleUNameFamily');

  @override
  bool get visibleUNameFamily {
    _$visibleUNameFamilyAtom.reportRead();
    return super.visibleUNameFamily;
  }

  @override
  set visibleUNameFamily(bool value) {
    _$visibleUNameFamilyAtom.reportWrite(value, super.visibleUNameFamily, () {
      super.visibleUNameFamily = value;
    });
  }

  final _$visibleUNameFriendsAtom =
      Atom(name: '_SettingsStore.visibleUNameFriends');

  @override
  bool get visibleUNameFriends {
    _$visibleUNameFriendsAtom.reportRead();
    return super.visibleUNameFriends;
  }

  @override
  set visibleUNameFriends(bool value) {
    _$visibleUNameFriendsAtom.reportWrite(value, super.visibleUNameFriends, () {
      super.visibleUNameFriends = value;
    });
  }

  final _$visibleUNameValueAtom =
      Atom(name: '_SettingsStore.visibleUNameValue');

  @override
  int get visibleUNameValue {
    _$visibleUNameValueAtom.reportRead();
    return super.visibleUNameValue;
  }

  @override
  set visibleUNameValue(int value) {
    _$visibleUNameValueAtom.reportWrite(value, super.visibleUNameValue, () {
      super.visibleUNameValue = value;
    });
  }

  final _$savingSettingsAtom = Atom(name: '_SettingsStore.savingSettings');

  @override
  bool get savingSettings {
    _$savingSettingsAtom.reportRead();
    return super.savingSettings;
  }

  @override
  set savingSettings(bool value) {
    _$savingSettingsAtom.reportWrite(value, super.savingSettings, () {
      super.savingSettings = value;
    });
  }

  final _$isLoadingAtom = Atom(name: '_SettingsStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$getSettingsAsyncAction = AsyncAction('_SettingsStore.getSettings');

  @override
  Future getSettings() {
    return _$getSettingsAsyncAction.run(() => super.getSettings());
  }

  final _$updateSettingsAsyncAction =
      AsyncAction('_SettingsStore.updateSettings');

  @override
  Future updateSettings() {
    return _$updateSettingsAsyncAction.run(() => super.updateSettings());
  }

  final _$deleteProfileAsyncAction =
      AsyncAction('_SettingsStore.deleteProfile');

  @override
  Future deleteProfile() {
    return _$deleteProfileAsyncAction.run(() => super.deleteProfile());
  }

  @override
  String toString() {
    return '''
keepPrivate: ${keepPrivate},
makePublic: ${makePublic},
family: ${family},
friends: ${friends},
visibleUNameAll: ${visibleUNameAll},
visibleUNameFamily: ${visibleUNameFamily},
visibleUNameFriends: ${visibleUNameFriends},
visibleUNameValue: ${visibleUNameValue},
savingSettings: ${savingSettings},
isLoading: ${isLoading}
    ''';
  }
}
