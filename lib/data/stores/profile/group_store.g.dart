// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'group_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$GroupStore on _GroupStore, Store {
  final _$isLoadingAtom = Atom(name: '_GroupStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$peopleCategoryAtom = Atom(name: '_GroupStore.peopleCategory');

  @override
  String get peopleCategory {
    _$peopleCategoryAtom.reportRead();
    return super.peopleCategory;
  }

  @override
  set peopleCategory(String value) {
    _$peopleCategoryAtom.reportWrite(value, super.peopleCategory, () {
      super.peopleCategory = value;
    });
  }

  final _$userAtom = Atom(name: '_GroupStore.user');

  @override
  User? get user {
    _$userAtom.reportRead();
    return super.user;
  }

  @override
  set user(User? value) {
    _$userAtom.reportWrite(value, super.user, () {
      super.user = value;
    });
  }

  final _$groupsAtom = Atom(name: '_GroupStore.groups');

  @override
  List<GroupWithWrapper> get groups {
    _$groupsAtom.reportRead();
    return super.groups;
  }

  @override
  set groups(List<GroupWithWrapper> value) {
    _$groupsAtom.reportWrite(value, super.groups, () {
      super.groups = value;
    });
  }

  final _$friendsFamilyAtom = Atom(name: '_GroupStore.friendsFamily');

  @override
  List<UserWithWrapper> get friendsFamily {
    _$friendsFamilyAtom.reportRead();
    return super.friendsFamily;
  }

  @override
  set friendsFamily(List<UserWithWrapper> value) {
    _$friendsFamilyAtom.reportWrite(value, super.friendsFamily, () {
      super.friendsFamily = value;
    });
  }

  final _$friendsFamilyCheckedListAtom =
      Atom(name: '_GroupStore.friendsFamilyCheckedList');

  @override
  List<UserWithWrapper> get friendsFamilyCheckedList {
    _$friendsFamilyCheckedListAtom.reportRead();
    return super.friendsFamilyCheckedList;
  }

  @override
  set friendsFamilyCheckedList(List<UserWithWrapper> value) {
    _$friendsFamilyCheckedListAtom
        .reportWrite(value, super.friendsFamilyCheckedList, () {
      super.friendsFamilyCheckedList = value;
    });
  }

  final _$pickedGroupImageAtom = Atom(name: '_GroupStore.pickedGroupImage');

  @override
  String? get pickedGroupImage {
    _$pickedGroupImageAtom.reportRead();
    return super.pickedGroupImage;
  }

  @override
  set pickedGroupImage(String? value) {
    _$pickedGroupImageAtom.reportWrite(value, super.pickedGroupImage, () {
      super.pickedGroupImage = value;
    });
  }

  final _$isGroupUpdateDisabledAtom =
      Atom(name: '_GroupStore.isGroupUpdateDisabled');

  @override
  bool get isGroupUpdateDisabled {
    _$isGroupUpdateDisabledAtom.reportRead();
    return super.isGroupUpdateDisabled;
  }

  @override
  set isGroupUpdateDisabled(bool value) {
    _$isGroupUpdateDisabledAtom.reportWrite(value, super.isGroupUpdateDisabled,
        () {
      super.isGroupUpdateDisabled = value;
    });
  }

  final _$viewedGroupAtom = Atom(name: '_GroupStore.viewedGroup');

  @override
  Group? get viewedGroup {
    _$viewedGroupAtom.reportRead();
    return super.viewedGroup;
  }

  @override
  set viewedGroup(Group? value) {
    _$viewedGroupAtom.reportWrite(value, super.viewedGroup, () {
      super.viewedGroup = value;
    });
  }

  final _$groupMembersListAtom = Atom(name: '_GroupStore.groupMembersList');

  @override
  List<GroupMemberWithWrapper> get groupMembersList {
    _$groupMembersListAtom.reportRead();
    return super.groupMembersList;
  }

  @override
  set groupMembersList(List<GroupMemberWithWrapper> value) {
    _$groupMembersListAtom.reportWrite(value, super.groupMembersList, () {
      super.groupMembersList = value;
    });
  }

  final _$groupMembersListLoadingAtom =
      Atom(name: '_GroupStore.groupMembersListLoading');

  @override
  bool get groupMembersListLoading {
    _$groupMembersListLoadingAtom.reportRead();
    return super.groupMembersListLoading;
  }

  @override
  set groupMembersListLoading(bool value) {
    _$groupMembersListLoadingAtom
        .reportWrite(value, super.groupMembersListLoading, () {
      super.groupMembersListLoading = value;
    });
  }

  final _$ischeckedAllGMembersAtom =
      Atom(name: '_GroupStore.ischeckedAllGMembers');

  @override
  bool get ischeckedAllGMembers {
    _$ischeckedAllGMembersAtom.reportRead();
    return super.ischeckedAllGMembers;
  }

  @override
  set ischeckedAllGMembers(bool value) {
    _$ischeckedAllGMembersAtom.reportWrite(value, super.ischeckedAllGMembers,
        () {
      super.ischeckedAllGMembers = value;
    });
  }

  final _$groupMemberDeleteModeAtom =
      Atom(name: '_GroupStore.groupMemberDeleteMode');

  @override
  bool get groupMemberDeleteMode {
    _$groupMemberDeleteModeAtom.reportRead();
    return super.groupMemberDeleteMode;
  }

  @override
  set groupMemberDeleteMode(bool value) {
    _$groupMemberDeleteModeAtom.reportWrite(value, super.groupMemberDeleteMode,
        () {
      super.groupMemberDeleteMode = value;
    });
  }

  final _$groupBlockValueAtom = Atom(name: '_GroupStore.groupBlockValue');

  @override
  bool get groupBlockValue {
    _$groupBlockValueAtom.reportRead();
    return super.groupBlockValue;
  }

  @override
  set groupBlockValue(bool value) {
    _$groupBlockValueAtom.reportWrite(value, super.groupBlockValue, () {
      super.groupBlockValue = value;
    });
  }

  final _$getMyFriendsFamilyAsyncAction =
      AsyncAction('_GroupStore.getMyFriendsFamily');

  @override
  Future getMyFriendsFamily(String? searchQuery) {
    return _$getMyFriendsFamilyAsyncAction
        .run(() => super.getMyFriendsFamily(searchQuery));
  }

  final _$createGroupAsyncAction = AsyncAction('_GroupStore.createGroup');

  @override
  Future createGroup({required String groupName}) {
    return _$createGroupAsyncAction
        .run(() => super.createGroup(groupName: groupName));
  }

  final _$chooseGroupPhotoAsyncAction =
      AsyncAction('_GroupStore.chooseGroupPhoto');

  @override
  Future chooseGroupPhoto({required bool isCamera}) {
    return _$chooseGroupPhotoAsyncAction
        .run(() => super.chooseGroupPhoto(isCamera: isCamera));
  }

  final _$updateGroupAsyncAction = AsyncAction('_GroupStore.updateGroup');

  @override
  Future updateGroup({required String groupName, required String groupId}) {
    return _$updateGroupAsyncAction
        .run(() => super.updateGroup(groupName: groupName, groupId: groupId));
  }

  final _$getGroupDetailsAsyncAction =
      AsyncAction('_GroupStore.getGroupDetails');

  @override
  Future getGroupDetails({required int groupId}) {
    return _$getGroupDetailsAsyncAction
        .run(() => super.getGroupDetails(groupId: groupId));
  }

  final _$getGroupMemberListAsyncAction =
      AsyncAction('_GroupStore.getGroupMemberList');

  @override
  Future getGroupMemberList({required int groupId, String? q}) {
    return _$getGroupMemberListAsyncAction
        .run(() => super.getGroupMemberList(groupId: groupId, q: q));
  }

  final _$addMembersToGroupAsyncAction =
      AsyncAction('_GroupStore.addMembersToGroup');

  @override
  Future addMembersToGroup({required int groupId}) {
    return _$addMembersToGroupAsyncAction
        .run(() => super.addMembersToGroup(groupId: groupId));
  }

  final _$removeMembersFromGroupAsyncAction =
      AsyncAction('_GroupStore.removeMembersFromGroup');

  @override
  Future removeMembersFromGroup(
      {required int groupId, required String userIdList}) {
    return _$removeMembersFromGroupAsyncAction.run(() =>
        super.removeMembersFromGroup(groupId: groupId, userIdList: userIdList));
  }

  final _$leaveFromGroupAsyncAction = AsyncAction('_GroupStore.leaveFromGroup');

  @override
  Future leaveFromGroup({required int groupId}) {
    return _$leaveFromGroupAsyncAction
        .run(() => super.leaveFromGroup(groupId: groupId));
  }

  final _$reportGroupAsyncAction = AsyncAction('_GroupStore.reportGroup');

  @override
  Future reportGroup({required int groupId, String? reason, String? message}) {
    return _$reportGroupAsyncAction.run(() =>
        super.reportGroup(groupId: groupId, reason: reason, message: message));
  }

  final _$blockGroupAsyncAction = AsyncAction('_GroupStore.blockGroup');

  @override
  Future blockGroup({required int groupId}) {
    return _$blockGroupAsyncAction
        .run(() => super.blockGroup(groupId: groupId));
  }

  final _$_GroupStoreActionController = ActionController(name: '_GroupStore');

  @override
  dynamic checkAllItems(bool val) {
    final _$actionInfo = _$_GroupStoreActionController.startAction(
        name: '_GroupStore.checkAllItems');
    try {
      return super.checkAllItems(val);
    } finally {
      _$_GroupStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic updatePeopleCategory(String value) {
    final _$actionInfo = _$_GroupStoreActionController.startAction(
        name: '_GroupStore.updatePeopleCategory');
    try {
      return super.updatePeopleCategory(value);
    } finally {
      _$_GroupStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic checkAllGroupMember(bool val) {
    final _$actionInfo = _$_GroupStoreActionController.startAction(
        name: '_GroupStore.checkAllGroupMember');
    try {
      return super.checkAllGroupMember(val);
    } finally {
      _$_GroupStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isLoading: ${isLoading},
peopleCategory: ${peopleCategory},
user: ${user},
groups: ${groups},
friendsFamily: ${friendsFamily},
friendsFamilyCheckedList: ${friendsFamilyCheckedList},
pickedGroupImage: ${pickedGroupImage},
isGroupUpdateDisabled: ${isGroupUpdateDisabled},
viewedGroup: ${viewedGroup},
groupMembersList: ${groupMembersList},
groupMembersListLoading: ${groupMembersListLoading},
ischeckedAllGMembers: ${ischeckedAllGMembers},
groupMemberDeleteMode: ${groupMemberDeleteMode},
groupBlockValue: ${groupBlockValue}
    ''';
  }
}
