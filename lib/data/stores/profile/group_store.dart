import 'dart:convert';

import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:happy_place/api_utils/api_parser.dart';
import 'package:happy_place/api_utils/api_provider.dart';
import 'package:happy_place/api_utils/http_exception.dart';
import 'package:happy_place/data/stores/profile/profile_store.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/injection.dart';
import 'package:happy_place/main.dart';
import 'package:happy_place/models/group_with_wrapper.dart';
import 'package:happy_place/models/user_with_wrapper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:injectable/injectable.dart';
import 'package:mobx/mobx.dart';
import 'package:http/http.dart' as http;

part 'group_store.g.dart';

@singleton
class GroupStore = _GroupStore with _$GroupStore;

abstract class _GroupStore with Store {
  final ApiProvider apiProvider;
  _GroupStore({
    required this.apiProvider,
  });

  final ImagePicker _picker = ImagePicker();

  @observable
  bool isLoading = false;

  @observable
  String peopleCategory = 'Friends';

  @observable
  User? user;

// ----- Groups -----//

  @observable
  List<GroupWithWrapper> groups = [];

  @observable
  List<UserWithWrapper> friendsFamily = [];

  @observable
  List<UserWithWrapper> friendsFamilyCheckedList = [];

  @observable
  String? pickedGroupImage;

  @observable
  bool isGroupUpdateDisabled = true;

  @observable
  Group? viewedGroup;

  @observable
  List<GroupMemberWithWrapper> groupMembersList = [];

  @observable
  bool groupMembersListLoading = false;

  @observable
  bool ischeckedAllGMembers = false;

  @observable
  bool groupMemberDeleteMode = false;

  @observable
  bool groupBlockValue = false;

  @action
  checkAllItems(bool val) {
    if (peopleCategory == 'Groups') {
      for (var i in groups) {
        i.checked = val;
      }
      groups = groups;
    }
  }

  @action
  updatePeopleCategory(String value) {
    peopleCategory = value;
  }

  @action
  getMyFriendsFamily(String? searchQuery) async {
    try {
      isLoading = true;
      Response<FriendGetMyFriendsAndFamilyResponse> response =
          await apiProvider.apiClient.friendsFamilyGet(q: searchQuery);
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        List<User> user = ApiSuccessParser.payloadOrThrow(response);
        friendsFamily.clear();
        for (var u in user) {
          friendsFamily.add(UserWithWrapper(user: u, checked: false));
        }
        isLoading = false;
      } else {
        isLoading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      isLoading = false;
      debugPrint('get friendFamily exception: $e');
      rethrow;
    }
  }

  @action
  createGroup({required String groupName}) async {
    isLoading = true;
    try {
      List<String> userIds = [];
      for (var element in friendsFamilyCheckedList) {
        userIds.add(element.user.id!);
      }
      String userIdString = '';
      if (userIds.isNotEmpty) {
        userIdString = userIds.join(',');
      }

      final url = Uri.parse(apiProvider.apiClient.client.baseUrl + '/groups');

      Map<String, String> headers = {
        'x-access-token': apiProvider.userProvider.user!.accessToken!,
        'x-api-key': apiProvider.apiSettings.apiKey,
      };
      var request = http.MultipartRequest('POST', url);

      request.headers.addAll(headers);

      if (pickedGroupImage != null) {
        request.files
            .add(await http.MultipartFile.fromPath('image', pickedGroupImage!));
      }
      request.fields['name'] = groupName;
      request.fields['user_ids'] = userIdString;

      var response = await request.send();

      if (response.statusCode == 200) {
        isLoading = false;
        HttpExceptionNotifyUser.showMessage("Group created");
        getIt<ProfileStore>().init();
        Navigator.pop(navigatorKey.currentContext!);
        Navigator.pop(navigatorKey.currentContext!);
      } else {
        isLoading = false;
        final body = await response.stream.bytesToString();
        final Map<String, dynamic> bodyJson = json.decode(body);
        final message = bodyJson['message'] ?? '';
        debugPrint('message is : $message');
        HttpExceptionNotifyUser.showError(message);
      }
    } catch (e) {
      isLoading = false;
      debugPrint('create group exception: $e');
      rethrow;
    }
  }

  @action
  chooseGroupPhoto({required bool isCamera}) async {
    XFile? _pickedFiles = await _picker.pickImage(
      source: isCamera ? ImageSource.camera : ImageSource.gallery,
      imageQuality: 20,
    );
    if (_pickedFiles != null) {
      debugPrint('images picked: ${_pickedFiles.path}');
      pickedGroupImage = _pickedFiles.path;
      isGroupUpdateDisabled = false;
    }
  }

  @action
  updateGroup({required String groupName, required String groupId}) async {
    isLoading = true;
    try {
      final url =
          Uri.parse(apiProvider.apiClient.client.baseUrl + '/groups/$groupId');

      var request = http.MultipartRequest('POST', url);

      Map<String, String> headers = {
        'x-access-token': apiProvider.userProvider.user!.accessToken!,
        'x-api-key': apiProvider.apiSettings.apiKey,
      };

      request.headers.addAll(headers);
      request.fields['name'] = groupName;
      if (pickedGroupImage != null) {
        request.files.add(
          await http.MultipartFile.fromPath('image', pickedGroupImage!),
        );
      }

      debugPrint(request.toString());
      debugPrint(request.fields.toString());

      var response = await request.send();

      final body = await response.stream.bytesToString();
      final Map<String, dynamic> bodyJson = json.decode(body);
      debugPrint(bodyJson.toString());

      if (response.statusCode == 200) {
        isLoading = false;
        HttpExceptionNotifyUser.showMessage("Group updated");
        getIt<ProfileStore>().init();
        Navigator.pop(navigatorKey.currentContext!);
      } else {
        isLoading = false;
        final message = bodyJson['message'] ?? '';
        HttpExceptionNotifyUser.showError(message);
      }
    } catch (e) {
      isLoading = false;
      debugPrint('update group exception: $e');
      rethrow;
    }
  }

  @action
  getGroupDetails({required int groupId}) async {
    isLoading = true;
    try {
      Response<GroupsShowResponse> response =
          await apiProvider.apiClient.groupsIdGet(id: groupId);
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        viewedGroup = ApiSuccessParser.payloadOrThrow(response);
        isLoading = false;
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
        isLoading = false;
      }
    } catch (e) {
      isLoading = false;
      debugPrint('get group exception: $e');
      rethrow;
    }
  }

  @action
  getGroupMemberList({required int groupId, String? q}) async {
    groupMembersListLoading = true;
    try {
      Response<GroupsGetMembersListResponse> response =
          await apiProvider.apiClient.groupsMembersIdGet(id: groupId, q: q);
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        List<GroupMember> groupMembers =
            ApiSuccessParser.payloadOrThrow(response);
        groupMembersList.clear();
        for (var u in groupMembers) {
          groupMembersList
              .add(GroupMemberWithWrapper(groupMember: u, checked: false));
        }
        groupMembersListLoading = false;
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
        groupMembersListLoading = false;
      }
    } catch (e) {
      groupMembersListLoading = false;
      debugPrint('get group exception: $e');
      rethrow;
    }
  }

  @action
  checkAllGroupMember(bool val) {
    ischeckedAllGMembers = val;
    for (var i in groupMembersList) {
      if (i.groupMember.userId != user!.id) {
        i.checked = val;
      }
    }
    groupMembersList = groupMembersList;
  }

  @action
  addMembersToGroup({required int groupId}) async {
    isLoading = true;
    try {
      List<String> userIds = [];
      for (var element in friendsFamilyCheckedList) {
        userIds.add(element.user.id!);
      }
      String userIdString = '';
      if (userIds.isNotEmpty) {
        userIdString = userIds.join(',');
      }

      Response<SuccessResponse> response = await apiProvider.apiClient
          .groupsMembersAddPost(
              groupId: groupId, userIds: userIdString, type: 1);
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        isLoading = false;
        HttpExceptionNotifyUser.showMessage("Group members are updated");
        getIt<GroupStore>().getGroupDetails(groupId: groupId);
        getIt<GroupStore>().getGroupMemberList(groupId: groupId, q: '');
        Navigator.pop(navigatorKey.currentContext!);
      } else {
        isLoading = false;
        return ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      isLoading = false;
      debugPrint('add members to group exception: $e');
      rethrow;
    }
  }

  @action
  removeMembersFromGroup(
      {required int groupId, required String userIdList}) async {
    isLoading = true;
    try {
      Response<SuccessResponse> response = await apiProvider.apiClient
          .groupsMembersRemoveDelete(groupId: groupId, userIds: userIdList);
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        isLoading = false;
        HttpExceptionNotifyUser.showMessage("Group members are removed");
        getIt<GroupStore>().getGroupDetails(groupId: groupId);
        getIt<GroupStore>().getGroupMemberList(groupId: groupId, q: '');
        groupMemberDeleteMode = false;
      } else {
        isLoading = false;
        return ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      isLoading = false;
      debugPrint('remove members from group exception: $e');
      rethrow;
    }
  }

  @action
  leaveFromGroup({required int groupId}) async {
    isLoading = true;
    try {
      Response<SuccessResponse> response =
          await apiProvider.apiClient.groupsMembersLeaveGroupPut(
        groupId: groupId,
      );
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        isLoading = false;
        HttpExceptionNotifyUser.showMessage("You left from  the group");
        getIt<ProfileStore>().init();
        Navigator.pop(navigatorKey.currentContext!);
      } else {
        isLoading = false;
        return ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      isLoading = false;
      debugPrint('leave group exception: $e');
      rethrow;
    }
  }

  @action
  reportGroup({required int groupId, String? reason, String? message}) async {
    isLoading = true;
    try {
      if (groupBlockValue) {
        blockGroup(groupId: groupId);
      }
      Response<SuccessResponse> response = await apiProvider.apiClient
          .groupsReportIdPost(id: groupId, reason: reason, message: message);
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        isLoading = false;
        HttpExceptionNotifyUser.showMessage("Group reported");
        getIt<ProfileStore>().init();
        Navigator.pop(navigatorKey.currentContext!);
        Navigator.pop(navigatorKey.currentContext!);
      } else {
        isLoading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      isLoading = false;
      debugPrint('group block exception: $e');
      rethrow;
    }
  }

  @action
  blockGroup({required int groupId}) async {
    try {
      Response<SuccessResponse> response =
          await apiProvider.apiClient.groupsBlockIdPost(id: groupId);
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        debugPrint('group blocked');
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      debugPrint('group block exception: $e');
      rethrow;
    }
  }
}
