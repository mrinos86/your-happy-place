import 'dart:io';

import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:happy_place/api_utils/api_parser.dart';
import 'package:happy_place/api_utils/api_provider.dart';
import 'package:happy_place/api_utils/device_id_provider.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/injection.dart';
import 'package:injectable/injectable.dart';
import 'package:mobx/mobx.dart';

part 'notification_store.g.dart';

@injectable
class NotificationStore = _NotificationStore with _$NotificationStore;

abstract class _NotificationStore with Store {
  final ApiProvider apiProvider;
  _NotificationStore({
    required this.apiProvider,
  }) {
    getNotifications();
  }

  @observable
  List<PushNotificationResponse>? notificationsList;

  @observable
  bool isLoading = false;

  @action
  getNotifications() async {
    try {
      isLoading = true;
      Response<NotificationsIndexResponse> response =
          await apiProvider.apiClient.notificationsGet(
        xDeviceId: getIt<DeviceIdProvider>().deviceId.deviceId,
        xDeviceType: Platform.isIOS ? 'iOS' : 'Android',
      );
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        notificationsList = ApiSuccessParser.payloadOrThrow(response);
        debugPrint('notifications list length: ${notificationsList?.length}');
        if (notificationsList != null) {
          for (var v in notificationsList!) {
            debugPrint('value v: ${v.message}');
          }
        }
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
      isLoading = false;
    } catch (e) {
      isLoading = false;
      debugPrint('complete happy place exception: $e');
      rethrow;
    }
  }
}
