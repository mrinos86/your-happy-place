// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$NotificationStore on _NotificationStore, Store {
  final _$notificationsListAtom =
      Atom(name: '_NotificationStore.notificationsList');

  @override
  List<PushNotificationResponse>? get notificationsList {
    _$notificationsListAtom.reportRead();
    return super.notificationsList;
  }

  @override
  set notificationsList(List<PushNotificationResponse>? value) {
    _$notificationsListAtom.reportWrite(value, super.notificationsList, () {
      super.notificationsList = value;
    });
  }

  final _$isLoadingAtom = Atom(name: '_NotificationStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$getNotificationsAsyncAction =
      AsyncAction('_NotificationStore.getNotifications');

  @override
  Future getNotifications() {
    return _$getNotificationsAsyncAction.run(() => super.getNotifications());
  }

  @override
  String toString() {
    return '''
notificationsList: ${notificationsList},
isLoading: ${isLoading}
    ''';
  }
}
