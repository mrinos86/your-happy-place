import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_place/google_place.dart';
import 'package:happy_place/api_utils/api_parser.dart';
import 'package:happy_place/api_utils/api_provider.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/models/activity_selectable.dart';
import 'package:happy_place/models/happy_place.dart';
import 'package:happy_place/models/hexaco_selectable.dart';
import 'package:happy_place/models/mood_selectable.dart';
import 'package:happy_place/models/place_selectable.dart';
import 'package:happy_place/models/vibe_selectable.dart';
import 'package:happy_place/ui/explore/explore_page.dart';
import 'package:happy_place/ui/widgets/explore_map_marker.dart';
import 'package:injectable/injectable.dart';

import 'package:mobx/mobx.dart';

part 'explore_store.g.dart';

@injectable
class ExploreStore = _ExploreStore with _$ExploreStore;

abstract class _ExploreStore with Store {
  final ApiProvider apiProvider;

  _ExploreStore({
    required this.apiProvider,
  }) {
    init();
  }

  init() async {
    await getPosts();
  }

  String? errorMessage;
  bool alreadyFocused = false;

  Position? locationData;

  @observable
  bool isLoading = false;

  @observable
  int currentTab = 0;

  @observable
  int focusMyCurrentPosition = 0;

  @observable
  int markersUpdated = 0;

  @observable
  List<VisitedPlace> places = [];

  @observable
  List<ExploreMapMarker> markers = [];

  @observable
  Set<Marker> customMarkers = {};

  @observable
  int searchLocationUpdated = 0;

  @observable
  List<HappyPlace> placesAutoCompleteList = [];

  @observable
  bool loadingPlaces = false;

  @observable
  HappyPlace? searchLocationSelected;

  @action
  getPosts() async {
    debugPrint('getting places from expore store');
    try {
      Response<VisitedPlacesListResponse> response;

      // Setting up the explore by field.
      String exploreBy = "my";
      if (searchFamilyAndFriends) {
        exploreBy = 'others';
      } else if (searchMyEntries) {
        exploreBy = 'my';
      }

      // Setting up the moods field. This will contain all the ids for moods, activities and places.
      List<int> moodIds = [];
      List<int> acitivityIds = [];
      List<int> placeIds = [];
      List<int> vibeIds = [];
      List<String> hexacoIds = [];

      for (var element in moodsSelectable) {
        if (element.isSelected) {
          moodIds.add(element.mood.id!);
        }
      }
      for (var element in activitySelectable) {
        if (element.isSelected) {
          acitivityIds.add(int.parse(element.activity.id!));
        }
      }
      for (var element in placesSelectable) {
        if (element.isSelected) {
          placeIds.add(element.place.id!);
        }
      }
      for (var element in vibessSelectable) {
        if (element.isSelected) {
          vibeIds.add(element.feeling.id!);
        }
      }
      for (var element in hexacoSubSelectable) {
        if (element.isSelected) {
          hexacoIds.add(element.hexacoData.id!);
        }
      }

      // for filtering data
      String moodString = '';
      String placeString = '';
      String activityString = '';
      String vibeString = '';
      String hexacoString = '';

      if (moodIds.isNotEmpty) {
        moodString = moodIds.join(',');
      }
      if (placeIds.isNotEmpty) {
        placeString = placeIds.join(',');
      }
      if (acitivityIds.isNotEmpty) {
        activityString = acitivityIds.join(',');
      }
      if (vibeIds.isNotEmpty) {
        vibeString = vibeIds.join(',');
      }
      if (hexacoIds.isNotEmpty) {
        hexacoString = hexacoIds.join(',');
      }

      //  explore data filter by ....
      if (searchFamilyAndFriends || searchMyEntries) {
        response = await apiProvider.apiClient.visitedPlacesGet(
            moodId: moodString,
            activityId: activityString,
            placeId: placeString,
            feelingId: vibeString,
            exploreBy: exploreBy,
            hexacoId: hexacoString);
      } else {
        //explore all user data
        response = await apiProvider.apiClient.visitedPlacesGet(
          exploreBy: '',
          moodId: moodString,
          activityId: activityString,
          placeId: placeString,
          feelingId: vibeString,
          hexacoId: hexacoString,
        );
      }

      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        places = ApiSuccessParser.payloadOrThrow(response);
        markers.clear();
        for (var p in places) {
          //     final icon = await BitmapDescriptor.fromAssetImage(
          // ImageConfiguration(size: Size(24, 24)), 'assets/my_icon.png');
          debugPrint('we have a place: ${p.feeling?.imageUrl}');
          markers.add(ExploreMapMarker(p));
        }

        markers = markers;
        markersUpdated++;
        debugPrint('myposts length: ${places.length}');
      } else {
        if (isLoading) isLoading = false;
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
      debugPrint(response.toString());
    } catch (e) {
      debugPrint(e.toString());
      rethrow;
    }
  }

  @action
  getLocationPermission() async {
    try {
      bool serviceEnabled;
      LocationPermission permission;

      // Test if location services are enabled.
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        // Location services are not enabled don't continue
        // accessing the position and request users of the
        // App to enable the location services.
        return Future.error('Location services are disabled.');
      }

      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          // Permissions are denied, next time you could try
          // requesting permissions again (this is also where
          // Android's shouldShowRequestPermissionRationale
          // returned true. According to Android guidelines
          // your App should show an explanatory UI now.
          return Future.error('Location permissions are denied');
        }
      }

      if (permission == LocationPermission.deniedForever) {
        // Permissions are denied forever, handle appropriately.
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }

      // When we reach here, permissions are granted and we can
      // continue accessing the position of the device.
      locationData = await Geolocator.getCurrentPosition();

      if (!alreadyFocused) {
        alreadyFocused = true;
        focusMyCurrentPosition++;
      }
    } catch (e) {
      rethrow;
    }
  }

  @action
  searchAddressWithAutoComplete(String value) async {
    placesAutoCompleteList.clear();
    loadingPlaces = true;

    var googlePlace = GooglePlace(kGoogleApiKey);
    AutocompleteResponse? result = await googlePlace.autocomplete.get(value);

    if (result != null) {
      if (result.status == 'OK') {
        if (result.predictions?.isNotEmpty ?? false) {
          if (result.predictions != null) {
            await Future.forEach(result.predictions!,
                (AutocompletePrediction prediction) async {
              if (prediction.description != null) {
                double? latitude, longitude;

                if (prediction.placeId != null) {
                  DetailsResponse? placeDetails =
                      await googlePlace.details.get(prediction.placeId!);
                  latitude = placeDetails?.result?.geometry?.location?.lat;
                  longitude = placeDetails?.result?.geometry?.location?.lng;

                  if (latitude != null && longitude != null) {
                    placesAutoCompleteList.add(
                      HappyPlace(
                          description: prediction.description!,
                          latitude: latitude,
                          longitude: longitude),
                    );
                  }
                }
              }
            });
            placesAutoCompleteList = placesAutoCompleteList;
            loadingPlaces = false;
          }
        }
      }
    }
  }

  /* 
  For filters page
  */

  @observable
  bool loadingFilterOptions = false;

  @observable
  bool searchAll = false;

  @observable
  bool searchMyEntries = false;

  @observable
  bool searchFamilyAndFriends = false;

  @observable
  ObservableList<ActivitySelectable> activitySelectable = ObservableList.of([]);

  @observable
  ObservableList<PlaceSelectable> placesSelectable = ObservableList.of([]);

  @observable
  ObservableList<MoodSelectable> moodsSelectable = ObservableList.of([]);

  @observable
  ObservableList<VibeSelectable> vibessSelectable = ObservableList.of([]);

  @observable
  ObservableList<HexacoSelectable> hexacoSelectable = ObservableList.of([]);

  @observable
  ObservableList<HexacoSubSelectable> hexacoSubSelectable =
      ObservableList.of([]);

  @action
  loadFilters() async {
    loadingFilterOptions = true;
    await getMoods();
    await getPlaces();
    await getActivities();
    await getVibes();
    await getHexaco();
    loadingFilterOptions = false;
  }

  @action
  getMoods() async {
    try {
      Response<MoodsListResponse> response =
          await apiProvider.apiClient.moodsGet();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        moodsSelectable.clear();
        List<Mood>? moods = ApiSuccessParser.payloadOrThrow(response);
        if (moods != null) {
          for (var m in moods) {
            moodsSelectable.add(MoodSelectable(mood: m, isSelected: false));
          }
        }
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      debugPrint('get Feelings exception: $e');
    }
  }

  @action
  getPlaces() async {
    try {
      Response<PlacesListResponse> response =
          await apiProvider.apiClient.placesGet();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        placesSelectable.clear();
        List<Place>? places = ApiSuccessParser.payloadOrThrow(response);
        if (places != null) {
          for (var m in places) {
            placesSelectable.add(PlaceSelectable(place: m, isSelected: false));
          }
        }
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      debugPrint('get Places exception: $e');
    }
  }

  @action
  getActivities() async {
    try {
      Response<ActivitiesListResponse> response =
          await apiProvider.apiClient.activitiesGet();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        activitySelectable.clear();
        List<Activity>? activities = ApiSuccessParser.payloadOrThrow(response);
        if (activities != null) {
          for (var m in activities) {
            activitySelectable
                .add(ActivitySelectable(activity: m, isSelected: false));
          }
        }
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      debugPrint('get Activities exception: $e');
    }
  }

  @action
  getVibes() async {
    try {
      Response<FeelingsListResponse> response =
          await apiProvider.apiClient.feelingsGet();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        vibessSelectable.clear();
        List<Feeling>? feelings = ApiSuccessParser.payloadOrThrow(response);
        if (feelings != null) {
          for (var m in feelings) {
            vibessSelectable.add(VibeSelectable(feeling: m, isSelected: false));
          }
        }
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      debugPrint('get feeling/vibe exception: $e');
    }
  }

  @action
  getHexaco() async {
    try {
      Response<HexacoHexacoListPeopleLikeMeFilterResponse> response =
          await apiProvider.apiClient.hexacoGet();
      if (ApiSuccessParser.isSuccessfulWithPayload(response)) {
        hexacoSelectable.clear();
        hexacoSubSelectable.clear();
        List<HexacoResponse>? hexaco =
            ApiSuccessParser.payloadOrThrow(response);
        if (hexaco != null) {
          for (HexacoResponse hex in hexaco) {
            hexacoSelectable
                .add(HexacoSelectable(hexaco: hex, isSelected: false));

            for (HexacoData hexacosub in hex.subCategory!) {
              hexacoSubSelectable.add(HexacoSubSelectable(
                  hexacoId: hex.id!, hexacoData: hexacosub, isSelected: false));
            }
          }
        }
      } else {
        ApiSuccessParser.isSuccessfulOrThrowWithMessage(response);
      }
    } catch (e) {
      debugPrint('get hexaco exception: $e');
    }
  }

  @action
  refreshList() {
    moodsSelectable = moodsSelectable;
    placesSelectable = placesSelectable;
    activitySelectable = activitySelectable;
    vibessSelectable = vibessSelectable;
    hexacoSelectable = hexacoSelectable;
    hexacoSubSelectable = hexacoSubSelectable;
  }

  @action
  clickedHexaco({required String hexacoId, required bool value}) {
    for (var element in hexacoSubSelectable) {
      if (hexacoId == element.hexacoId) {
        int index = hexacoSubSelectable.indexOf(element);
        hexacoSubSelectable[index].isSelected = value;
      }
    }
    hexacoSubSelectable = hexacoSubSelectable;
  }
}
