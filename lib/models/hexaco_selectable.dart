import 'package:happy_place/generated_api_code/api_client.swagger.dart';

class HexacoSelectable {
  HexacoResponse hexaco;
  bool isSelected;
  HexacoSelectable({
    required this.hexaco,
    required this.isSelected,
  });
}

class HexacoSubSelectable {
  String hexacoId;
  HexacoData hexacoData;
  bool isSelected;
  HexacoSubSelectable({
    required this.hexacoId,
    required this.hexacoData,
    required this.isSelected,
  });
}
