import 'package:happy_place/generated_api_code/api_client.swagger.dart';

class VibeSelectable {
  Feeling feeling;
  bool isSelected;
  VibeSelectable({
    required this.feeling,
    required this.isSelected,
  });
}
