import 'package:happy_place/generated_api_code/api_client.swagger.dart';

class GroupWithWrapper {
  Group group;
  bool checked;
  GroupWithWrapper({
    required this.group,
    required this.checked,
  });
}

class GroupMemberWithWrapper {
  GroupMember groupMember;
  bool checked;
  GroupMemberWithWrapper({
    required this.groupMember,
    required this.checked,
  });
}
