// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i9;

import 'api_utils/api_provider.dart' as _i15;
import 'api_utils/device_id_provider.dart' as _i76;
import 'api_utils/user_provider.dart' as _i47;
import 'config/api_settings.dart' as _i3;
import 'data/stores/add_happy_places/add_new_happy_place_store.dart' as _i37;
import 'data/stores/add_happy_places/how_are_you_generic_store.dart' as _i20;
import 'data/stores/add_happy_places/places_history_store.dart' as _i25;
import 'data/stores/add_happy_places/were_your_needs_met_store.dart' as _i36;
import 'data/stores/auth/forgot_password_store.dart' as _i18;
import 'data/stores/auth/login_store.dart' as _i21;
import 'data/stores/auth/signup_store.dart' as _i31;
import 'data/stores/dashboard_store.dart' as _i5;
import 'data/stores/explore/explore_store.dart' as _i17;
import 'data/stores/home_store.dart' as _i19;
import 'data/stores/my_happiness_store/my_happiness_store.dart' as _i22;
import 'data/stores/my_happiness_store/test_store.dart' as _i33;
import 'data/stores/my_places_post/add_place_store.dart' as _i39;
import 'data/stores/my_places_post/my_places_store.dart' as _i23;
import 'data/stores/my_places_post/select_friend_share_post_store.dart' as _i27;
import 'data/stores/my_places_post/share_post_store.dart' as _i30;
import 'data/stores/my_places_post/view_comment_store.dart' as _i34;
import 'data/stores/my_places_post/view_posts_store.dart' as _i35;
import 'data/stores/navigation_store.dart' as _i13;
import 'data/stores/notification_store.dart' as _i24;
import 'data/stores/profile/add_people_store.dart' as _i38;
import 'data/stores/profile/change_password_store.dart' as _i14;
import 'data/stores/profile/edit_profile_store.dart' as _i16;
import 'data/stores/profile/group_store.dart' as _i45;
import 'data/stores/profile/profile_store.dart' as _i48;
import 'data/stores/profile/requests_store.dart' as _i26;
import 'data/stores/settings_store.dart' as _i29;
import 'ui/add_new_happy_place/add_new_happy_place.dart' as _i65;
import 'ui/add_new_happy_place/how_are_you_generic.dart' as _i49;
import 'ui/add_new_happy_place/were_your_needs_met.dart' as _i63;
import 'ui/authentication/forgot_password_page.dart' as _i43;
import 'ui/authentication/login_page.dart' as _i50;
import 'ui/authentication/signup_page.dart' as _i32;
import 'ui/dashboard/dashboard_page.dart' as _i12;
import 'ui/explore/explore_page.dart' as _i42;
import 'ui/explore/filters_page.dart' as _i6;
import 'ui/home/home_page.dart' as _i46;
import 'ui/home/profile_completion.dart' as _i4;
import 'ui/my_happiness/big_five_result_page.dart' as _i68;
import 'ui/my_happiness/big_five_test_page.dart' as _i69;
import 'ui/my_happiness/happiness_entry_category_page.dart' as _i72;
import 'ui/my_happiness/happiness_entry_page.dart' as _i73;
import 'ui/my_happiness/hexaco_result_page.dart' as _i74;
import 'ui/my_happiness/hexaco_test_page.dart' as _i75;
import 'ui/my_happiness/my_happiness_page.dart' as _i51;
import 'ui/my_places/add_place_page.dart' as _i67;
import 'ui/my_places/comment_report_page.dart' as _i71;
import 'ui/my_places/my_places_page.dart' as _i52;
import 'ui/my_places/select_friends_share_page.dart' as _i28;
import 'ui/my_places/share_post_page.dart' as _i58;
import 'ui/my_places/view_comment_page.dart' as _i59;
import 'ui/my_places/view_posts_page.dart' as _i62;
import 'ui/places_history/places_history_page.dart' as _i54;
import 'ui/places_history/view_note_page.dart' as _i11;
import 'ui/profile/screens/add_people.dart' as _i66;
import 'ui/profile/screens/change_password_page.dart' as _i40;
import 'ui/profile/screens/edit_profile_page.dart' as _i41;
import 'ui/profile/screens/group/add_group_member_page.dart' as _i64;
import 'ui/profile/screens/group/block_or_report_page.dart' as _i70;
import 'ui/profile/screens/group/create_edit_group_page.dart' as _i44;
import 'ui/profile/screens/group/view_group_page.dart' as _i60;
import 'ui/profile/screens/profile_page.dart' as _i55;
import 'ui/profile/screens/requests_page.dart' as _i56;
import 'ui/profile/screens/view_other_profile_page.dart' as _i61;
import 'ui/settings/information_page.dart' as _i7;
import 'ui/settings/notification_page.dart' as _i53;
import 'ui/settings/privacy_policy_page.dart' as _i8;
import 'ui/settings/settings_page.dart' as _i57;
import 'ui/settings/terms_and_condtion_page.dart' as _i10;

const String _awsStaging = 'awsStaging';
const String _prod = 'prod';
const String _sandbox = 'sandbox';
const String _dev = 'dev';
// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) async {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final sharedPreferencesModule = _$SharedPreferencesModule();
  gh.factory<_i3.ApiSettings>(() => _i3.AwsStagingApiSettings(),
      registerFor: {_awsStaging});
  gh.factory<_i3.ApiSettings>(() => _i3.ProdApiSettings(),
      registerFor: {_prod});
  gh.factory<_i3.ApiSettings>(() => _i3.SandboxApiSettings(),
      registerFor: {_sandbox});
  gh.factory<_i3.ApiSettings>(() => _i3.DevApiSettings(), registerFor: {_dev});
  gh.factoryParam<_i4.CompleteProfilePage, _i4.CompleteProfilePageParams?,
          dynamic>(
      (params, _) => _i4.CompleteProfilePage(
          params: params, services: get<_i4.CompleteProfileServices>()));
  gh.factory<_i4.CompleteProfileServices>(() => _i4.CompleteProfileServices());
  gh.factory<_i5.DashboardStore>(() => _i5.DashboardStore());
  gh.factoryParam<_i6.FiltersPage, _i6.FiltersPageParams?, dynamic>(
      (params, _) => _i6.FiltersPage(params: params));
  gh.factoryParam<_i7.InformationPage, _i7.InformationPageParams?, dynamic>(
      (params, _) => _i7.InformationPage(params: params));
  gh.factoryParam<_i8.PrivacyPolicyPage, _i8.PrivacyPolicyPageParams?, dynamic>(
      (params, _) => _i8.PrivacyPolicyPage(params: params));
  await gh.factoryAsync<_i9.SharedPreferences>(
      () => sharedPreferencesModule.sharedPreferences,
      preResolve: true);
  gh.factoryParam<_i10.TermsAndCondtionPage, _i10.TermsAndCondtionPageParams?,
      dynamic>((param, _) => _i10.TermsAndCondtionPage(param: param));
  gh.factoryParam<_i11.ViewNotePage, _i11.ViewNotePageParams?, dynamic>(
      (params, _) => _i11.ViewNotePage(params: params));
  gh.factory<_i12.DashboardServices>(() => _i12.DashboardServices(
      store: get<_i5.DashboardStore>(),
      navigationStore: get<_i13.NavigationStore>()));
  gh.factory<_i14.ChangePasswordStore>(
      () => _i14.ChangePasswordStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factoryParam<_i12.DashboardPage, _i12.DashboardParams?, dynamic>(
      (params, _) => _i12.DashboardPage(
          params: params, services: get<_i12.DashboardServices>()));
  gh.factory<_i16.EditProfileStore>(
      () => _i16.EditProfileStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i17.ExploreStore>(
      () => _i17.ExploreStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i18.ForgotPasswordStore>(
      () => _i18.ForgotPasswordStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i19.HomeStore>(
      () => _i19.HomeStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i20.HowAreYouGenericStore>(
      () => _i20.HowAreYouGenericStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i21.LoginStore>(
      () => _i21.LoginStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i22.MyHappinessStore>(
      () => _i22.MyHappinessStore(get<_i15.ApiProvider>()));
  gh.factory<_i23.MyPlacesStore>(
      () => _i23.MyPlacesStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i24.NotificationStore>(
      () => _i24.NotificationStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i25.PlacesHistoryStore>(
      () => _i25.PlacesHistoryStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i26.RequestsStore>(
      () => _i26.RequestsStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i27.SelectFriendSharePostStore>(() =>
      _i27.SelectFriendSharePostStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i28.SelectFriendsServices>(() => _i28.SelectFriendsServices(
      store: get<_i27.SelectFriendSharePostStore>()));
  gh.factory<_i29.SettingsStore>(
      () => _i29.SettingsStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i30.SharePostStore>(
      () => _i30.SharePostStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i31.SignupStore>(
      () => _i31.SignupStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i32.SingupPageServices>(
      () => _i32.SingupPageServices(store: get<_i31.SignupStore>()));
  gh.factory<_i33.TestStore>(() => _i33.TestStore(get<_i15.ApiProvider>()));
  gh.factory<_i34.ViewCommentStore>(
      () => _i34.ViewCommentStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i35.ViewPostsStore>(
      () => _i35.ViewPostsStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i36.WereYourNeedsMetStore>(
      () => _i36.WereYourNeedsMetStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i37.AddNewHappyPlaceStore>(
      () => _i37.AddNewHappyPlaceStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i38.AddPeopleStore>(
      () => _i38.AddPeopleStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i39.AddPlaceStore>(
      () => _i39.AddPlaceStore(apiProvider: get<_i15.ApiProvider>()));
  gh.factory<_i40.ChangePasswordPageServices>(() =>
      _i40.ChangePasswordPageServices(store: get<_i14.ChangePasswordStore>()));
  gh.factory<_i41.EditProfilePageServices>(
      () => _i41.EditProfilePageServices(store: get<_i16.EditProfileStore>()));
  gh.factory<_i42.ExploreServices>(
      () => _i42.ExploreServices(store: get<_i17.ExploreStore>()));
  gh.factory<_i43.ForgotPasswordPageServices>(() =>
      _i43.ForgotPasswordPageServices(store: get<_i18.ForgotPasswordStore>()));
  gh.factory<_i44.GroupServices>(
      () => _i44.GroupServices(store: get<_i45.GroupStore>()));
  gh.factory<_i46.HomePageServices>(() => _i46.HomePageServices(
      userProvider: get<_i47.UserProvider>(),
      navigationStore: get<_i13.NavigationStore>(),
      profileStore: get<_i48.ProfileStore>()));
  gh.factory<_i49.HowAreYouGenericPageServices>(() =>
      _i49.HowAreYouGenericPageServices(
          store: get<_i20.HowAreYouGenericStore>()));
  gh.factory<_i50.LoginPageServices>(
      () => _i50.LoginPageServices(loginStore: get<_i21.LoginStore>()));
  gh.factory<_i51.MyHappinessPageServices>(
      () => _i51.MyHappinessPageServices(store: get<_i22.MyHappinessStore>()));
  gh.factory<_i52.MyPlacesServices>(
      () => _i52.MyPlacesServices(store: get<_i23.MyPlacesStore>()));
  gh.factory<_i53.NotificationPageServices>(() =>
      _i53.NotificationPageServices(store: get<_i24.NotificationStore>()));
  gh.factory<_i54.PlacesHistoryPageServices>(() =>
      _i54.PlacesHistoryPageServices(store: get<_i25.PlacesHistoryStore>()));
  gh.factory<_i55.ProfilePageServices>(
      () => _i55.ProfilePageServices(store: get<_i48.ProfileStore>()));
  gh.factory<_i56.RequestsPageServices>(
      () => _i56.RequestsPageServices(store: get<_i26.RequestsStore>()));
  gh.factoryParam<_i28.SelectFriendsPage, _i28.SelectFriendsParams?, dynamic>(
      (params, _) => _i28.SelectFriendsPage(
          params: params, services: get<_i28.SelectFriendsServices>()));
  gh.factory<_i57.SettingsPageServices>(
      () => _i57.SettingsPageServices(store: get<_i29.SettingsStore>()));
  gh.factory<_i58.SharePostServices>(
      () => _i58.SharePostServices(store: get<_i30.SharePostStore>()));
  gh.factoryParam<_i32.SignUpPage, _i32.SingupPageParams?, dynamic>(
      (params, _) => _i32.SignUpPage(
          params: params, services: get<_i32.SingupPageServices>()));
  gh.factory<_i51.TestPageServices>(
      () => _i51.TestPageServices(store: get<_i33.TestStore>()));
  gh.factory<_i59.ViewCommentServices>(() => _i59.ViewCommentServices(
      store: get<_i34.ViewCommentStore>(),
      postStore: get<_i35.ViewPostsStore>()));
  gh.factoryParam<_i60.ViewGroupPage, _i60.ViewGroupPageParams?, dynamic>(
      (params, _) => _i60.ViewGroupPage(
          params: params, services: get<_i44.GroupServices>()));
  gh.factoryParam<_i61.ViewOtherProfilePage, _i61.ViewOtherProfilePageParams?,
          dynamic>(
      (params, _) => _i61.ViewOtherProfilePage(
          params: params, services: get<_i56.RequestsPageServices>()));
  gh.factory<_i62.ViewPostsServices>(
      () => _i62.ViewPostsServices(store: get<_i35.ViewPostsStore>()));
  gh.factory<_i63.WereYourNeedsMetPageServices>(() =>
      _i63.WereYourNeedsMetPageServices(
          store: get<_i36.WereYourNeedsMetStore>()));
  gh.factoryParam<_i64.AddGroupMemberPage, _i64.AddGroupMemberPageParams?,
          dynamic>(
      (params, _) => _i64.AddGroupMemberPage(
          params: params, services: get<_i44.GroupServices>()));
  gh.factory<_i65.AddNewHappyPlacePageServices>(() =>
      _i65.AddNewHappyPlacePageServices(
          store: get<_i37.AddNewHappyPlaceStore>()));
  gh.factory<_i66.AddPeoplePageServices>(
      () => _i66.AddPeoplePageServices(store: get<_i38.AddPeopleStore>()));
  gh.factory<_i67.AddPlaceServices>(
      () => _i67.AddPlaceServices(store: get<_i39.AddPlaceStore>()));
  gh.factoryParam<_i68.BigFiveResultPage, _i51.TestPageParams?, dynamic>(
      (params, _) => _i68.BigFiveResultPage(
          params: params, services: get<_i51.TestPageServices>()));
  gh.factoryParam<_i69.BigFiveTestPage, _i51.TestPageParams?, dynamic>(
      (params, _) => _i69.BigFiveTestPage(
          params: params, services: get<_i51.TestPageServices>()));
  gh.factoryParam<_i70.BlockOrReportPage, _i70.BlockOrReportParams?, dynamic>(
      (params, _) => _i70.BlockOrReportPage(
          params: params, services: get<_i44.GroupServices>()));
  gh.factoryParam<_i40.ChangePasswordPage, _i40.ChangePasswordPageParams?,
          dynamic>(
      (params, _) => _i40.ChangePasswordPage(
          params: params, services: get<_i40.ChangePasswordPageServices>()));
  gh.factoryParam<_i71.CommentReportPage, _i71.ReportParams?, dynamic>(
      (params, _) => _i71.CommentReportPage(
          params: params, services: get<_i59.ViewCommentServices>()));
  gh.factoryParam<_i44.CreateEditGroupPage, _i44.CreateEditPageParams?,
          dynamic>(
      (params, _) => _i44.CreateEditGroupPage(
          params: params, services: get<_i44.GroupServices>()));
  gh.factoryParam<_i41.EditProfilePage, _i41.EditProfilePageParams?, dynamic>(
      (params, _) => _i41.EditProfilePage(
          params: params, services: get<_i41.EditProfilePageServices>()));
  gh.factoryParam<_i42.ExplorePage, _i42.ExplorePageParams?, dynamic>((params,
          _) =>
      _i42.ExplorePage(params: params, services: get<_i42.ExploreServices>()));
  gh.factoryParam<_i43.ForgotPasswordPage, _i43.ForgotPasswordPageParams?,
          dynamic>(
      (params, _) => _i43.ForgotPasswordPage(
          params: params, services: get<_i43.ForgotPasswordPageServices>()));
  gh.factoryParam<_i72.HappinessEntryCategoryPage,
          _i72.HappinessEntryPageParam?, dynamic>(
      (params, _) => _i72.HappinessEntryCategoryPage(
          params: params, services: get<_i51.TestPageServices>()));
  gh.factoryParam<_i73.HappinessEntryPage, _i51.TestPageParams?, dynamic>(
      (params, _) => _i73.HappinessEntryPage(
          params: params, services: get<_i51.TestPageServices>()));
  gh.factoryParam<_i74.HexacoResultPage, _i51.TestPageParams?, dynamic>(
      (params, _) => _i74.HexacoResultPage(
          params: params, services: get<_i51.TestPageServices>()));
  gh.factoryParam<_i75.HexacoTestPage, _i51.TestPageParams?, dynamic>(
      (params, _) => _i75.HexacoTestPage(
          params: params, services: get<_i51.TestPageServices>()));
  gh.factoryParam<_i46.HomePage, _i46.HomePageParams?, dynamic>((params, _) =>
      _i46.HomePage(params: params, services: get<_i46.HomePageServices>()));
  gh.factoryParam<_i49.HowAreYouGenericPage, _i49.HowAreYouGenericPageParams?,
          dynamic>(
      (params, _) => _i49.HowAreYouGenericPage(
          params: params, services: get<_i49.HowAreYouGenericPageServices>()));
  gh.factoryParam<_i50.LoginPage, _i50.LoginPageParams?, dynamic>((params, _) =>
      _i50.LoginPage(params: params, services: get<_i50.LoginPageServices>()));
  gh.factoryParam<_i51.MyHappinessPage, _i51.MyHappinessPageParams?, dynamic>(
      (params, _) => _i51.MyHappinessPage(
          params: params, services: get<_i51.MyHappinessPageServices>()));
  gh.factoryParam<_i52.MyPlacesPage, _i52.MyPlacesParams?, dynamic>(
      (params, _) => _i52.MyPlacesPage(
          params: params, services: get<_i52.MyPlacesServices>()));
  gh.factoryParam<_i53.NotificationPage, _i53.NotificationPageParams?, dynamic>(
      (params, _) => _i53.NotificationPage(
          params: params, services: get<_i53.NotificationPageServices>()));
  gh.factoryParam<_i54.PlacesHistoryPage, _i54.PlaceHistoryPageParams?,
          dynamic>(
      (params, _) => _i54.PlacesHistoryPage(
          params: params, services: get<_i54.PlacesHistoryPageServices>()));
  gh.factoryParam<_i55.ProfilePage, _i55.ProfilePageParams?, dynamic>(
      (params, _) => _i55.ProfilePage(
          params: params,
          services: get<_i55.ProfilePageServices>(),
          groupStore: get<_i45.GroupStore>()));
  gh.factoryParam<_i56.RequestsPage, _i56.RequestsPageParams?, dynamic>(
      (params, _) => _i56.RequestsPage(
          params: params, services: get<_i56.RequestsPageServices>()));
  gh.factoryParam<_i57.SettingsPage, _i57.SettingsPageParams?, dynamic>(
      (params, _) => _i57.SettingsPage(
          params: params, services: get<_i57.SettingsPageServices>()));
  gh.factoryParam<_i58.SharePostPage, _i58.SharePostParams?, dynamic>(
      (params, _) => _i58.SharePostPage(
          params: params, services: get<_i58.SharePostServices>()));
  gh.factoryParam<_i59.ViewCommentPage, _i59.ViewCommentPageParams?, dynamic>(
      (params, _) => _i59.ViewCommentPage(
          params: params, services: get<_i59.ViewCommentServices>()));
  gh.factoryParam<_i62.ViewPostsPage, _i62.ViewPostsParams?, dynamic>(
      (params, _) => _i62.ViewPostsPage(
          params: params, services: get<_i62.ViewPostsServices>()));
  gh.factoryParam<_i63.WereYourNeedsMetPage, _i63.WereYourNeedsMetPageParams?,
          dynamic>(
      (params, _) => _i63.WereYourNeedsMetPage(
          params: params, services: get<_i63.WereYourNeedsMetPageServices>()));
  gh.factoryParam<_i65.AddNewHappyPlacePage, _i65.AddNewHappyPlacePageParams?,
          dynamic>(
      (params, _) => _i65.AddNewHappyPlacePage(
          params: params, services: get<_i65.AddNewHappyPlacePageServices>()));
  gh.factoryParam<_i66.AddPeoplePage, _i66.AddPeoplePageParams?, dynamic>(
      (params, _) => _i66.AddPeoplePage(
          params: params, services: get<_i66.AddPeoplePageServices>()));
  gh.factoryParam<_i67.AddPlacePage, _i67.AddPlaceParams?, dynamic>(
      (params, _) => _i67.AddPlacePage(
          params: params, services: get<_i67.AddPlaceServices>()));
  gh.singleton<_i13.NavigationStore>(_i13.NavigationStore());
  gh.singleton<_i76.DeviceIdProvider>(
      _i76.DeviceIdProvider(get<_i9.SharedPreferences>()));
  await gh.singletonAsync<_i47.UserProvider>(
      () => _i47.UserProvider.create(
          get<_i9.SharedPreferences>(), get<_i76.DeviceIdProvider>()),
      preResolve: true);
  gh.singleton<_i15.ApiProvider>(_i15.ApiProvider(
      apiSettings: get<_i3.ApiSettings>(),
      userProvider: get<_i47.UserProvider>()));
  gh.singleton<_i45.GroupStore>(
      _i45.GroupStore(apiProvider: get<_i15.ApiProvider>()));
  gh.singleton<_i48.ProfileStore>(
      _i48.ProfileStore(apiProvider: get<_i15.ApiProvider>()));
  return get;
}

class _$SharedPreferencesModule extends _i47.SharedPreferencesModule {}
