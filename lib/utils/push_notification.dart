import 'dart:async';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:happy_place/injection.dart';
import 'package:happy_place/main.dart';
import 'package:happy_place/ui/profile/screens/requests_page.dart';

class PushNotificationService {
  static PushNotificationService? _instance;

  final StreamController<Map<String, dynamic>> _streamController =
      StreamController.broadcast();

  Stream<Map<String, dynamic>> get onNotification => _streamController.stream;

  /// Create a [AndroidNotificationChannel] for heads up notifications
  late AndroidNotificationChannel channel;

  /// Initialize the [FlutterLocalNotificationsPlugin] package.
  late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  PushNotificationService._() {
    _initialise();
  }

  static PushNotificationService initialize() {
    if (_instance == null) {
      _instance = PushNotificationService._();
    } else {
      throw Exception("Push Notification Service already initialized");
    }
    return _instance!;
  }

  static PushNotificationService get instance {
    if (_instance == null) {
      throw Exception("PushNotificationService not initialized");
    }
    return _instance!;
  }

  ///////////////////// iOS push notification..................
  final FirebaseMessaging _fcm = FirebaseMessaging.instance;

  List<String> userNotificationsList = <String>[];

  Future _initialise() async {
    if (Platform.isIOS) {
      await _fcm.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );
    }

    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if (message != null) {}
    });

///////////////////// iOS push notification..................

    if (!kIsWeb) {
      channel = const AndroidNotificationChannel(
        'high_importance_channel', // id
        'High Importance Notifications', // title
        description: 'This channel is used for important notifications.',
        // description
        importance: Importance.high,
      );

      flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

      /// Create an Android Notification Channel.
      ///
      /// We use this channel in the `AndroidManifest.xml` file to override the
      /// default FCM channel to enable heads up notifications.
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(channel);

      /// Update the iOS foreground notification presentation options to allow
      /// heads up notifications.
      await FirebaseMessaging.instance
          .setForegroundNotificationPresentationOptions(
        alert: true,
        badge: true,
        sound: true,
      );
    }

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      if (message.notification!.title == "New friend request." ||
          message.notification!.title == "New family request.") {
        Navigator.push(navigatorKey.currentContext!,
            MaterialPageRoute(builder: (context) => getIt<RequestsPage>()));
      }

      // Navigator.pushNamed(navigatorKey.currentContext!, Routes.notification);
    });

    FirebaseMessaging.instance.getToken().then((value) {
      String? token = value;
      debugPrint(token.toString());
    });

    ///forground work
    FirebaseMessaging.onMessage.listen((message) async {
      if (message.notification != null) {
        debugPrint(message.notification!.body);
        debugPrint(message.notification!.title);
      }

      RemoteNotification? notification = message.notification;
      if (notification != null) {
        flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channelDescription: channel.description,
              icon: 'ic_foreground',
            ),
          ),
        );
      }
    });
  }

  Future<String> getDevicePushToken() async {
    String token = '';

    await _fcm.getToken().then((value) {
      token = value!;
    });

    return token;
  }
}

Future<void> backgroundHandler(RemoteMessage message) async {
  if (message.notification != null) {
    debugPrint(message.notification!.body);
    debugPrint(message.notification!.title);
  }
}
