enum ShareTabType { friends, family, groups }

enum PostBottomButtonsType { like, share, comment, edit }

enum PostType { my, shared, feed }
