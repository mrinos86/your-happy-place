import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:happy_place/api_utils/user_provider.dart';
import 'package:happy_place/generated/l10n.dart';
import 'package:happy_place/injection.dart';
import 'package:happy_place/ui/authentication/login_page.dart';
import 'package:happy_place/ui/home/home_page.dart';
import 'package:happy_place/ui/theme/app_theme.dart';
import 'package:happy_place/utils/push_notification.dart';
import 'package:injectable/injectable.dart';
import 'package:logging/logging.dart';

import 'generated_api_code/api_client.swagger.dart';

// TODO - Localise completely

void setLoggingLevelAll() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((rec) {
    debugPrint('${rec.level.name}: ${rec.time}: ${rec.message}');
  });
}

GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // TODO:  change for live server
  await configureInjection(prod.name);
  setLoggingLevelAll();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(backgroundHandler);
  PushNotificationService.initialize();
  final home = await getHome();
  runApp(MyApp(home));
}

class MyApp extends StatelessWidget {
  final Widget home;
  const MyApp(this.home, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      debugShowCheckedModeBanner: false,
      theme: AppTheme.getTheme(context),
      home: home,
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
    );
  }
}

Future<Widget> getHome() async {
  final userProvider = getIt<UserProvider>();
  final user = await userProvider.getUserOnAppStart();
  return getHomeFromUser(user);
}

Widget getHomeFromUser(User? user) {
  Widget home = getIt<LoginPage>();
  if (user != null) {
    home = getIt<HomePage>();
  }
  return home;
}
