// ignore_for_file: implementation_imports

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/ui/my_places/view_comment_page.dart';
import 'package:happy_place/ui/widgets/app_button.dart';
import 'package:happy_place/ui/widgets/app_edit_text.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:injectable/injectable.dart';
import 'package:mobx/mobx.dart';

class ReportParams {
  const ReportParams({
    this.key,
    required this.comment,
  });
  final Key? key;
  final Comment comment;
}

@injectable
class CommentReportPage extends StatefulWidget {
  final ViewCommentServices services;
  final ReportParams? params;

  CommentReportPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<CommentReportPage> createState() => _CommentReportPageState();
}

class _CommentReportPageState extends State<CommentReportPage> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  late List<ReactionDisposer> _disposers;

  DropdownItems? _currentSelectedValue;

  List<DropdownItems> dropdownItemList = [
    DropdownItems(id: 1, title: "Harassment or bullying"),
    DropdownItems(id: 2, title: "Hateful or abusive content"),
    DropdownItems(id: 3, title: "Violent or repulsive content"),
    DropdownItems(id: 4, title: "Harmful or dangerous acts"),
    DropdownItems(id: 5, title: "Sexual content"),
    DropdownItems(id: 6, title: "Child abuse")
  ];

  final TextEditingController _messageTextController = TextEditingController();

  @override
  void initState() {
    _disposers = [
      reaction(
        (_) => widget.services.store.reported,
        (bool result) async {
          if (result) {
            Navigator.pop(context, true);
          }
        },
      ),
    ];
    super.initState();
  }

  @override
  void dispose() {
    for (final d in _disposers) {
      d();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text('Report User'),
        centerTitle: true,
      ),
      body: Observer(builder: (context) {
        if (widget.services.store.isLoading) {
          return const Loader();
        }
        return Container(
          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                const SizedBox(height: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "Select a Reason",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: 2),
                    DropdownButtonFormField<DropdownItems>(
                      icon: const Icon(Icons.keyboard_arrow_down,
                          size: 30, color: Colors.grey),
                      isDense: true,
                      items: dropdownItemList.map((DropdownItems entityType) {
                        return DropdownMenuItem<DropdownItems>(
                          value: entityType,
                          child: Text(entityType.title),
                        );
                      }).toList(),
                      onChanged: (DropdownItems? newValue) {
                        _currentSelectedValue = newValue!;
                      },
                      value: _currentSelectedValue,
                      decoration: const InputDecoration(
                        // filled: true,
                        border: OutlineInputBorder(),
                        fillColor: Colors.white,
                        hintText: 'Select a Reason',
                        hintStyle: TextStyle(color: Colors.grey),
                        contentPadding: EdgeInsets.only(
                            left: 20, right: 10, top: 15, bottom: 15),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 15),
                AppEditText(
                    textController: _messageTextController,
                    headingText: 'Message (Optional)',
                    hint: "",
                    isMultiLine: true,
                    filled: false,
                    // validator: (value) {
                    //   if (value == null || value.trim().isEmpty) {
                    //     return S.of(context).enter_post_caption;
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    borderColor: Colors.grey.shade400,
                    radius: 2),
                const SizedBox(height: 24),
                AppButton(
                    title: 'Report',
                    onPressed: () {
                      String _reason = "";
                      if (_currentSelectedValue != null) {
                        _reason = _currentSelectedValue!.title;
                      }
                      widget.services.store.reportCommentedUser(
                        userId: int.parse(widget.params!.comment.userId!),
                        commentId: int.parse(widget.params!.comment.id!),
                        reason: _reason,
                        message: _messageTextController.text,
                      );
                    }),
              ],
            ),
          ),
        );
      }),
    );
  }

  void onPressedReportButton() {}
}

class DropdownItems {
  DropdownItems({
    required this.id,
    required this.title,
  });
  int id;
  String title;
}
