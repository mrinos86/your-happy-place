import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/data/stores/my_places_post/view_comment_store.dart';
import 'package:happy_place/data/stores/my_places_post/view_posts_store.dart';
import 'package:happy_place/injection.dart';
import 'package:happy_place/ui/my_places/comment_report_page.dart';
import 'package:happy_place/ui/my_places/widgets/comment_card.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:injectable/injectable.dart';
import 'package:mobx/mobx.dart';

import 'widgets/comment_send_widget.dart';

class ViewCommentPageParams {
  ViewCommentPageParams({this.key, required this.postId});
  final Key? key;
  final int postId;
}

@injectable
class ViewCommentServices {
  ViewCommentStore store;
  ViewPostsStore postStore;

  ViewCommentServices({required this.store, required this.postStore});
}

@injectable
class ViewCommentPage extends StatefulWidget {
  final ViewCommentPageParams? params;
  final ViewCommentServices services;

  ViewCommentPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<ViewCommentPage> createState() => _ViewCommentPageState();
}

class _ViewCommentPageState extends State<ViewCommentPage> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController _commentTextController = TextEditingController();
  late List<ReactionDisposer> _disposers;

  @override
  void initState() {
    widget.services.store.getPostComments(postId: widget.params!.postId);
    _disposers = [
      reaction(
        (_) => widget.services.store.commentPosted,
        (bool result) async {
          if (result) {
            widget.services.store
                .getPostComments(postId: widget.params!.postId);
            _commentTextController.text = '';
          }
        },
      ),
    ];

    super.initState();
  }

  @override
  void dispose() {
    for (final d in _disposers) {
      d();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        title: const Text('Comments'),
        centerTitle: true,
      ),
      body: Observer(builder: (context) {
        if (widget.services.store.isLoading) {
          return Center(
            child: SizedBox(
              height: MediaQuery.of(context).size.height / 1.5,
              child: const Loader(),
            ),
          );
        }
        return Form(
          key: _formKey,
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Container(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(12.0, 12, 12, 80),
                    child: _commentList(),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: Colors.white,
                  padding: const EdgeInsets.fromLTRB(15.0, 20, 15, 40),
                  child: CommentSendText(
                    textController: _commentTextController,
                    suffixIcon: IconButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            widget.services.store.commentPosting(
                              postId: widget.params!.postId,
                              comment: _commentTextController.text,
                            );
                          }
                        },
                        icon: const Icon(Icons.send)),
                    validator: (value) {
                      if (value == null || value.trim().isEmpty) {
                        return "Enter Message to send";
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }

  Widget _commentList() {
    return Observer(builder: (context) {
      if (widget.services.store.commentList.isEmpty) {
        return const Align(
          alignment: Alignment.topCenter,
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Text('No Comments'),
          ),
        );
      }
      return ListView.builder(
          shrinkWrap: true,
          physics: const ScrollPhysics(),
          itemCount: widget.services.store.commentList.length,
          itemBuilder: (context, i) {
            final _menuKey = GlobalKey<PopupMenuButtonState>();
            return Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: CommentCard(
                  viewComment: widget.services.store.commentList[i],
                  menuKey: _menuKey,
                  popButtonCallBack: (value) {
                    if (value == 1) {
                      // report page
                      Navigator.push(
                          context,
                          MaterialPageRoute<void>(
                            builder: (context) => getIt<CommentReportPage>(
                              param1: ReportParams(
                                  comment:
                                      widget.services.store.commentList[i]),
                            ),
                          )).then(onReportCallBack);
                    }
                  },
                ));
          });
    });
  }

  Future<FutureOr> onReportCallBack(dynamic value) async {
    debugPrint("onCallback called..$value");
    if (value) {
      await widget.services.store
          .getPostComments(postId: widget.params!.postId);
    }
  }
}
