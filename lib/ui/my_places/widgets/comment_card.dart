import 'package:flutter/material.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/ui/widgets/rounded_border_image.dart';
import 'package:intl/intl.dart';

// ignore: must_be_immutable
class CommentCard extends StatelessWidget {
  Comment viewComment;
  GlobalKey<PopupMenuButtonState> menuKey;
  Function popButtonCallBack;
  CommentCard({
    Key? key,
    required this.viewComment,
    required this.menuKey,
    required this.popButtonCallBack,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 8,
        child: Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  RoundedBorderBoxImage(
                    size: 50,
                    borderWidth: 2,
                    borderRadius: 6,
                    profileUrl: viewComment.user!.avatarUrl,
                    borderColor: Colors.white,
                  ),
                  const SizedBox(width: 12),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Flexible(
                              fit: FlexFit.loose,
                              child: Text(
                                viewComment.user!.fullName!,
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    ?.copyWith(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17,
                                    ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            const SizedBox(width: 12),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              DateFormat('EEEEE, MMMM d, ' 'yyyy').format(
                                  DateTime.parse(viewComment.createdAt!)
                                      .toLocal()),
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText2
                                  ?.copyWith(color: Colors.grey, fontSize: 12),
                            ),
                            Text(
                              "  |  ${DateFormat('hh:mm a').format(DateTime.parse(viewComment.createdAt!).toLocal())} ",
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText2
                                  ?.copyWith(color: Colors.grey, fontSize: 12),
                              // DateFormat('EEEEE, MMMM d, ' 'yyyy')
                              //     .format(dateShared!),
                              // DateFormat.MMMMEEEEd().add_y().format(datePost!),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  PopupMenuButton(
                    key: menuKey,
                    shape: const RoundedRectangleBorder(
                      side: BorderSide(color: Color(0xFF9B8AFE), width: 1),
                      borderRadius: BorderRadius.all(
                        Radius.circular(8.0),
                      ),
                    ),
                    child: GestureDetector(
                      onTap: () async {
                        if (FocusScope.of(context).hasFocus) {
                          FocusScope.of(context).unfocus();
                          await Future.delayed(
                              const Duration(milliseconds: 400));
                        }
                        menuKey.currentState!.showButtonMenu();
                      },
                      child: const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Icon(Icons.more_vert)),
                    ),
                    offset: const Offset(-20, 30),
                    itemBuilder: (context) => [
                      const PopupMenuItem(
                        child: Text("Report",
                            style: TextStyle(
                                color: Color(0xFF9B8AFE),
                                fontWeight: FontWeight.w600)),
                        value: 1,
                      ),
                    ],
                    onSelected: (value) {
                      popButtonCallBack(value);
                    },
                  ),
                ],
              ),
              const SizedBox(height: 8),
              Text(
                viewComment.comment!,
                textAlign: TextAlign.left,
                style: Theme.of(context).textTheme.bodyText1?.copyWith(),
              )
            ],
          ),
        ));
  }
}
