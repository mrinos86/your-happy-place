import 'package:flutter/material.dart';

class CommentSendText extends StatelessWidget {
  final Function(String)? onChanged;
  final Function()? onTap;
  final TextEditingController? textController;
  final String? Function(String?)? validator;
  final String? errorText;
  final dynamic suffixIcon;

  const CommentSendText({
    Key? key,
    this.onChanged,
    this.textController,
    this.validator,
    this.errorText,
    this.onTap,
    this.suffixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: TextFormField(
            onTap: onTap,
            minLines: 1,
            maxLines: 4,
            textCapitalization: TextCapitalization.none,
            obscureText: false,
            keyboardType: TextInputType.multiline,
            onChanged: onChanged,
            controller: textController,
            validator: validator,
            style: Theme.of(context).textTheme.bodyText1?.copyWith(
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                  fontSize: Theme.of(context).textTheme.bodyText2?.fontSize,
                ),
            decoration: InputDecoration(
              fillColor: Colors.white,
              focusColor: Colors.white,
              filled: false,
              hintText: "Type your comment here",
              suffixIcon: suffixIcon,
              hintStyle: Theme.of(context).textTheme.bodyText1?.copyWith(
                    color: Colors.grey,
                    fontSize: Theme.of(context).textTheme.bodyText2?.fontSize,
                  ),
              errorText: errorText,
              border: OutlineInputBorder(
                gapPadding: 0,
                borderRadius: BorderRadius.circular(2),
                borderSide: BorderSide(color: Colors.grey.shade400),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(2),
                borderSide: BorderSide(color: Colors.grey.shade400),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
