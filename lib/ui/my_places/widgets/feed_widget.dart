import 'dart:async';

import 'package:flutter/material.dart';
import 'package:happy_place/data/stores/my_places_post/view_posts_store.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/injection.dart';
import 'package:happy_place/models/checkable_my_post.dart';
import 'package:happy_place/ui/my_places/share_post_page.dart';
import 'package:happy_place/ui/my_places/view_comment_page.dart';
import 'package:happy_place/ui/my_places/widgets/post_bottom_buttons_widget.dart';
import 'package:happy_place/ui/my_places/widgets/post_widget.dart';
import 'package:happy_place/utils/enums.dart';

class FeedWidget extends StatefulWidget {
  final Entry post;
  final List<Mood> moods;
  final ViewPostsStore store;
  final int index;

  const FeedWidget({
    Key? key,
    required this.post,
    required this.moods,
    required this.store,
    required this.index,
  }) : super(key: key);

  @override
  State<FeedWidget> createState() => _FeedWidgetState();
}

class _FeedWidgetState extends State<FeedWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Card(
        elevation: 8,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6),
        ),
        child: Column(
          children: [
            PostsWidget(
              post: CheckableMyPost(entry: widget.post, isChecked: false),
              moods: widget.moods,
              store: widget.store,
              index: widget.index,
              isSharedOne: true,
            ),
            const SizedBox(height: 12),
            const Divider(),
            PostBottomButtonsWidget(
              callBackButton: bottomButtonPressed,
              likeEnabled: true,
              shareEnabled: true,
              commentEnabled: true,
              isliked: widget.post.amILiked!,
              likeCount: widget.post.likesCount!,
              shareCount: widget.post.sharesCount!,
              commentCount: widget.post.commentsCount!,
            )
          ],
        ),
      ),
    );
  }

  Future<void> bottomButtonPressed(PostBottomButtonsType value) async {
    if (value == PostBottomButtonsType.like) {
      int postId = int.parse(widget.post.id!);
      widget.store.likePost(postId: postId, type: PostType.feed);
    } else if (value == PostBottomButtonsType.comment) {
      debugPrint("comment button presss");
      await Navigator.push(
          context,
          MaterialPageRoute<void>(
            builder: (context) => getIt<ViewCommentPage>(
              param1: ViewCommentPageParams(postId: int.parse(widget.post.id!)),
            ),
          )).then(onCommentCallBack);
    } else if (value == PostBottomButtonsType.share) {
      debugPrint("share button presss");
      Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (context) => getIt<SharePostPage>(
            param1: SharePostParams(post: widget.post),
          ),
        ),
      ).then(onCallBack);
    }
  }

  Future<FutureOr> onCallBack(dynamic value) async {
    debugPrint("onCallback called..$value");
    if (value) {
      await widget.store.getFeedPosts();
    }
  }

  Future<FutureOr> onCommentCallBack(dynamic value) async {
    debugPrint("onCommentCallback called..$value");
    await widget.store.getFeedPosts();
  }
}
