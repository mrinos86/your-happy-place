import 'dart:async';

import 'package:flutter/material.dart';
import 'package:happy_place/data/stores/my_places_post/view_posts_store.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/injection.dart';
import 'package:happy_place/models/checkable_my_post.dart';
import 'package:happy_place/ui/my_places/view_comment_page.dart';
import 'package:happy_place/ui/my_places/widgets/post_bottom_buttons_widget.dart';
import 'package:happy_place/ui/my_places/widgets/post_widget.dart';
import 'package:happy_place/ui/widgets/rounded_border_image.dart';
import 'package:happy_place/utils/enums.dart';
import 'package:intl/intl.dart';

class SharedPostWidget extends StatefulWidget {
  final SharedPost post;
  final List<Mood> moods;
  final ViewPostsStore store;
  final int index;

  const SharedPostWidget({
    Key? key,
    required this.post,
    required this.moods,
    required this.store,
    required this.index,
  }) : super(key: key);

  @override
  State<SharedPostWidget> createState() => _SharedPostWidgetState();
}

class _SharedPostWidgetState extends State<SharedPostWidget> {
  DateTime? datePost;
  DateTime? dateShared;

  @override
  void initState() {
    datePost = DateFormat('y-M-d').parse(widget.post.entry!.postDate!);
    dateShared = DateFormat('y-M-d').parse(widget.post.createdAt!);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20.0),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Column(
          children: [
            Row(
              children: [
                RoundedBorderBoxImage(
                  size: 50,
                  borderWidth: 2,
                  borderRadius: 6,
                  profileUrl: widget.post.user?.avatarUrl,
                  borderColor: Colors.white,
                ),
                const SizedBox(width: 12),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Flexible(
                            fit: FlexFit.loose,
                            child: Text(
                              widget.post.user!.fullName!.toString(),
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1
                                  ?.copyWith(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          const SizedBox(width: 12),
                          Text(
                            'Shared a Post',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                ?.copyWith(color: Colors.grey.shade400),
                          ),
                        ],
                      ),
                      Text(
                        DateFormat('EEEEE, MMMM d, ' 'yyyy')
                            .format(dateShared!.toLocal()),
                        // DateFormat.MMMMEEEEd().add_y().format(datePost!),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
        widget.post.description != null
            ? const SizedBox(height: 4)
            : const SizedBox(),
        widget.post.description != null
            ? Text(
                widget.post.description!,
              )
            : const SizedBox(),
        const SizedBox(height: 4),
        Card(
          elevation: 8,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6),
          ),
          child: Column(
            children: [
              PostsWidget(
                post: CheckableMyPost(
                    entry: widget.post.entry!, isChecked: false),
                moods: widget.moods,
                isSharedOne: true,
                store: widget.store,
                index: widget.index,
              ),
              const SizedBox(height: 12),
              const Divider(),
              PostBottomButtonsWidget(
                callBackButton: bottomButtonPressed,
                likeEnabled: true,
                shareEnabled: false,
                commentEnabled: true,
                isliked: widget.post.entry!.amILiked!,
                likeCount: widget.post.entry!.likesCount!,
                commentCount: widget.post.entry!.commentsCount!,
              )
            ],
          ),
        ),
      ]),
    );
  }

  Future<void> bottomButtonPressed(PostBottomButtonsType value) async {
    if (value == PostBottomButtonsType.like) {
      int postId = int.parse(widget.post.entry!.id!);
      widget.store.likePost(postId: postId, type: PostType.shared);
    } else if (value == PostBottomButtonsType.comment) {
      debugPrint("comment button presss");
      await Navigator.push(
          context,
          MaterialPageRoute<void>(
            builder: (context) => getIt<ViewCommentPage>(
              param1: ViewCommentPageParams(
                  postId: int.parse(widget.post.entry!.id!)),
            ),
          )).then(onCommentCallBack);
    }
  }

  Future<FutureOr> onCommentCallBack(dynamic value) async {
    debugPrint("onCallback called..$value");
    await widget.store.getSharedPosts();
  }
}
