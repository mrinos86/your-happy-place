import 'package:flutter/material.dart';
// ignore: implementation_imports
import 'package:flutter/src/widgets/image.dart' as image;
import 'package:happy_place/utils/enums.dart';

class PostBottomButtonsWidget extends StatefulWidget {
  final Function callBackButton;
  final bool likeEnabled;
  final bool commentEnabled;
  final bool shareEnabled;
  final bool isliked;
  final int likeCount;
  final int commentCount;
  final int shareCount;

  const PostBottomButtonsWidget({
    Key? key,
    required this.callBackButton,
    this.likeEnabled = false,
    this.commentEnabled = false,
    this.shareEnabled = false,
    this.isliked = false,
    this.likeCount = 0,
    this.commentCount = 0,
    this.shareCount = 0,
  }) : super(key: key);

  @override
  State<PostBottomButtonsWidget> createState() =>
      _PostBottomButtonsWidgetState();
}

class _PostBottomButtonsWidgetState extends State<PostBottomButtonsWidget> {
  Color likeCountColor = Colors.grey;
  Color likeIconColor = Colors.grey.shade500;
  int likeCountDisplay = 0;

  @override
  void initState() {
    super.initState();
    setState(() {
      likeCountColor = widget.isliked ? Colors.blue : Colors.grey;
      likeIconColor = widget.isliked ? Colors.blue : Colors.grey.shade500;
      likeCountDisplay = widget.likeCount;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(top: 8.0, bottom: 16),
        child:
            widget.commentEnabled && widget.shareEnabled && !widget.likeEnabled
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      commentButtonWidget(context),
                      Container(
                        height: 18,
                        width: 0.5,
                        color: Colors.grey.shade300,
                      ),
                      shareButtonWidget(context)
                    ],
                  )
                : widget.likeEnabled &&
                        widget.commentEnabled &&
                        !widget.shareEnabled
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          likeButtonWidget(context),
                          Container(
                            height: 18,
                            width: 0.5,
                            color: Colors.grey.shade300,
                          ),
                          commentButtonWidget(context)
                        ],
                      )
                    : widget.likeEnabled &&
                            widget.commentEnabled &&
                            widget.shareEnabled
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              likeButtonWidget(context),
                              Container(
                                height: 18,
                                width: 0.5,
                                color: Colors.grey.shade300,
                              ),
                              commentButtonWidget(context),
                              Container(
                                height: 18,
                                width: 0.5,
                                color: Colors.grey.shade300,
                              ),
                              shareButtonWidget(context)
                            ],
                          )
                        : Container());
  }

  Widget likeButtonWidget(context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          likeCountDisplay = likeCountColor == Colors.grey
              ? likeCountDisplay + 1
              : likeCountDisplay - 1;
          likeCountColor =
              likeCountColor == Colors.grey ? Colors.blue : Colors.grey;
          likeIconColor = likeIconColor == Colors.grey.shade500
              ? Colors.blue
              : Colors.grey.shade500;
        });
        widget.callBackButton(PostBottomButtonsType.like);
      },
      child: Row(children: [
        Icon(
          Icons.thumb_up_alt_outlined,
          size: 20,
          color: likeIconColor,
        ),
        const SizedBox(width: 8),
        Text(
          likeCountDisplay <= 0 ? '' : likeCountDisplay.toString(),
          style: Theme.of(context).textTheme.bodyText1?.copyWith(
                fontWeight: FontWeight.w700,
                color: likeCountColor,
              ),
        )
      ]),
    );
  }

  Widget commentButtonWidget(context) {
    return GestureDetector(
      onTap: () {
        widget.callBackButton(PostBottomButtonsType.comment);
      },
      child: Row(
        children: [
          Icon(
            Icons.comment_outlined,
            size: 20,
            color: Colors.grey.shade400,
          ),
          const SizedBox(width: 8),
          Text(
            widget.commentCount == 0 ? '' : widget.commentCount.toString(),
            style: Theme.of(context).textTheme.bodyText1?.copyWith(
                  fontWeight: FontWeight.w700,
                  color: Colors.grey,
                ),
          )
        ],
      ),
    );
  }

  Widget shareButtonWidget(context) {
    return GestureDetector(
      onTap: () {
        widget.callBackButton(PostBottomButtonsType.share);
      },
      child: Row(
        children: [
          image.Image.asset(
            'assets/images/ic_share.png',
            height: 16,
            color: Colors.grey.shade500,
          ),
          const SizedBox(width: 8),
          Text(
            widget.shareCount == 0 ? '' : widget.shareCount.toString(),
            style: Theme.of(context).textTheme.bodyText1?.copyWith(
                  fontWeight: FontWeight.w700,
                  color: Colors.grey,
                ),
          )
        ],
      ),
    );
  }
}
