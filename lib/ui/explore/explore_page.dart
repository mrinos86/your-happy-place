// ignore_for_file: implementation_imports

import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/image.dart' as image;
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:happy_place/data/stores/explore/explore_store.dart';
import 'package:happy_place/generated/l10n.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/ui/explore/filters_page.dart';
import 'package:happy_place/ui/settings/information_page.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:happy_place/ui/widgets/marker_generator.dart';
import 'package:injectable/injectable.dart';
import 'package:mobx/mobx.dart';

import '../../injection.dart';

const kGoogleApiKey = "AIzaSyDcPcvMga2LhgQqGW6S2zfFj3ZCEfvP05I";

class ExplorePageParams {
  const ExplorePageParams(this.key);
  final Key key;
}

@injectable
class ExploreServices {
  ExploreStore store;

  ExploreServices({
    required this.store,
  });
}

@injectable
class ExplorePage extends StatefulWidget {
  final ExploreServices services;
  final ExplorePageParams? params;
  ExplorePage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<ExplorePage> createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  final Completer<GoogleMapController> _controller = Completer();
  final TextEditingController _textController = TextEditingController();

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 7,
  );

  late List<ReactionDisposer> _disposers;
  late GoogleMapController googleMapController;

  void mapBitmapsToMarkers(List<Uint8List> bitmaps) {
    widget.services.store.customMarkers.clear();
    bitmaps.asMap().forEach((i, bmp) {
      widget.services.store.customMarkers.add(
        Marker(
            markerId: MarkerId("$i"),
            position: LatLng(
              double.parse(widget.services.store.places[i].latitude!),
              double.parse(widget.services.store.places[i].longitude!),
            ),
            icon: BitmapDescriptor.fromBytes(bmp),
            onTap: () async {}),
      );
    });
    debugPrint(
        'custom marker length: ${widget.services.store.customMarkers.length}');
    widget.services.store.customMarkers = widget.services.store.customMarkers;
  }

  @override
  void initState() {
    _disposers = [
      reaction(
        (_) => widget.services.store.focusMyCurrentPosition,
        (int result) async {
          googleMapController = await _controller.future;
          googleMapController.animateCamera(
            CameraUpdate.newCameraPosition(
              CameraPosition(
                  target: LatLng(widget.services.store.locationData!.latitude,
                      widget.services.store.locationData!.longitude),
                  zoom: 16),
            ),
          );
        },
      ),
      reaction(
        (_) => widget.services.store.searchLocationUpdated,
        (int result) async {
          googleMapController = await _controller.future;
          googleMapController.animateCamera(
            CameraUpdate.newCameraPosition(
              CameraPosition(
                  target: LatLng(
                      widget.services.store.searchLocationSelected!.latitude,
                      widget.services.store.searchLocationSelected!.longitude),
                  zoom: 16),
            ),
          );
        },
      ),
      reaction(
        (_) => widget.services.store.markersUpdated,
        (int markers) async {
          await Future.forEach(widget.services.store.places,
              (VisitedPlace post) async {
            if (post.feeling != null) {
              await precacheImage(
                  NetworkImage(post.feeling!.imageUrl!), context);
            }
            return null;
          });

          MarkerGenerator(widget.services.store.markers.toList(), (bitmaps) {
            debugPrint('bitmaps found: ${bitmaps.length}');
            mapBitmapsToMarkers(bitmaps);
          }).generate(context);
        },
      ),
    ];
    super.initState();
  }

  @override
  void dispose() {
    for (final d in _disposers) {
      d();
    }
    super.dispose();
  }

  void _onSearchedPlaceSelected(index) {
    _textController.text =
        widget.services.store.placesAutoCompleteList[index].description;
    widget.services.store.searchLocationSelected =
        widget.services.store.placesAutoCompleteList[index];
    widget.services.store.getPosts();
    widget.services.store.placesAutoCompleteList = [];
    widget.services.store.searchLocationUpdated++;
    FocusManager.instance.primaryFocus?.unfocus();
  }

  void _onChangedSearchedText(text) {
    widget.services.store.searchLocationSelected = null;
    if (text != '') {
      widget.services.store.searchAddressWithAutoComplete(text);
    } else {
      widget.services.store.placesAutoCompleteList = [];
      widget.services.store.loadingPlaces = false;
    }
  }

  void _onPressedFilterButton() {
    _textController.text = "";
    FocusManager.instance.primaryFocus?.unfocus();
    widget.services.store.placesAutoCompleteList = [];
    widget.services.store.loadingPlaces = false;
    Navigator.push(
      context,
      MaterialPageRoute<void>(
        builder: (context) => getIt<FiltersPage>(
          param1: FiltersPageParams(store: widget.services.store),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          S.of(context).explore,
        ),
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: GestureDetector(
              onTap: () async {
                await Navigator.push(
                  context,
                  MaterialPageRoute<void>(
                    builder: (context) => getIt<InformationPage>(
                        param1: InformationPageParams(type: 0)),
                  ),
                );
                widget.services.store.init();
              },
              child: Center(
                child: image.Image.asset(
                  'assets/images/ic_about.png',
                  height: 20,
                ),
              ),
            ),
          )
        ],
      ),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Observer(builder: (context) {
            return GestureDetector(
              child: GoogleMap(
                mapType: MapType.normal,
                markers: widget.services.store.customMarkers,
                initialCameraPosition: _kGooglePlex,
                myLocationEnabled: true,
                myLocationButtonEnabled: false,
                zoomControlsEnabled: false,
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                  widget.services.store.getLocationPermission();
                },
              ),
            );
          }),
          Padding(
            padding: const EdgeInsets.only(top: 12.0, right: 16, left: 16),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          widget.services.store.placesAutoCompleteList = [];
                          widget.services.store.loadingPlaces = false;
                        },
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16.0, vertical: 8.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: TextFormField(
                                    controller: _textController,
                                    decoration: InputDecoration(
                                      isDense: true,
                                      border: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      contentPadding: const EdgeInsets.only(
                                          left: 12, right: 12),
                                      hintText: "Search",
                                      hintStyle: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          ?.copyWith(
                                            color: Colors.grey.shade400,
                                          ),
                                    ),
                                    onChanged: (text) {
                                      _onChangedSearchedText(text);
                                    },
                                  ),
                                ),
                                Icon(
                                  Icons.search,
                                  size: 22,
                                  color: Colors.grey.shade400,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: Observer(builder: (context) {
                            if (widget.services.store.loadingPlaces) {
                              return const Loader();
                            }
                            if (widget.services.store.placesAutoCompleteList
                                .isEmpty) {
                              return const SizedBox();
                            }
                            return ListView.builder(
                              itemCount: widget
                                  .services.store.placesAutoCompleteList.length,
                              itemBuilder: (context, index) {
                                return ListTile(
                                  onTap: () {
                                    _onSearchedPlaceSelected(index);
                                  },
                                  title: Text(
                                    widget
                                        .services
                                        .store
                                        .placesAutoCompleteList[index]
                                        .description,
                                  ),
                                );
                              },
                            );
                          }),
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: _onPressedFilterButton,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12.0, vertical: 8.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          image.Image.asset(
                            'assets/images/ic_filter.png',
                            height: 20,
                            width: 20,
                          ),
                          const SizedBox(width: 8),
                          Text(
                            'Filter',
                            style:
                                Theme.of(context).textTheme.bodyText1?.copyWith(
                                      color: Colors.grey.shade400,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
