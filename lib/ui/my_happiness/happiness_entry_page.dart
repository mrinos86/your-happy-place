import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
// ignore: implementation_imports
import 'package:flutter/src/widgets/image.dart' as image;
import 'package:happy_place/ui/my_happiness/happiness_entry_category_page.dart';
import 'package:happy_place/ui/my_happiness/my_happiness_page.dart';
import 'package:happy_place/ui/widgets/app_button.dart';
import 'package:happy_place/ui/widgets/info_alert.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:happy_place/ui/my_happiness/widgets/happiness_entry/line_chart_widget.dart';
import 'package:happy_place/ui/my_happiness/widgets/happiness_entry/entry_question_widget.dart';
import 'package:injectable/injectable.dart';

import '../../injection.dart';
import '../settings/information_page.dart';

@injectable
class HappinessEntryPage extends StatefulWidget {
  final TestPageServices services;
  final TestPageParams? params;
  HappinessEntryPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<HappinessEntryPage> createState() => _HappinessEntryPageState();
}

class _HappinessEntryPageState extends State<HappinessEntryPage> {
  late int showingTooltip;
  @override
  void initState() {
    _initPage();
    super.initState();
  }

  void _initPage() {
    widget.services.store.getHappinessTestResult();
    widget.services.store.getHappinessTestQuestions(widget.params!.test);
    widget.services.store.getTestAvailabilityStatus("Make a Happiness Entry");
    widget.services.store.getHappynessTestLatestResult("all");
  }

  void _showAlert() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return InfoAlert(
          title: "Alert",
          detail: "You are already made the happiness entries for today.",
          buttonText: "Ok",
        );
      },
    );
  }

  Future<void> _onPressedSaveButton() async {
    if (widget.services.store.status == "test_not_available") {
      _showAlert();
    } else {
      debugPrint("zzzzzzz::::");
      debugPrint(widget.services.store.message);
      await widget.services.store.submitTest();
      _initPage();
    }
  }

  _onPressedEyeIconButton() async {
    Navigator.push(
      context,
      MaterialPageRoute<void>(
        builder: (context) => getIt<HappinessEntryCategoryPage>(
          param1: HappinessEntryPageParam(
              test: widget.params!.test,
              question: widget.services.store.selectedQuestion!),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.params!.test.name!),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: GestureDetector(
              onTap: () async {
                widget.services.store.init();
                await Navigator.push(
                  context,
                  MaterialPageRoute<void>(
                    builder: (context) => getIt<InformationPage>(
                        param1: InformationPageParams(
                      type: 1,
                    )),
                  ),
                );
              },
              child: Center(
                child: image.Image.asset(
                  'assets/images/ic_about.png',
                  height: 20,
                ),
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: Observer(builder: (context) {
            if (widget.services.store.loading) {
              return Center(
                  child: SizedBox(
                      height: MediaQuery.of(context).size.height / 1.5,
                      child: const Loader()));
            } else {
              return Padding(
                padding: const EdgeInsets.only(top: 25.0, bottom: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    LineChartWidget(
                        list: widget.services.store.listHappinessTestResult),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        const SizedBox(height: 24),
                        widget.services.store.listQuestions.isNotEmpty
                            ? ListView.builder(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount:
                                    widget.services.store.listQuestions.length,
                                itemBuilder: (context, index) {
                                  return EntryQuestionWidget(
                                    buildContext: context,
                                    store: widget.services.store,
                                    question: widget
                                        .services.store.listQuestions[index],
                                    isFromView: false,
                                    callBackFunction: _onPressedEyeIconButton,
                                  );
                                },
                              )
                            : const Loader(),
                        const SizedBox(height: 24),
                        widget.services.store.listQuestions.isNotEmpty
                            ? Align(
                                alignment: Alignment.center,
                                child: SizedBox(
                                  width: 150,
                                  child: AppButton(
                                      title: 'Save',
                                      onPressed: _onPressedSaveButton),
                                ),
                              )
                            : const SizedBox(),
                        const SizedBox(height: 24),
                      ],
                    )
                  ],
                ),
              );
              // : Center(
              //     child: SizedBox(
              //         height: MediaQuery.of(context).size.height / 1.5,
              //         child: const Loader()));
            }
          }),
        ),
      ),
    );
  }
}
