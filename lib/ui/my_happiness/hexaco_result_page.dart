import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
// ignore: implementation_imports
import 'package:flutter/src/widgets/image.dart' as image;
import 'package:happy_place/injection.dart';
import 'package:happy_place/ui/my_happiness/hexaco_test_page.dart';
import 'package:happy_place/ui/my_happiness/my_happiness_page.dart';
import 'package:happy_place/ui/settings/information_page.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:happy_place/ui/my_happiness/widgets/hexaco_test/hexaco_result_card.dart';
import 'package:injectable/injectable.dart';

@injectable
class HexacoResultPage extends StatefulWidget {
  final TestPageServices services;
  final TestPageParams? params;
  HexacoResultPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<HexacoResultPage> createState() => _HexacoResultPageState();
}

class _HexacoResultPageState extends State<HexacoResultPage> {
  late int showingTooltip;
  @override
  void initState() {
    widget.services.store.getHexacoTestResult();
    super.initState();
  }

  FutureOr _onCallBack(dynamic value) {
    widget.services.store.getHexacoTestResult();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.params!.test.name!),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: GestureDetector(
              onTap: () async {
                await Navigator.push(
                  context,
                  MaterialPageRoute<void>(
                    builder: (context) => getIt<InformationPage>(
                        param1: InformationPageParams(type: 2)),
                  ),
                );
              },
              child: Center(
                child: image.Image.asset(
                  'assets/images/ic_about.png',
                  height: 20,
                ),
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: Observer(builder: (context) {
            if (widget.services.store.loading) {
              return Center(
                  child: SizedBox(
                      height: MediaQuery.of(context).size.height / 1.5,
                      child: const Loader()));
            }
            return Padding(
              padding: const EdgeInsets.only(top: 25.0, bottom: 5),
              child: Column(children: [
                Column(
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute<void>(
                                builder: (context) => getIt<HexacoTestPage>(
                                    param1: TestPageParams(
                                  test: widget.params!.test,
                                )),
                              )).then(_onCallBack);
                          // }
                        },
                        child: Container(
                          padding: const EdgeInsets.all(30.0),
                          width: MediaQuery.of(context).size.width,
                          child: Text("Take the Test",
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  ?.copyWith(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                      fontSize: 20)),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            color: Theme.of(context).primaryColor,
                          ),
                        )),
                    const SizedBox(height: 25),
                    const Divider(thickness: 1),
                    const SizedBox(height: 12),
                  ],
                ),
                const Text(
                  'Result',
                  style: TextStyle(fontSize: 18),
                ),
                const SizedBox(height: 18),
                Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: widget.services.store.listHexacoTestResult.isNotEmpty
                        ? Card(
                            shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(16.0),
                              ),
                            ),
                            elevation: 10,
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(16),
                              ),
                              child: SizedBox(
                                height: 800,
                                child: ListView.builder(
                                    padding: const EdgeInsets.all(0),
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemCount: widget.services.store
                                        .listHexacoTestResult.length,
                                    itemBuilder: (buildContext, index) {
                                      return HexacoResultCard(
                                        index: index,
                                        title: widget.services.store
                                            .listHexacoTestResult[index].title
                                            .toString(),
                                        detail: widget.services.store
                                            .listHexacoTestResult[index].result
                                            .toString(),
                                      );
                                    }),
                              ),
                            ),
                          )
                        : const Text(
                            "Results are not available,\nTake the Test to see the Results",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Colors.grey),
                            textAlign: TextAlign.center,
                          )),
              ]),
            );
          }),
        ),
      ),
    );
  }
}
