import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';

// ignore: must_be_immutable
class LineChartWidget extends StatelessWidget {
  List<HappinessTestResult>? list;
  LineChartWidget({Key? key, this.list}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 200,
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(15),
        margin: const EdgeInsets.all(2),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16.0),
            gradient: const LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: [Color(0xff302534), Color(0xffcda0df)],
            )),
        child: Center(
            child: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: LineChart(
            LineChartData(
                minY: 0,
                // minX: 0,
                lineTouchData: lineTouchData1,
                titlesData: titlesData1,
                borderData: borderData,
                gridData: gridData,
                lineBarsData: lineBarsData1),
            swapAnimationDuration:
                const Duration(milliseconds: 150), // Optional
            swapAnimationCurve: Curves.linear, // Optional
          ),
        )));
  }

  List<LineChartBarData> get lineBarsData1 => [
        lineChartBarData,
      ];

  List<FlSpot> flSpotList() {
    List<FlSpot> flList = [];

    if (list!.isNotEmpty) {
      for (var element in list!) {
        flList.add(FlSpot(
            list!.indexOf(element) + 1.toDouble(), element.score!.toDouble()));
      }
    }
    return flList;
  }

  LineChartBarData get lineChartBarData => LineChartBarData(
      color: const Color(0xffd5aee7),
      barWidth: 2,
      isStrokeCapRound: true,
      dotData: FlDotData(
        show: true,
        getDotPainter: (spot, percent, barData, index) =>
            FlDotCirclePainter(color: Colors.white),
      ),
      belowBarData: BarAreaData(
        show: true,
        color: const Color(0xffa189ac).withOpacity(0.5),
      ),
      spots: flSpotList());

  FlTitlesData get titlesData1 => FlTitlesData(
        bottomTitles: AxisTitles(sideTitles: bottomTitles),
        rightTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
        topTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
        leftTitles: AxisTitles(sideTitles: leftTitles),
      );

  SideTitles get bottomTitles => SideTitles(
      interval: 1,
      showTitles: true,
      getTitlesWidget: (value, meta) {
        String text = '';
        if (list!.isNotEmpty) {
          var data = list![value.toInt() - 1].date.toString().split("-");
          text = (data[1] + "/" + data[2]);
        }
        return Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Text(
            text,
            style: const TextStyle(color: Colors.white, fontSize: 12),
          ),
        );
      });

  SideTitles get leftTitles => SideTitles(
        showTitles: true,
        reservedSize: 50,
        getTitlesWidget: (value, meta) {
          return Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Text(
              value.toString(),
              textAlign: TextAlign.right,
              style: const TextStyle(color: Colors.white, fontSize: 12),
            ),
          );
        },
      );

  FlGridData get gridData => FlGridData(
      show: true,
      getDrawingHorizontalLine: (value) => FlLine(
          color: const Color(0xffe7e8ec), dashArray: [5], strokeWidth: 0.5),
      getDrawingVerticalLine: (value) => FlLine(
          color: const Color(0xffe7e8ec), dashArray: [5], strokeWidth: 0.5),
      drawVerticalLine: true,
      drawHorizontalLine: true);

  FlBorderData get borderData => FlBorderData(
        show: true,
        border: const Border(
          bottom: BorderSide(color: Colors.white, width: 0.2),
          left: BorderSide(color: Colors.transparent),
          right: BorderSide(color: Colors.transparent),
          top: BorderSide(color: Colors.white, width: 0.2),
        ),
      );

  LineTouchData get lineTouchData1 => LineTouchData(
        handleBuiltInTouches: true,
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.blueGrey.withOpacity(0.8),
        ),
      );

  // String setxAxis(value) {
  //   var data = value.split("-");
  //   String returnValue = data[1] + "/" + data[2] + "\n" + data[0];
  //   return "asasa";
  // }
}
