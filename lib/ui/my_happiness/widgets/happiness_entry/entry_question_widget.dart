import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/data/stores/my_happiness_store/test_store.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/ui/widgets/custom_slider_shape.dart';
import 'package:happy_place/ui/my_happiness/widgets/polygon_slider_thumb.dart';

// ignore: must_be_immutable
class EntryQuestionWidget extends StatelessWidget {
  BuildContext buildContext;
  TestStore store;
  Question question;
  bool isFromView;
  Function? callBackFunction;
  EntryQuestionWidget(
      {Key? key,
      required this.buildContext,
      required this.store,
      required this.question,
      required this.isFromView,
      this.callBackFunction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(16.0),
        ),
      ),
      elevation: 10,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Flexible(flex: 1, child: SizedBox()),
                Flexible(
                  flex: 6,
                  child: Text(
                    question.title!,
                    style: Theme.of(buildContext)
                        .textTheme
                        .headline6
                        ?.copyWith(fontWeight: FontWeight.bold),
                  ),
                ),
                !isFromView
                    ? Flexible(
                        flex: 1,
                        child: IconButton(
                          padding: const EdgeInsets.only(right: 12),
                          constraints: const BoxConstraints(),
                          icon: const Icon(Icons.remove_red_eye_sharp),
                          iconSize: 20,
                          color: Theme.of(context).primaryColor,
                          onPressed: () {
                            store.selectedQuestion = question;
                            callBackFunction!();
                          },
                        ))
                    : const Flexible(flex: 1, child: SizedBox()),
              ],
            ),
            const SizedBox(height: 12),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: question.subQuestions!.length,
                itemBuilder: (buildContext, subQuestionIndex) {
                  return Observer(builder: (buildContext) {
                    var itemIndex = subQuestionIndex + 1;
                    double currentValue = double.parse(
                      isFromView
                          ? store.listAnswer.isNotEmpty
                              ? store.listAnswer
                                  .firstWhere(
                                    (answer) =>
                                        answer.questionId ==
                                        question
                                            .subQuestions![subQuestionIndex].id,
                                  )
                                  .answer!
                                  .toString()
                              : "0"
                          : store.listBigFiveAnswers
                              .firstWhere(
                                (answer) =>
                                    answer.questionId ==
                                    question.subQuestions![subQuestionIndex].id,
                              )
                              .answer!,
                    ).round().toDouble();
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                itemIndex.toString() + '.',
                                style: Theme.of(buildContext)
                                    .textTheme
                                    .bodyText1
                                    ?.copyWith(color: Colors.grey),
                              ),
                              const SizedBox(width: 8),
                              Expanded(
                                child: Text(
                                  question
                                      .subQuestions![subQuestionIndex].title!,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.sentiment_dissatisfied_rounded,
                                color: Colors.grey.shade500,
                              ),
                              const SizedBox(width: 13),
                              Expanded(
                                child: SliderTheme(
                                  data: SliderTheme.of(context).copyWith(
                                      trackShape: CustomTrackShape(),
                                      thumbShape: PolygonSliderThumb(
                                        thumbRadius: 13.0,
                                        sliderValue: currentValue,
                                      )),
                                  child: Slider(
                                    max: 10,
                                    min: 0,
                                    divisions: 10,
                                    value: currentValue,
                                    onChanged: (s) {
                                      isFromView
                                          ? {}
                                          : store.updateBigFiveAnswerValue(
                                              question.subQuestions![
                                                  subQuestionIndex],
                                              s,
                                            );
                                    },
                                  ),
                                ),
                              ),
                              const SizedBox(width: 13),
                              Icon(
                                Icons.sentiment_satisfied_rounded,
                                color: Colors.grey.shade500,
                              ),
                            ],
                          ),
                        ],
                      ),
                    );
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
