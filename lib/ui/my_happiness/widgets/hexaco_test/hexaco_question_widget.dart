import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/data/stores/my_happiness_store/test_store.dart';
import 'package:happy_place/ui/widgets/custom_slider_shape.dart';
import 'package:happy_place/ui/my_happiness/widgets/polygon_slider_thumb.dart';

// ignore: must_be_immutable
class HexacoQuestionWidget extends StatelessWidget {
  TestStore store;
  BuildContext buildContext;
  HexacoQuestionWidget(
      {Key? key, required this.store, required this.buildContext})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(16.0),
        ),
      ),
      elevation: 10,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 12,
        ),
        child: ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: store.listQuestions[0].subQuestions!.length,
          itemBuilder: (buildContext, index) {
            return Observer(builder: (buildContext) {
              var subQuestion = store.listQuestions[0].subQuestions![index];
              double currentValue = double.parse(store.listBigFiveAnswers
                      .firstWhere(
                        (answer) =>
                            answer.questionId ==
                            store.listQuestions[0].subQuestions![index].id,
                      )
                      .answer!)
                  .round()
                  .toDouble();
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Center(
                      child: Text(subQuestion.title!,
                          style: Theme.of(buildContext).textTheme.subtitle1!),
                    ),
                    const SizedBox(height: 5),
                    Padding(
                      padding: const EdgeInsets.only(left: 5.0, right: 5.0),
                      child: SliderTheme(
                        data: SliderTheme.of(context).copyWith(
                            trackShape: CustomTrackShape(),
                            thumbShape: PolygonSliderThumb(
                              thumbRadius: 13.0,
                              sliderValue: currentValue,
                            )),
                        child: Slider(
                          max: 10,
                          min: 1,
                          divisions: 9,
                          value: currentValue,
                          onChanged: (s) {
                            store.updateBigFiveAnswerValue(
                              store
                                  .listQuestions[0] // always one top question
                                  .subQuestions![index],
                              s,
                            );
                          },
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Flexible(
                          flex: 3,
                          child: Text(
                            subQuestion.bottomEndNote!,
                            textAlign: TextAlign.left,
                            style: Theme.of(buildContext)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black),
                          ),
                        ),
                        Flexible(
                          child: Container(),
                          flex: 2,
                        ),
                        Flexible(
                          flex: 3,
                          child: Text(
                            subQuestion.topEndNote!,
                            textAlign: TextAlign.right,
                            style: Theme.of(buildContext)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              );
            });
          },
        ),
      ),
    );
  }
}
