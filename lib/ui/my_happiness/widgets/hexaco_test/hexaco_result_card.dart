import 'package:flutter/material.dart';
import 'package:happy_place/extensions/colors.dart';

// ignore: must_be_immutable
class HexacoResultCard extends StatelessWidget {
  String title;
  String detail;
  int index;

  HexacoResultCard({
    Key? key,
    required this.title,
    required this.detail,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 18),
        Text(title,
            // index == 0
            //     ? "$title (Style)"
            //     : index == 1
            //         ? "$title (Stability)"
            //         : index == 2
            //             ? "$title (Relational)"
            //             : index == 3
            //                 ? "$title (Friendliness)"
            //                 : index == 4
            //                     ? "$title (Focus)"
            //                     : index == 5
            //                         ? "$title (Interests)"
            //                         : title,
            style: Theme.of(context)
                .textTheme
                .bodyText1
                ?.copyWith(fontWeight: FontWeight.bold)),
        const SizedBox(height: 20),
        Container(
          decoration: BoxDecoration(
            color: index == 0
                ? testResultColorsHonest
                : index == 1
                    ? testResultColorsCalm
                    : index == 2
                        ? testResultColorsAmbivert
                        : index == 3
                            ? testResultColorsIndependent
                            : index == 4
                                ? testResultColorsPlayful
                                : index == 5
                                    ? testResultColorsCreative
                                    : Colors.blueAccent,
            borderRadius: BorderRadius.circular(5),
          ),
          width: MediaQuery.of(context).size.width / 2.5,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(detail,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1?.copyWith(
                    fontWeight: FontWeight.bold, color: Colors.white)),
          ),
        ),
        const SizedBox(height: 20),
        index != 5 ? const Divider(thickness: 1) : Container()
      ],
    );
  }
}
