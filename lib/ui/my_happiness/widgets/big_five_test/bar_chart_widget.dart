import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:happy_place/data/stores/my_happiness_store/test_store.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';

// ignore: must_be_immutable
class BarChartWidget extends StatelessWidget {
  TestStore store;

  BarChartWidget({Key? key, required this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      margin: const EdgeInsets.all(2),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16.0),
          gradient: const LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            colors: [Color(0xff302534), Color(0xffcda0df)],
          )),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: BarChart(
            BarChartData(
                titlesData: titlesData,
                borderData: borderData,
                gridData: gridData,
                groupsSpace: 4,
                barGroups: generateGroupData()),
          ),
        ),
      ),
      height: 200,
      width: MediaQuery.of(context).size.width,
    );
  }

  List<BarChartGroupData> generateGroupData() {
    List<BarChartGroupData> barGroupData = [];
    for (BigFiveTestResult element in store.listBigFiveTestResult) {
      {
        barGroupData.add(BarChartGroupData(
          x: store.listBigFiveTestResult.indexOf(element),
          barRods: [
            BarChartRodData(
              color: const Color(0xffd5aee7),
              toY: element.score!.toDouble(),
              width: 25,
              borderRadius: const BorderRadius.all(Radius.zero),
            ),
          ],
        ));
      }
    }

    return barGroupData;
  }

  FlGridData get gridData => FlGridData(
      show: true,
      getDrawingHorizontalLine: (value) => FlLine(
          color: const Color(0xffe7e8ec), dashArray: [5], strokeWidth: 0.5),
      drawVerticalLine: false,
      drawHorizontalLine: true);

  FlBorderData get borderData => FlBorderData(
        show: true,
        border: const Border(
          bottom: BorderSide(color: Colors.white, width: 0.2),
          left: BorderSide(color: Colors.transparent),
          right: BorderSide(color: Colors.transparent),
          top: BorderSide(color: Colors.white, width: 0.2),
        ),
      );

  FlTitlesData get titlesData => FlTitlesData(
      bottomTitles: AxisTitles(sideTitles: _bottomTitles),
      rightTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
      topTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
      leftTitles: AxisTitles(sideTitles: _leftTitles));

  SideTitles get _bottomTitles => SideTitles(
        showTitles: true,
        getTitlesWidget: (value, meta) {
          return Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text(
              store.listBigFiveTestResult[value.toInt()].letter.toString(),
              style: const TextStyle(color: Colors.white, fontSize: 12),
            ),
          );
        },
      );

  SideTitles get _leftTitles => SideTitles(
        showTitles: true,
        reservedSize: 25,
        getTitlesWidget: (value, meta) {
          return Text(
            value.toInt().toString(),
            style: const TextStyle(color: Colors.white, fontSize: 12),
          );
        },
      );
}
