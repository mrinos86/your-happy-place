import 'package:expandable_page_view/expandable_page_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/data/stores/my_happiness_store/test_store.dart';
import 'package:happy_place/ui/widgets/custom_slider_shape.dart';
import 'package:happy_place/ui/my_happiness/widgets/polygon_slider_thumb.dart';

// ignore: must_be_immutable
class SliderWidget extends StatelessWidget {
  TestStore store;
  BuildContext buildContext;
  SliderWidget({Key? key, required this.store, required this.buildContext})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpandablePageView.builder(
      itemCount: store.listQuestions.length,
      controller: store.pageController,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (buildContext, pageViewIndex) {
        return Card(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: ListView.separated(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount:
                  store.listQuestions[pageViewIndex].subQuestions!.length,
              itemBuilder: (buildContext, listIndex) {
                var number = listIndex + 1;
                return Observer(builder: (buildContext) {
                  double currentValue = double.parse(store.listBigFiveAnswers
                          .firstWhere((answer) =>
                              answer.questionId ==
                              store.listQuestions[pageViewIndex]
                                  .subQuestions![listIndex].id)
                          .answer!)
                      .round()
                      .toDouble();
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            number.toString() + '.',
                            style: Theme.of(buildContext)
                                .textTheme
                                .bodyText1
                                ?.copyWith(fontWeight: FontWeight.w600),
                          ),
                          const SizedBox(width: 5),
                          Expanded(
                            child: Text(
                              store.listQuestions[pageViewIndex]
                                  .subQuestions![listIndex].title!,
                              style: Theme.of(buildContext)
                                  .textTheme
                                  .bodyText1
                                  ?.copyWith(fontWeight: FontWeight.w600),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.thumb_down, color: Colors.grey[300]),
                            const SizedBox(width: 17),
                            SliderTheme(
                              data: SliderTheme.of(context).copyWith(
                                  trackShape: CustomTrackShape(),
                                  thumbShape: PolygonSliderThumb(
                                    thumbRadius: 13.0,
                                    sliderValue: currentValue,
                                  )),
                              child: Slider(
                                max: 10,
                                min: 0,
                                divisions: 10,
                                value: currentValue,
                                onChanged: (double value) {
                                  store.updateBigFiveAnswerValue(
                                      store.listQuestions[pageViewIndex]
                                          .subQuestions![listIndex],
                                      value);
                                },
                              ),
                            ),
                            const SizedBox(width: 17),
                            Icon(Icons.thumb_up, color: Colors.grey[300]),
                          ],
                        ),
                      ),
                    ],
                  );
                });
              },
              separatorBuilder: (BuildContext buildContext, int index) {
                return const SizedBox(height: 18);
              },
            ),
          ),
        );
      },
    );
  }
}
