import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/ui/my_happiness/my_happiness_page.dart';
import 'package:happy_place/ui/widgets/app_outlined_button.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:happy_place/ui/my_happiness/widgets/big_five_test/slider_widget.dart';
import 'package:injectable/injectable.dart';

@injectable
class BigFiveTestPage extends StatefulWidget {
  final TestPageServices services;
  final TestPageParams? params;
  BigFiveTestPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<BigFiveTestPage> createState() => _BigFiveTestPageState();
}

class _BigFiveTestPageState extends State<BigFiveTestPage> {
  late int showingTooltip;
  @override
  void initState() {
    widget.services.store.getTestQuestions(widget.params!.test);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.params!.test.name!),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: Observer(builder: (context) {
            if (widget.services.store.loading) {
              return Center(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height / 1.5,
                  child: const Loader(),
                ),
              );
            }
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 24),
                Center(
                  child: Text(
                    widget
                        .services
                        .store
                        .listQuestions[widget.services.store.pageViewPage - 1]
                        .title!,
                    style: Theme.of(context)
                        .textTheme
                        .headline6
                        ?.copyWith(fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(height: 8),
                Padding(
                  padding: const EdgeInsets.only(left: 25.0, right: 25),
                  child: Text(
                    widget
                        .services
                        .store
                        .listQuestions[widget.services.store.pageViewPage - 1]
                        .description!,
                    style: Theme.of(context)
                        .textTheme
                        .bodySmall
                        ?.copyWith(color: Colors.grey),
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(height: 12),
                SliderWidget(
                  store: widget.services.store,
                  buildContext: context,
                ),
                const SizedBox(height: 12),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: widget.services.store.pageViewPage == 1
                          ? const SizedBox()
                          : Align(
                              alignment: Alignment.centerLeft,
                              child: GestureDetector(
                                onTap: () {
                                  widget.services.store.previousPageViewPage();
                                },
                                child: Container(
                                  height: 50,
                                  width: 50,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(24),
                                    border: Border.all(
                                      color: Theme.of(context).primaryColor,
                                      width: 2,
                                    ),
                                  ),
                                  child: Icon(
                                    Icons.arrow_back_rounded,
                                    color: Theme.of(context).primaryColor,
                                  ),
                                ),
                              ),
                            ),
                    ),
                    Expanded(
                      child: Center(
                        child: Text(
                          '${widget.services.store.pageViewPage} of ${widget.services.store.listQuestions.length}',
                          style: const TextStyle(color: Colors.grey),
                        ),
                      ),
                    ),
                    Expanded(
                      child: widget.services.store.pageViewPage ==
                              widget.services.store.listQuestions.length
                          ? const SizedBox()
                          : Align(
                              alignment: Alignment.centerRight,
                              child: GestureDetector(
                                onTap: () {
                                  widget.services.store.nextPageViewPage();
                                },
                                child: Container(
                                  height: 50,
                                  width: 50,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(24),
                                    color: Theme.of(context).primaryColor,
                                  ),
                                  child: const Icon(
                                    Icons.arrow_forward_rounded,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                    ),
                  ],
                ),
                const SizedBox(height: 16),
                Align(
                  alignment: Alignment.center,
                  child: AppOutlinedButton(
                    title: widget.services.store.pageViewPage ==
                            widget.services.store.listQuestions.length
                        ? 'Save'
                        : 'Cancel',
                    onPressed: () async {
                      if (widget.services.store.pageViewPage ==
                          widget.services.store.listQuestions.length) {
                        await widget.services.store.submitTest();
                        Navigator.pop(context);
                      } else {
                        Navigator.pop(context);
                      }
                    },
                  ),
                ),
                const SizedBox(height: 24),
              ],
            );
          }),
        ),
      ),
    );
  }
}
