import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
// ignore: implementation_imports
import 'package:flutter/src/widgets/image.dart' as image;
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/ui/my_happiness/my_happiness_page.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:happy_place/ui/my_happiness/widgets/happiness_entry/line_chart_widget.dart';
import 'package:happy_place/ui/my_happiness/widgets/happiness_entry/entry_question_widget.dart';
import 'package:injectable/injectable.dart';

import '../../injection.dart';
import '../settings/information_page.dart';

@injectable
class HappinessEntryCategoryPage extends StatefulWidget {
  final TestPageServices services;
  final HappinessEntryPageParam? params;
  HappinessEntryCategoryPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<HappinessEntryCategoryPage> createState() =>
      _HappinessEntryCategoryPageState();
}

class _HappinessEntryCategoryPageState
    extends State<HappinessEntryCategoryPage> {
  late int showingTooltip;
  @override
  void initState() {
    widget.services.store.selectedQuestion = widget.params!.question;
    widget.services.store.getHappinessCategoryTestResult(
        widget.services.store.selectedQuestion!.title!);
    widget.services.store.getHappynessTestLatestResult(
        widget.services.store.selectedQuestion!.title!);
    super.initState();
  }

  double getInfoType() {
    double value = 1;

    if (widget.services.store.selectedQuestion!.title == "Basic Needs") {
      value = 1.1;
    } else if (widget.services.store.selectedQuestion!.title ==
        "Love & Belonging") {
      value = 1.2;
    } else if (widget.services.store.selectedQuestion!.title == "Self-Esteem") {
      value = 1.3;
    } else if (widget.services.store.selectedQuestion!.title ==
        "Beauty & Culture") {
      value = 1.4;
    } else if (widget.services.store.selectedQuestion!.title ==
        "Meaning of Life") {
      value = 1.5;
    }
    return value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.params!.test.name!),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: GestureDetector(
              onTap: () async {
                widget.services.store.init();
                await Navigator.push(
                  context,
                  MaterialPageRoute<void>(
                    builder: (context) => getIt<InformationPage>(
                        param1: InformationPageParams(
                      type: getInfoType(),
                    )),
                  ),
                );
              },
              child: Center(
                child: image.Image.asset(
                  'assets/images/ic_about.png',
                  height: 20,
                ),
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: Observer(builder: (context) {
            if (widget.services.store.loading) {
              return Center(
                  child: SizedBox(
                      height: MediaQuery.of(context).size.height / 1.5,
                      child: const Loader()));
            } else {
              return Padding(
                padding: const EdgeInsets.only(top: 25.0, bottom: 5),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      LineChartWidget(
                          list: widget
                              .services.store.listHappinessCategoryTestResult),
                      const SizedBox(height: 24),
                      EntryQuestionWidget(
                        buildContext: context,
                        store: widget.services.store,
                        question: widget.services.store.selectedQuestion!,
                        isFromView: true,
                      ),
                    ]),
              );
            }
          }),
        ),
      ),
    );
  }
}

class HappinessEntryPageParam {
  const HappinessEntryPageParam(
      {this.key, required this.test, required this.question});
  final Key? key;
  final HappynessTest test;
  final Question question;
}
