import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/extensions/colors.dart';
import 'package:happy_place/injection.dart';
import 'package:happy_place/ui/my_happiness/big_five_test_page.dart';
import 'package:happy_place/ui/my_happiness/my_happiness_page.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:happy_place/ui/my_happiness/widgets/big_five_test/bar_chart_widget.dart';
import 'package:happy_place/ui/my_happiness/widgets/big_five_test/big_five_result_card.dart';
import 'package:injectable/injectable.dart';

@injectable
class BigFiveResultPage extends StatefulWidget {
  final TestPageServices services;
  final TestPageParams? params;
  BigFiveResultPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<BigFiveResultPage> createState() => _BigFiveResultPageState();
}

class _BigFiveResultPageState extends State<BigFiveResultPage> {
  late int showingTooltip;
  @override
  void initState() {
    widget.services.store.getBigFiveTestResult();
    super.initState();
  }

  FutureOr _onCallBack(dynamic value) {
    widget.services.store.getBigFiveTestResult();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.params!.test.name!),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: Observer(builder: (context) {
            if (widget.services.store.loading) {
              return Center(
                child: SizedBox(
                    height: MediaQuery.of(context).size.height / 1.5,
                    child: const Loader()),
              );
            }
            return Padding(
              padding: const EdgeInsets.only(top: 25.0, bottom: 5),
              child: Column(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute<void>(
                                builder: (context) => getIt<BigFiveTestPage>(
                                  param1: TestPageParams(
                                    test: widget.params!.test,
                                  ),
                                ),
                              )).then(_onCallBack);
                        },
                        child: Container(
                          padding: const EdgeInsets.all(30.0),
                          child: Text("Take the Test",
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  ?.copyWith(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                      fontSize: 20)),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            border: Border.all(color: whiteType, width: 3),
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                      ),
                      const SizedBox(height: 25),
                      const Divider(thickness: 1),
                      const SizedBox(height: 12),
                    ],
                  ),
                  const Text(
                    'Result',
                    style: TextStyle(fontSize: 18),
                  ),
                  const SizedBox(height: 18),
                  widget.services.store.listBigFiveTestResult.isNotEmpty
                      ? Column(
                          children: [
                            BarChartWidget(store: widget.services.store),
                            ListView.builder(
                                shrinkWrap: true,
                                padding: const EdgeInsets.all(0),
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: widget.services.store
                                    .listBigFiveTestResult.length,
                                itemBuilder: (buildContext, index) {
                                  return BigFiveResultCard(
                                    title: widget.services.store
                                        .listBigFiveTestResult[index].question!,
                                    detail: widget
                                        .services
                                        .store
                                        .listBigFiveTestResult[index]
                                        .description!,
                                  );
                                })
                          ],
                        )
                      : const Text(
                          "Results are not available,\nTake the Test to see the Results",
                          style: TextStyle(
                              fontWeight: FontWeight.w500, color: Colors.grey),
                          textAlign: TextAlign.center,
                        )
                ],
              ),
            );
          }),
        ),
      ),
    );
  }
}
