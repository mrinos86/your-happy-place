import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
// ignore: implementation_imports
import 'package:flutter/src/widgets/image.dart' as image;
import 'package:happy_place/injection.dart';
import 'package:happy_place/ui/my_happiness/my_happiness_page.dart';
import 'package:happy_place/ui/settings/information_page.dart';
import 'package:happy_place/ui/widgets/app_button.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:happy_place/ui/my_happiness/widgets/hexaco_test/hexaco_question_widget.dart';
import 'package:injectable/injectable.dart';

@injectable
class HexacoTestPage extends StatefulWidget {
  final TestPageServices services;
  final TestPageParams? params;
  HexacoTestPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<HexacoTestPage> createState() => _HexacoTestPageState();
}

class _HexacoTestPageState extends State<HexacoTestPage> {
  late int showingTooltip;
  @override
  void initState() {
    widget.services.store.getTestQuestions(widget.params!.test);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.params!.test.name!),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: GestureDetector(
              onTap: () async {
                await Navigator.push(
                    context,
                    MaterialPageRoute<void>(
                      builder: (context) => getIt<InformationPage>(
                          param1: InformationPageParams(type: 2.1)),
                    ));
              },
              child: Center(
                child: image.Image.asset(
                  'assets/images/ic_about.png',
                  height: 20,
                ),
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: Observer(builder: (context) {
            if (widget.services.store.loading) {
              return Center(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height / 1.5,
                  child: const Loader(),
                ),
              );
            }
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 24),
                Center(
                  child: Text(
                    widget.services.store.listQuestions[0].title!,
                    style: Theme.of(context)
                        .textTheme
                        .headline6
                        ?.copyWith(fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(height: 8),
                Padding(
                  padding: const EdgeInsets.only(left: 25.0, right: 25),
                  child: Text(
                    widget.services.store.listQuestions[0].description!,
                    style: Theme.of(context).textTheme.bodySmall?.copyWith(
                        color: Colors.black, fontWeight: FontWeight.w300),
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(height: 12),
                HexacoQuestionWidget(
                  store: widget.services.store,
                  buildContext: context,
                ),
                const SizedBox(height: 24),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: 150,
                    child: AppButton(
                      title: 'Save',
                      onPressed: () async {
                        debugPrint("zzzzzzz::::");
                        debugPrint(widget.services.store.message);
                        await widget.services.store.submitTest();
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ),
                const SizedBox(height: 24),
              ],
            );
          }),
        ),
      ),
    );
  }
}
