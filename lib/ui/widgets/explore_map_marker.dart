// ignore_for_file: implementation_imports

import 'package:flutter/material.dart';
import 'package:happy_place/api_utils/http_exception.dart';
import 'package:flutter/src/widgets/image.dart' as image;
import 'package:happy_place/generated_api_code/api_client.swagger.dart';

class ExploreMapMarker extends StatelessWidget {
  final VisitedPlace post;
  const ExploreMapMarker(this.post, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        HttpExceptionNotifyUser.showMessage('tapped');
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: 8),
              if (post.feeling?.imageUrl != null)
                ClipRRect(
                  child: Column(
                    children: [
                      post.userName != ""
                          ? Container(
                              padding: const EdgeInsets.all(3),
                              color: Colors.white,
                              child: Text(
                                post.userName ?? '',
                                style: const TextStyle(
                                    color: Colors.purple,
                                    fontWeight: FontWeight.w600),
                              ))
                          : Container(),
                      image.Image.network(
                        post.feeling!.imageUrl!,
                        height: 50,
                        width: 48,
                        fit: BoxFit.fitWidth,
                        loadingBuilder: (BuildContext context, Widget child,
                            ImageChunkEvent? loadingProgress) {
                          if (loadingProgress == null) return child;
                          return Center(
                            child: CircularProgressIndicator(
                              value: loadingProgress.expectedTotalBytes != null
                                  ? loadingProgress.cumulativeBytesLoaded /
                                      loadingProgress.expectedTotalBytes!
                                  : null,
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
            ],
          ),
        ],
      ),
    );
  }
}

class CustomClipPath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width / 3, 0.0);
    path.lineTo(size.width / 2, size.height / 3);
    path.lineTo(size.width - size.width / 3, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
