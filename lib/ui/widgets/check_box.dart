import 'package:flutter/material.dart';

class CheckBoxWidget extends StatelessWidget {
  const CheckBoxWidget({
    Key? key,
    required this.value,
    required this.onChangedCallback,
    required this.text,
  }) : super(key: key);

  final bool value;
  final Function? onChangedCallback;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          height: 32,
          width: 32,
          child: Checkbox(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            value: value,
            onChanged: (i) {
              onChangedCallback!(i!);
            },
            fillColor:
                MaterialStateProperty.all(Theme.of(context).primaryColor),
          ),
        ),
        const SizedBox(width: 4),
        Text(text),
      ],
    );
  }
}
