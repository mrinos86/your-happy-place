import 'package:flutter/material.dart';

class AppTabLayout extends StatefulWidget {
  final String tab1Text;
  final String tab2Text;
  final String tab3Text;
  final Function(int) ontap;
  final int? alreadySelectedtab;

  const AppTabLayout({
    Key? key,
    required this.tab1Text,
    required this.tab2Text,
    required this.tab3Text,
    required this.ontap,
    this.alreadySelectedtab,
  }) : super(key: key);

  @override
  _AppTabLayoutState createState() => _AppTabLayoutState();
}

class _AppTabLayoutState extends State<AppTabLayout> {
  int tabSelected = 0;

  @override
  void initState() {
    if (widget.alreadySelectedtab != null) {
      tabSelected = widget.alreadySelectedtab!;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: PhysicalModel(
        color: Colors.white,
        elevation: 8,
        borderRadius: BorderRadius.circular(24),
        shadowColor: Colors.grey,
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(24),
          ),
          child: Row(
            children: [
              tabItem(selectedTabIndex: 0, text: widget.tab1Text),
              tabItem(selectedTabIndex: 1, text: widget.tab2Text),
              tabItem(selectedTabIndex: 2, text: widget.tab3Text),
            ],
          ),
        ),
      ),
    );
  }

  Widget tabItem({required String text, required int selectedTabIndex}) {
    return Expanded(
      child: GestureDetector(
        onTap: () {
          setState(() {
            tabSelected = selectedTabIndex;
          });
          widget.ontap.call(selectedTabIndex);
        },
        child: Container(
          color: Colors.transparent,
          child: Column(
            children: [
              Expanded(
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 14.0),
                    child: Text(
                      text,
                      style: Theme.of(context).textTheme.bodyText1?.copyWith(
                          fontWeight: FontWeight.bold,
                          color: tabSelected == selectedTabIndex
                              ? Colors.black
                              : Colors.grey),
                    ),
                  ),
                ),
              ),
              Container(
                width: 95,
                height: 5,
                decoration: BoxDecoration(
                  color: tabSelected == selectedTabIndex
                      ? Colors.grey.shade700
                      : Colors.transparent,
                  borderRadius: BorderRadius.circular(8),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
