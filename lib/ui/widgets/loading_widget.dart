import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  final double opacity;
  final Color color;

  const LoadingWidget({
    Key? key,
    this.opacity = 0.6,
    this.color = Colors.black87,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Opacity(
          opacity: opacity,
          child: ModalBarrier(dismissible: false, color: color),
        ),
        Center(
          child: Platform.isAndroid
              ? const CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Color(0xff83B4CF)),
                )
              : const CupertinoActivityIndicator(
                  animating: true,
                  radius: 16.0,
                ),
        ),
      ],
    );
  }
}
