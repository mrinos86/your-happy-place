// ignore_for_file: implementation_imports

import 'package:flutter/material.dart';
import 'package:happy_place/api_utils/http_exception.dart';
import 'package:flutter/src/widgets/image.dart' as image;
import 'package:happy_place/generated_api_code/api_client.swagger.dart';

class MapMarker extends StatelessWidget {
  final Entry post;

  const MapMarker(this.post, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (post.images?.isNotEmpty ?? false) {
      debugPrint('marker image is: ${post.images?.isNotEmpty ?? false}');
    }
    return GestureDetector(
      onTap: () {
        HttpExceptionNotifyUser.showMessage('tapped');
      },
      child: SizedBox(
        width: 100,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 110,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(14),
                  topRight: Radius.circular(14),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(height: 5),
                  Stack(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(14)),
                        height: 90,
                        width: 90,
                        child: Column(
                          children: const [
                            Text(" Loading...",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 12)),
                            Icon(Icons.image_outlined,
                                size: 70, color: Colors.white)
                          ],
                        ),
                      ),
                      if (post.images?.isNotEmpty ?? false)
                        ClipRRect(
                          borderRadius: BorderRadius.circular(14),
                          child: image.Image.network(
                            post.images!.first.imageThumbPath!,
                            height: 90,
                            width: 90,
                            fit: BoxFit.cover,
                            loadingBuilder: (BuildContext context, Widget child,
                                ImageChunkEvent? loadingProgress) {
                              if (loadingProgress == null) return child;
                              return Center(
                                child: CircularProgressIndicator(
                                  value: loadingProgress.expectedTotalBytes !=
                                          null
                                      ? loadingProgress.cumulativeBytesLoaded /
                                          loadingProgress.expectedTotalBytes!
                                      : null,
                                ),
                              );
                            },
                          ),
                        ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 100,
              alignment: Alignment.center,
              child: Text(
                post.images?.length.toString() ?? '0',
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold),
              ),
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                border: Border.all(
                  color: Theme.of(context).primaryColor,
                ),
                borderRadius: const BorderRadius.only(
                  bottomRight: Radius.circular(14),
                  bottomLeft: Radius.circular(14),
                ),
              ),
            ),
            ClipPath(
              clipper: CustomClipPath(),
              child: Container(
                height: 36.0,
                color: Theme.of(context).primaryColor,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CustomClipPath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width / 3, 0.0);
    path.lineTo(size.width / 2, size.height / 3);
    path.lineTo(size.width - size.width / 3, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
