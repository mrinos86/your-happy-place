import 'package:flutter/material.dart';

// ignore: must_be_immutable
class InfoAlert extends StatelessWidget {
  String title;
  String detail;
  String buttonText;
  InfoAlert(
      {Key? key,
      required this.title,
      required this.detail,
      required this.buttonText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.white,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(8.0),
        ),
      ),
      title: Text(title),
      content: Text(detail),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            buttonText,
            style: const TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }
}
