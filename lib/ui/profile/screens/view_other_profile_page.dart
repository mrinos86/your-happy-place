import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/ui/profile/screens/requests_page.dart';
import 'package:happy_place/ui/widgets/app_button.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:happy_place/ui/widgets/rounded_border_image.dart';
import 'package:injectable/injectable.dart';

class ViewOtherProfilePageParams {
  const ViewOtherProfilePageParams(
      {this.key, required this.userId, required this.isFromRequestPage});
  final Key? key;
  final String userId;
  final bool isFromRequestPage;
}

@injectable
class ViewOtherProfilePage extends StatefulWidget {
  final RequestsPageServices services;
  final ViewOtherProfilePageParams? params;
  ViewOtherProfilePage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<ViewOtherProfilePage> createState() => _ViewOtherProfilePageState();
}

class _ViewOtherProfilePageState extends State<ViewOtherProfilePage> {
  @override
  void initState() {
    widget.services.store.getOtherUser(userId: widget.params!.userId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("View Profile"),
        centerTitle: true,
      ),
      body: Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: SingleChildScrollView(
          child: Observer(builder: (context) {
            if (widget.services.store.isLoading) {
              return const Loader();
            } else if (widget.services.store.otherUsers != null) {
              return Stack(
                children: [
                  _coverBanner(context),
                  _mainContent(context),
                  _profileImage(context),
                ],
              );
            } else {
              return Container();
            }
          }),
        ),
      ),
    );
  }

  Widget _mainContent(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.3,
        left: 16.0,
        right: 16.0,
      ),
      child: Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(height: 50),
            Text(
              widget.services.store.otherUsers!.fullName!,
              style: Theme.of(context).textTheme.headline5?.copyWith(
                    fontWeight: FontWeight.w800,
                  ),
            ),
            const SizedBox(height: 8),
            Text(widget.services.store.otherUsers!.description ?? ""),
            SizedBox(
                height: widget.services.store.otherUsers!.description != null
                    ? 30
                    : 0),
            const Divider(
              thickness: 0.8,
            ),
            const SizedBox(height: 20),
            widget.params!.isFromRequestPage
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                          width: 160,
                          child: AppButton(
                              title: "Accept",
                              onPressed: () {
                                widget.services.store.acceptRequest(
                                    int.parse(widget.params!.userId));
                                Navigator.pop(context);
                              })),
                      const SizedBox(width: 15),
                      SizedBox(
                        width: 160,
                        child: AppButton(
                          title: "Decline",
                          backgroundColor: Colors.red,
                          onPressed: () {
                            widget.services.store.rejectRequest(
                                int.parse(widget.params!.userId));
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ],
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  Widget _profileImage(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 16.0,
        top: (MediaQuery.of(context).size.height * 0.3) - 40,
      ),
      child: RoundedBorderBoxImage(
        size: 80,
        borderRadius: 12,
        profileUrl: widget.services.store.otherUsers?.avatarUrl,
      ),
    );
  }

  Widget _coverBanner(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.3,
      child: Stack(
        children: [
          Image.network(
            widget.services.store.otherUsers?.coverImageUrl ?? '',
            height: MediaQuery.of(context).size.height * 0.3,
            width: double.infinity,
            fit: BoxFit.cover,
            errorBuilder: (BuildContext context, Object exception, stackTrace) {
              return Image.asset(
                'assets/images/default_background.jpg',
                height: MediaQuery.of(context).size.height * 0.3,
                width: double.infinity,
                fit: BoxFit.cover,
              );
            },
          ),
        ],
      ),
    );
  }
}
