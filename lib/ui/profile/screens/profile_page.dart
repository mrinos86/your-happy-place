import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/data/stores/profile/group_store.dart';
import 'package:happy_place/data/stores/profile/profile_store.dart';
import 'package:happy_place/injection.dart';
import 'package:happy_place/ui/profile/screens/view_other_profile_page.dart';
import 'package:happy_place/ui/widgets/app_tab_layout.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:happy_place/ui/profile/widgets/group_rounded_border_image.dart';
import 'package:happy_place/ui/widgets/rounded_border_image.dart';
import 'package:injectable/injectable.dart';

import 'add_people.dart';
import 'edit_profile_page.dart';
import 'group/add_group_member_page.dart';
import 'group/create_edit_group_page.dart';
import 'group/view_group_page.dart';

class ProfilePageParams {
  const ProfilePageParams(this.key);
  final Key key;
}

@injectable
class ProfilePageServices {
  ProfileStore store;
  ProfilePageServices({
    required this.store,
  });
}

@injectable
class ProfilePage extends StatefulWidget {
  final ProfilePageServices services;
  final ProfilePageParams? params;
  final GroupStore groupStore;
  ProfilePage({
    @factoryParam this.params,
    required this.services,
    required this.groupStore,
  }) : super(key: params?.key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  void initState() {
    widget.services.store.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: SingleChildScrollView(
        child: Observer(builder: (context) {
          if (widget.services.store.isLoading) {
            return const Loader();
          } else {
            return Stack(
              children: [
                _coverBanner(context),
                _mainContent(context),
                _profileImage(context),
              ],
            );
          }
        }),
      ),
    );
  }

  Widget _mainContent(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.3,
        left: 16.0,
        right: 16.0,
      ),
      child: Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Align(
              alignment: Alignment.topRight,
              child: Padding(
                padding: const EdgeInsets.only(top: 12.0),
                child: GestureDetector(
                  onTap: () async {
                    var ss = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => getIt<EditProfilePage>(),
                      ),
                    );
                    if (ss != null) {
                      debugPrint('edit profile page returned back -- $ss');
                      widget.services.store.init();
                    }
                  },
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Image.asset(
                        'assets/images/ic_edit_profile.png',
                        height: 16,
                        color: Colors.grey.shade500,
                      ),
                      const SizedBox(width: 4),
                      Text(
                        'Edit',
                        style: Theme.of(context).textTheme.bodyText1?.copyWith(
                              color: Colors.grey.shade500,
                            ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(height: 24),
            Text(
              widget.services.store.user?.fullName ?? '',
              style: Theme.of(context).textTheme.headline5?.copyWith(
                    fontWeight: FontWeight.w800,
                  ),
            ),
            const SizedBox(height: 8),
            if (widget.services.store.user?.description != null)
              Text(
                widget.services.store.user?.description ?? '',
              ),
            if (widget.services.store.user?.description != null)
              const SizedBox(height: 32),
            if (widget.services.store.user?.description == null)
              const SizedBox(height: 16),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'My People',
                  style: Theme.of(context).textTheme.bodyText1?.copyWith(
                        color: Colors.grey,
                      ),
                ),
                widget.services.store.deletionMode
                    ? Container()
                    : Row(
                        children: [
                          widget.services.store.peopleCategory == "Groups"
                              ? Container()
                              : GestureDetector(
                                  onTap: () {
                                    debugPrint('pressed');
                                    widget.services.store.deletionMode = true;
                                    widget.services.store.deletionMode =
                                        widget.services.store.deletionMode;
                                  },
                                  child: Image.asset(
                                    'assets/images/ic_delete.png',
                                    height: 22,
                                    color: Colors.grey,
                                  ),
                                ),
                          const SizedBox(width: 8),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute<void>(
                                  builder: (context) =>
                                      widget.services.store.peopleCategory ==
                                              'Groups'
                                          ? getIt<AddGroupMemberPage>(
                                              param2: widget.services.store,
                                              param1: AddGroupMemberPageParams(
                                                  isNew: true))
                                          : getIt<AddPeoplePage>(),
                                ),
                              );
                            },
                            child: Icon(
                              Icons.add_circle_outline,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ],
                      )
              ],
            ),
            const SizedBox(height: 24),
            widget.services.store.deletionMode
                ? _deleteLayout()
                : _friendsFamilyGroupSegment(),
            const SizedBox(height: 12),
            _list(),
          ],
        ),
      ),
    );
  }

  Widget _list() {
    return Observer(builder: (context) {
      var getDeleteMode = widget.services.store.deletionMode;
      if (widget.services.store.peopleCategory == 'Friends') {
        if (widget.services.store.friends.isEmpty) {
          return Column(
            children: const [
              SizedBox(height: 20),
              Center(child: Text('No Friends Added')),
            ],
          );
        }
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: widget.services.store.friends.length,
              separatorBuilder: (context, index) {
                return const Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: Divider(),
                );
              },
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    if (!getDeleteMode) {
                      Navigator.push(
                        context,
                        MaterialPageRoute<void>(
                          builder: (context) => getIt<ViewOtherProfilePage>(
                            param1: ViewOtherProfilePageParams(
                              userId:
                                  widget.services.store.friends[index].user.id!,
                              isFromRequestPage: false,
                            ),
                          ),
                        ),
                      );
                    }
                  },
                  child: Row(
                    children: [
                      RoundedBorderBoxImage(
                          borderColor: Colors.transparent,
                          size: 40,
                          borderRadius: 100,
                          profileUrl: widget
                              .services.store.friends[index].user.avatarUrl),
                      const SizedBox(width: 16),
                      Expanded(
                          child: Text(widget
                              .services.store.friends[index].user.fullName!)),
                      const SizedBox(width: 16),
                      getDeleteMode
                          ? Checkbox(
                              value:
                                  widget.services.store.friends[index].checked,
                              onChanged: (v) {
                                widget.services.store.friends[index].checked =
                                    v!;
                                widget.services.store.friends =
                                    widget.services.store.friends;
                              },
                              fillColor: MaterialStateProperty.all(
                                  Theme.of(context).primaryColor),
                            )
                          : const SizedBox(),
                    ],
                  ),
                );
              }),
        );
      } else if (widget.services.store.peopleCategory == 'Family') {
        //family
        if (widget.services.store.family.isEmpty) {
          return Column(
            children: const [
              SizedBox(height: 20),
              Center(child: Text('No Family Added')),
            ],
          );
        }
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: widget.services.store.family.length,
              separatorBuilder: (context, index) {
                return const Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: Divider(),
                );
              },
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    if (!getDeleteMode) {
                      Navigator.push(
                        context,
                        MaterialPageRoute<void>(
                          builder: (context) => getIt<ViewOtherProfilePage>(
                            param1: ViewOtherProfilePageParams(
                              userId:
                                  widget.services.store.family[index].user.id!,
                              isFromRequestPage: false,
                            ),
                          ),
                        ),
                      );
                    }
                  },
                  child: Row(
                    children: [
                      RoundedBorderBoxImage(
                          borderColor: Colors.transparent,
                          size: 40,
                          borderRadius: 100,
                          profileUrl: widget
                              .services.store.family[index].user.avatarUrl),
                      const SizedBox(width: 16),
                      Expanded(
                          child: Text(widget
                              .services.store.family[index].user.fullName!)),
                      const SizedBox(width: 16),
                      getDeleteMode
                          ? Checkbox(
                              value:
                                  widget.services.store.family[index].checked,
                              onChanged: (v) {
                                widget.services.store.family[index].checked =
                                    v!;
                                widget.services.store.family =
                                    widget.services.store.family;
                              },
                              fillColor: MaterialStateProperty.all(
                                  Theme.of(context).primaryColor),
                            )
                          : const SizedBox(),
                    ],
                  ),
                );
              }),
        );
      } else if ((widget.services.store.peopleCategory == 'Groups')) {
        // Groups
        if (widget.services.store.groupListLoading) {
          return const Loader();
        }
        if (widget.services.store.groups.isEmpty) {
          return Column(
            children: const [
              SizedBox(height: 20),
              Center(child: Text('No Groups Added'))
            ],
          );
        }
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: widget.services.store.groups.length,
              separatorBuilder: (context, index) {
                return const Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: Divider(),
                );
              },
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    onPressedViewGroup(index);
                  },
                  child: Row(
                    children: [
                      GroupRoundedBordeImage(
                        borderColor: Colors.transparent,
                        size: 40,
                        borderRadius: 100,
                        profileUrl:
                            widget.services.store.groups[index].group.image,
                        defaultIconSize: 20,
                        isPickedImage: false,
                      ),
                      const SizedBox(width: 16),
                      Expanded(
                          child: Text(
                              widget.services.store.groups[index].group.name!)),
                      const SizedBox(width: 16),
                      getDeleteMode
                          ? Checkbox(
                              value:
                                  widget.services.store.groups[index].checked,
                              onChanged: (v) {
                                widget.services.store.groups[index].checked =
                                    v!;
                                widget.services.store.groups =
                                    widget.services.store.groups;
                              },
                              fillColor: MaterialStateProperty.all(
                                  Theme.of(context).primaryColor),
                            )
                          : const SizedBox(),
                      widget.services.store.deletionMode
                          ? Container()
                          : Container(),
                      Column(
                        children: [
                          const SizedBox(width: 16),
                          GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute<void>(
                                    builder: (context) =>
                                        getIt<CreateEditGroupPage>(
                                            param1: CreateEditPageParams(
                                      title: "Edit Group",
                                      group: widget
                                          .services.store.groups[index].group,
                                      isNew: false,
                                    )),
                                  ),
                                );
                              },
                              child: Chip(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 10),
                                  label: const Text('Edit',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 10)),
                                  backgroundColor:
                                      Theme.of(context).primaryColor)),
                        ],
                      ),
                    ],
                  ),
                );
              }),
        );
      } else {
        return Container();
      }
    });
  }

  Widget _deleteLayout() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                SizedBox(
                  height: 20,
                  width: 20,
                  child: Checkbox(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    value: widget.services.store.checkAll,
                    onChanged: (s) {
                      widget.services.store.checkAllItems(s!);
                    },
                    fillColor: MaterialStateProperty.all(
                        Theme.of(context).primaryColor),
                  ),
                ),
                const SizedBox(width: 3),
                const Text('Select all')
              ],
            ),
            GestureDetector(
              onTap: () {
                widget.services.store.deletionMode = false;
              },
              child: const Text('Cancel'),
            ),
            GestureDetector(
              onTap: () {
                widget.services.store.deleteFriendsAndFamily();
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(80),
                  color: Colors.red.shade900,
                ),
                child: const Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                  child: Text(
                    'Delete',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            )
          ],
        ),
        const SizedBox(height: 5),
        const Divider(thickness: 1),
      ],
    );
  }

  Widget _friendsFamilyGroupSegment() {
    return AppTabLayout(
      tab1Text: 'Friends',
      tab2Text: 'Family',
      tab3Text: 'Groups',
      alreadySelectedtab: widget.services.store.peopleCategory == 'Friends'
          ? 0
          : widget.services.store.peopleCategory == 'Family'
              ? 1
              : 2,
      ontap: (s) {
        if (s == 0) {
          // friends
          widget.services.store.updatePeopleCategory('Friends');
        } else if (s == 1) {
          // family
          widget.services.store.updatePeopleCategory('Family');
        } else {
          // groups
          widget.services.store.updatePeopleCategory('Groups');
        }
      },
    );
  }

  Widget _profileImage(BuildContext context) {
    debugPrint('profile url: ${widget.services.store.user?.avatarUrl}');
    return Padding(
      padding: EdgeInsets.only(
        left: 16.0,
        top: (MediaQuery.of(context).size.height * 0.3) - 40,
      ),
      child: RoundedBorderBoxImage(
        size: 80,
        borderRadius: 12,
        profileUrl: widget.services.store.user?.avatarUrl,
      ),
    );
  }

  Widget _coverBanner(BuildContext context) {
    debugPrint('cover url: ${widget.services.store.user?.coverImageUrl}');

    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.3,
      child: Stack(
        children: [
          Image.network(
            widget.services.store.user?.coverImageUrl ?? '',
            height: MediaQuery.of(context).size.height * 0.3,
            width: double.infinity,
            fit: BoxFit.cover,
            errorBuilder: (BuildContext context, Object exception, stackTrace) {
              return Image.asset(
                'assets/images/default_background.jpg',
                height: MediaQuery.of(context).size.height * 0.3,
                width: double.infinity,
                fit: BoxFit.cover,
              );
            },
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: GestureDetector(
                onTap: () {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ListTile(
                              title: const Text("Gallery"),
                              leading: const Icon(Icons.image),
                              onTap: () {
                                Navigator.pop(context);
                                widget.services.store.pickCoverImage();
                              },
                            ),
                            ListTile(
                              title: const Text("Camera"),
                              leading: const Icon(Icons.camera),
                              onTap: () {
                                Navigator.pop(context);
                                widget.services.store.captureCoverImage();
                              },
                            ),
                            const SizedBox(height: 8),
                          ],
                        );
                      });
                },
                child: Image.asset(
                  'assets/images/btn_add_photo.png',
                  height: 50,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  onPressedViewGroup(index) {
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    // check admin user
    bool adminUser = false;
    for (var element
        in widget.services.store.groups[index].group.groupmembers!) {
      if (element.type == "2") {
        if (widget.services.store.user!.id == element.userId) {
          adminUser = true;
        }
      }
    }

    Navigator.push(
      context,
      MaterialPageRoute<void>(
        builder: (context) => getIt<ViewGroupPage>(
          param1: ViewGroupPageParams(
            title: widget.services.store.groups[index].group.name!,
            groupId: int.parse(widget.services.store.groups[index].group.id!),
            isAdmin: adminUser,
          ),
        ),
      ),
    );
    // });
  }
}
