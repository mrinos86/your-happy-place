// ignore_for_file: implementation_imports

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/data/stores/profile/group_store.dart';
import 'package:happy_place/generated/l10n.dart';
import 'package:happy_place/generated_api_code/api_client.swagger.dart';
import 'package:happy_place/ui/widgets/app_button.dart';
import 'package:happy_place/ui/widgets/app_edit_text.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:happy_place/ui/profile/widgets/group_rounded_border_image.dart';
import 'package:injectable/injectable.dart';

class CreateEditPageParams {
  CreateEditPageParams({
    this.key,
    required this.title,
    this.group,
    required this.isNew,
  });
  final Key? key;
  String title;
  Group? group;
  bool isNew;
}

@injectable
class GroupServices {
  GroupStore store;
  GroupServices({
    required this.store,
  });
}

@injectable
class CreateEditGroupPage extends StatefulWidget {
  final GroupServices services;
  final CreateEditPageParams? params;

  CreateEditGroupPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<CreateEditGroupPage> createState() => _CreateEditGroupPageState();
}

class _CreateEditGroupPageState extends State<CreateEditGroupPage> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController groupNameController = TextEditingController();

  @override
  void initState() {
    widget.services.store.pickedGroupImage = null;
    widget.services.store.isGroupUpdateDisabled = true;

    if (!widget.params!.isNew) {
      groupNameController.text = widget.params!.group!.name!;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: Observer(builder: (context) {
          return widget.services.store.isLoading
              ? Container()
              : GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: const Icon(Icons.close));
        }),
        title: Text(widget.params!.title),
        centerTitle: true,
      ),
      body: Observer(builder: (context) {
        return widget.services.store.isLoading
            ? const Loader()
            : Container(
                padding: const EdgeInsets.all(20.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      const SizedBox(height: 24),
                      _groupPicture(context),
                      const SizedBox(height: 10),
                      AppEditText(
                          headingText: 'Group Name',
                          hint: "",
                          textController: groupNameController,
                          filled: false,
                          backgroundcolor: Colors.white,
                          validator: (value) {
                            if (value == null || value.trim().isEmpty) {
                              return S.of(context).enter_group_name;
                            } else {
                              return null;
                            }
                          },
                          onChanged: (c) {
                            if (!widget.params!.isNew) {
                              checkUpdateEnable();
                            }
                          },
                          borderColor: Colors.grey.shade400,
                          radius: 2),
                      const SizedBox(height: 24),
                      SizedBox(
                          width: 200,
                          child: AppButton(
                              disabled: widget.params!.isNew
                                  ? false
                                  : widget.services.store.isGroupUpdateDisabled,
                              title: widget.params!.isNew ? 'Create' : 'Update',
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  if (widget.params!.isNew) {
                                    widget.services.store.createGroup(
                                        groupName: groupNameController.text);
                                  } else {
                                    widget.services.store.updateGroup(
                                        groupName: groupNameController.text,
                                        groupId: widget.params!.group!.id!
                                            .toString());
                                  }
                                }
                              })),
                    ],
                  ),
                ),
              );
      }),
    );
  }

  Widget _groupPicture(BuildContext context) {
    return Observer(builder: (context) {
      return Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: GroupRoundedBordeImage(
              borderColor: Theme.of(context).primaryColor,
              size: 150,
              borderWidth: 2,
              borderRadius: 150,
              profileUrl: widget.params!.isNew
                  ? widget.services.store.pickedGroupImage
                  : widget.services.store.pickedGroupImage ??
                      widget.params!.group!.image,
              isPickedImage:
                  widget.services.store.pickedGroupImage != null ? true : false,
              defaultIconSize: 100,
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.only(top: 127),
              child: GestureDetector(
                onTap: () {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ListTile(
                              title: const Text("Gallery"),
                              leading: const Icon(Icons.image),
                              onTap: () {
                                Navigator.pop(context);
                                widget.services.store.chooseGroupPhoto(
                                  isCamera: false,
                                );
                              },
                            ),
                            ListTile(
                              title: const Text("Camera"),
                              leading: const Icon(Icons.camera),
                              onTap: () {
                                Navigator.pop(context);
                                widget.services.store.chooseGroupPhoto(
                                  isCamera: true,
                                );
                              },
                            ),
                            const SizedBox(height: 8),
                          ],
                        );
                      });
                },
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(200),
                    color: Theme.of(context).primaryColor,
                  ),
                  child: const Center(
                    child: Icon(
                      Icons.camera_alt,
                      color: Colors.white,
                      size: 24,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      );
    });
  }

  checkUpdateEnable() {
    widget.services.store.isGroupUpdateDisabled =
        widget.params!.group!.name != groupNameController.text ? false : true;
  }
}
