// ignore_for_file: implementation_imports

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/api_utils/http_exception.dart';
import 'package:happy_place/injection.dart';
import 'package:happy_place/models/user_with_wrapper.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:happy_place/ui/widgets/rounded_border_image.dart';
import 'package:injectable/injectable.dart';

import 'create_edit_group_page.dart';

class AddGroupMemberPageParams {
  AddGroupMemberPageParams({
    this.key,
    required this.isNew,
  });
  final Key? key;
  bool isNew;
}

@injectable
class AddGroupMemberPage extends StatefulWidget {
  final GroupServices services;
  final AddGroupMemberPageParams? params;
  AddGroupMemberPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<AddGroupMemberPage> createState() => _AddGroupMemberPageState();
}

class _AddGroupMemberPageState extends State<AddGroupMemberPage> {
  final TextEditingController _textController = TextEditingController();

  @override
  void initState() {
    initPage();
    super.initState();
  }

  initPage() async {
    await widget.services.store.getMyFriendsFamily("");
    widget.services.store.friendsFamilyCheckedList = [];

    if (!widget.params!.isNew) {
      checkExistingGroupMembers();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Add Members"),
        actions: [
          GestureDetector(
              onTap: () async {
                if (widget.services.store.friendsFamilyCheckedList.isNotEmpty) {
                  if (widget.params!.isNew) {
                    Navigator.push(
                        context,
                        MaterialPageRoute<void>(
                          builder: (context) => getIt<CreateEditGroupPage>(
                              param1: CreateEditPageParams(
                            title: "New Group",
                            isNew: true,
                          )),
                        ));
                  } else {
                    widget.services.store.addMembersToGroup(
                        groupId:
                            int.parse(widget.services.store.viewedGroup!.id!));
                  }
                } else {
                  HttpExceptionNotifyUser.showError(
                      "Select atleast one member");
                }
              },
              child: Center(
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      widget.params!.isNew ? "Done" : "Save",
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold),
                    )),
              )),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          children: [
            const SizedBox(height: 24),
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
              ),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: TextFormField(
                        controller: _textController,
                        decoration: InputDecoration(
                          isDense: true,
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          contentPadding:
                              const EdgeInsets.only(left: 12, right: 12),
                          hintText: "Search",
                          hintStyle:
                              Theme.of(context).textTheme.bodyText1?.copyWith(
                                    color: Colors.grey.shade400,
                                  ),
                        ),
                        onChanged: onTextChanged,
                      ),
                    ),
                    Icon(
                      Icons.search,
                      size: 22,
                      color: Colors.grey.shade400,
                    ),
                  ],
                ),
              ),
            ),
            Observer(builder: (context) {
              if (widget.services.store.isLoading) {
                return const Loader();
              }
              return Expanded(
                child: Column(
                  children: [
                    widget.services.store.friendsFamilyCheckedList.isNotEmpty
                        ? Column(
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: SizedBox(
                                  height: 60,
                                  child: ListView.builder(
                                      shrinkWrap: true,
                                      physics: const ClampingScrollPhysics(),
                                      scrollDirection: Axis.horizontal,
                                      itemCount: widget.services.store
                                          .friendsFamilyCheckedList.length,
                                      itemBuilder: (context, index) {
                                        return Stack(
                                          alignment: Alignment.bottomRight,
                                          children: [
                                            RoundedBorderBoxImage(
                                              borderColor: Colors.transparent,
                                              size: 50,
                                              borderRadius: 100,
                                              profileUrl: widget
                                                  .services
                                                  .store
                                                  .friendsFamilyCheckedList[
                                                      index]
                                                  .user
                                                  .avatarUrl,
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                onRemoveButtonPressed(index);
                                              },
                                              child: const Icon(
                                                Icons.cancel_rounded,
                                                size: 20,
                                                color: Colors.red,
                                              ),
                                            )
                                          ],
                                        );
                                      }),
                                ),
                              ),
                              const SizedBox(height: 15),
                            ],
                          )
                        : const SizedBox(height: 24),
                    widget.services.store.friendsFamily.isEmpty
                        ? const Center(child: Text('Members not found'))
                        : Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16.0),
                              child: ListView.separated(
                                  shrinkWrap: true,
                                  physics: const ScrollPhysics(),
                                  itemCount: widget
                                      .services.store.friendsFamily.length,
                                  separatorBuilder: (context, index) {
                                    return const Padding(
                                      padding: EdgeInsets.only(left: 10.0),
                                      child: Divider(thickness: 1),
                                    );
                                  },
                                  itemBuilder: (context, index) {
                                    return Row(
                                      children: [
                                        RoundedBorderBoxImage(
                                          borderColor: Colors.transparent,
                                          size: 50,
                                          borderRadius: 100,
                                          profileUrl: widget
                                              .services
                                              .store
                                              .friendsFamily[index]
                                              .user
                                              .avatarUrl,
                                        ),
                                        const SizedBox(width: 16),
                                        Expanded(
                                            child: Text(
                                          widget
                                              .services
                                              .store
                                              .friendsFamily[index]
                                              .user
                                              .fullName!,
                                          style: const TextStyle(
                                              fontWeight: FontWeight.w600),
                                        )),
                                        const SizedBox(width: 16),
                                        Checkbox(
                                          value: widget.services.store
                                              .friendsFamily[index].checked,
                                          onChanged: (v) {
                                            onCheckBoxPressed(
                                                index: index, value: v!);
                                          },
                                          fillColor: MaterialStateProperty.all(
                                              Theme.of(context).primaryColor),
                                        )
                                      ],
                                    );
                                  }),
                            ),
                          )
                  ],
                ),
              );
            }),
          ],
        ),
      ),
    );
  }

  checkExistingGroupMembers() {
    for (var element in widget.services.store.viewedGroup!.groupmembers!) {
      var id = element.userId;
      // cheking friendfamily list
      widget.services.store.friendsFamily
          .removeWhere((item) => item.user.id == id);
      widget.services.store.friendsFamily = widget.services.store.friendsFamily;
    }
  }

  onTextChanged(text) async {
    widget.services.store.friendsFamily.clear();
    await widget.services.store.getMyFriendsFamily(text);

    if (!widget.params!.isNew) {
      checkExistingGroupMembers();
    }

    for (var element in widget.services.store.friendsFamilyCheckedList) {
      for (var item in widget.services.store.friendsFamily) {
        if (item.user.id == element.user.id) {
          var index = widget.services.store.friendsFamily.indexOf(item);
          widget.services.store.friendsFamily[index].checked = true;
        }
      }
    }
    widget.services.store.friendsFamily = widget.services.store.friendsFamily;
  }

  onRemoveButtonPressed(index) {
    var id = widget.services.store.friendsFamilyCheckedList[index].user.id;

    // uncheck
    for (var item in widget.services.store.friendsFamily) {
      if (item.user.id == id) {
        var index = widget.services.store.friendsFamily.indexOf(item);
        widget.services.store.friendsFamily[index].checked = false;
      }
    }

    // remove
    widget.services.store.friendsFamilyCheckedList.removeAt(index);

    widget.services.store.friendsFamily = widget.services.store.friendsFamily;
  }

  onCheckBoxPressed({required int index, required bool value}) {
    widget.services.store.friendsFamily[index].checked = value;
    widget.services.store.friendsFamilyCheckedList.clear();

    if (!widget.params!.isNew) {
      checkExistingGroupMembers();
    }

    for (var u in widget.services.store.friendsFamily) {
      if (u.checked) {
        widget.services.store.friendsFamilyCheckedList
            .add(UserWithWrapper(user: u.user, checked: true));
      }
    }

    widget.services.store.friendsFamily = widget.services.store.friendsFamily;
  }
}
