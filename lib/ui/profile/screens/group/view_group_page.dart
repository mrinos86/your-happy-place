// ignore_for_file: implementation_imports

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/api_utils/http_exception.dart';
import 'package:happy_place/injection.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:happy_place/ui/profile/widgets/group_rounded_border_image.dart';
import 'package:happy_place/ui/widgets/rounded_border_image.dart';
import 'package:injectable/injectable.dart';
import 'package:flutter/src/widgets/image.dart' as image;

import 'add_group_member_page.dart';
import 'block_or_report_page.dart';
import 'create_edit_group_page.dart';

class ViewGroupPageParams {
  const ViewGroupPageParams({
    this.key,
    required this.title,
    required this.groupId,
    required this.isAdmin,
  });
  final Key? key;
  final String title;
  final int groupId;
  final bool isAdmin;
}

@injectable
class ViewGroupPage extends StatefulWidget {
  final GroupServices services;
  final ViewGroupPageParams? params;
  ViewGroupPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<ViewGroupPage> createState() => _ViewGroupPageState();
}

class _ViewGroupPageState extends State<ViewGroupPage> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final _menuKey = GlobalKey<PopupMenuButtonState>();
  TextEditingController textController = TextEditingController();

  @override
  void initState() {
    loadPageInitialData();
    super.initState();
  }

  loadPageInitialData() async {
    widget.services.store.groupMemberDeleteMode = false;
    widget.services.store.groupMembersList = [];
    widget.services.store.viewedGroup = null;
    await widget.services.store
        .getGroupDetails(groupId: widget.params!.groupId);
    await widget.services.store
        .getGroupMemberList(groupId: widget.params!.groupId, q: '');
  }

  setupDeleteModeView(bool value) {
    widget.services.store.groupMemberDeleteMode = value;
    widget.services.store.groupMemberDeleteMode =
        widget.services.store.groupMemberDeleteMode;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.params!.title),
        actions: [
          PopupMenuButton(
            key: _menuKey,
            child: GestureDetector(
              onTap: () async {
                if (FocusScope.of(context).hasFocus) {
                  FocusScope.of(context).unfocus();
                  await Future.delayed(const Duration(milliseconds: 400));
                }
                _menuKey.currentState!.showButtonMenu();
              },
              child: const Padding(
                  padding: EdgeInsets.all(8.0), child: Icon(Icons.more_vert)),
            ),
            offset: const Offset(-20, 40),
            itemBuilder: (context) => [
              PopupMenuItem(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    Icon(Icons.block, color: Colors.red),
                    SizedBox(width: 15),
                    Text("Block or Report")
                  ],
                ),
                value: 1,
              ),
              PopupMenuItem(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    Icon(Icons.logout, color: Colors.red),
                    SizedBox(width: 15),
                    Text("Leave Group", style: TextStyle(color: Colors.red)),
                  ],
                ),
                value: 2,
              )
            ],
            onSelected: (value) {
              if (value == 1) {
                // block or report page
                Navigator.push(
                    context,
                    MaterialPageRoute<void>(
                        builder: (context) => getIt<BlockOrReportPage>(
                            param1: BlockOrReportParams(
                                groupId: widget.params!.groupId))));
              } else {
                // leave from group..
                widget.services.store
                    .leaveFromGroup(groupId: widget.params!.groupId);
              }
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0),
        child: _body(context),
      ),
    );
  }

  Widget _body(BuildContext context) {
    return Observer(builder: (context) {
      if (widget.services.store.isLoading) {
        return const Loader();
      }
      return Form(
        key: _formKey,
        child: Column(
          children: [
            const SizedBox(height: 24),
            _profilePictureWidget(context),
            const SizedBox(height: 24),
            _memberCountAndButtonWidget(),
            _searchBarWidget(),
            const SizedBox(height: 24),
            _deleteLayoutWidget(),
            _groupMembersListWidget(),
          ],
        ),
      );
    });
  }

  Widget _profilePictureWidget(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: GroupRoundedBordeImage(
        borderColor: Theme.of(context).primaryColor,
        size: 150,
        borderWidth: 2,
        borderRadius: 150,
        profileUrl: widget.services.store.viewedGroup!.image,
        isPickedImage: false,
        defaultIconSize: 100,
      ),
    );
  }

  Widget _memberCountAndButtonWidget() {
    return widget.services.store.groupMemberDeleteMode
        ? const SizedBox()
        : Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                widget.services.store.viewedGroup != null
                    ? '${widget.services.store.viewedGroup!.groupmembers!.length} Members'
                    : widget.services.store.groupMembersList.isNotEmpty
                        ? '${widget.services.store.groupMembersList.length} Members'
                        : '',
                style: Theme.of(context).textTheme.bodyText1?.copyWith(
                      color: Colors.grey,
                    ),
              ),
              widget.params!.isAdmin
                  ? Row(
                      children: [
                        GestureDetector(
                          onTap: _onPressedDeleteIcon,
                          child: image.Image.asset(
                            'assets/images/ic_delete.png',
                            height: 22,
                            color: Colors.grey,
                          ),
                        ),
                        const SizedBox(width: 8),
                        GestureDetector(
                          onTap: () {
                            setupDeleteModeView(false);
                            Navigator.push(
                              context,
                              MaterialPageRoute<void>(
                                builder: (context) => getIt<AddGroupMemberPage>(
                                  param1: AddGroupMemberPageParams(
                                    isNew: false,
                                  ),
                                ),
                              ),
                            );
                          },
                          child: Icon(
                            Icons.add_circle_outline,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                      ],
                    )
                  : Container()
            ],
          );
  }

  Widget _deleteLayoutWidget() {
    return widget.services.store.groupMemberDeleteMode
        ? Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SizedBox(
                        height: 20,
                        width: 20,
                        child: Checkbox(
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          value: widget.services.store.ischeckedAllGMembers,
                          onChanged: (s) {
                            widget.services.store.checkAllGroupMember(s!);
                          },
                          fillColor: MaterialStateProperty.all(
                              Theme.of(context).primaryColor),
                        ),
                      ),
                      const SizedBox(width: 3),
                      const Text('Select all')
                    ],
                  ),
                  GestureDetector(
                    onTap: () {
                      setupDeleteModeView(false);
                      widget.services.store.checkAllGroupMember(false);
                    },
                    child: const Text('Cancel'),
                  ),
                  GestureDetector(
                    onTap: _onPressedDeleteButton,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(80),
                        color: Colors.red.shade900,
                      ),
                      child: const Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 16.0),
                        child: Text(
                          'Delete',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              const SizedBox(height: 5),
              const Divider(thickness: 1),
              const SizedBox(height: 20),
            ],
          )
        : Container();
  }

  Widget _searchBarWidget() {
    return widget.services.store.groupMemberDeleteMode
        ? const SizedBox()
        : Column(
            children: [
              const SizedBox(height: 24),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 8.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: TextFormField(
                          controller: textController,
                          decoration: InputDecoration(
                            isDense: true,
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            contentPadding:
                                const EdgeInsets.only(left: 12, right: 12),
                            hintText: "Search",
                            hintStyle:
                                Theme.of(context).textTheme.bodyText1?.copyWith(
                                      color: Colors.grey.shade400,
                                    ),
                          ),
                          onChanged: (text) async {
                            widget.services.store.groupMembersList = [];
                            await widget.services.store.getGroupMemberList(
                                groupId: widget.params!.groupId, q: text);
                          },
                        ),
                      ),
                      Icon(
                        Icons.search,
                        size: 22,
                        color: Colors.grey.shade400,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
  }

  Widget _groupMembersListWidget() {
    return widget.services.store.groupMembersListLoading
        ? const Loader()
        : widget.services.store.groupMembersList.isEmpty
            ? const Center(child: Text('Members not found'))
            : Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: ListView.separated(
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      itemCount: widget.services.store.groupMembersList.length,
                      separatorBuilder: (context, index) {
                        return const Padding(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Divider(thickness: 1),
                        );
                      },
                      itemBuilder: (context, index) {
                        return Row(
                          children: [
                            const RoundedBorderBoxImage(
                              borderColor: Colors.transparent,
                              size: 40,
                              borderRadius: 100,
                              profileUrl: null,
                            ),
                            const SizedBox(width: 16),
                            Expanded(
                                child: Text(
                              widget.services.store.groupMembersList[index]
                                  .groupMember.user!.fullName!,
                              style:
                                  const TextStyle(fontWeight: FontWeight.w600),
                            )),
                            const SizedBox(width: 16),
                            widget.services.store.groupMemberDeleteMode
                                ? widget.services.store.groupMembersList[index]
                                            .groupMember.type !=
                                        "2"
                                    ? Checkbox(
                                        value: widget.services.store
                                            .groupMembersList[index].checked,
                                        onChanged: (v) {
                                          widget
                                              .services
                                              .store
                                              .groupMembersList[index]
                                              .checked = v!;
                                          widget.services.store
                                                  .groupMembersList =
                                              widget.services.store
                                                  .groupMembersList;
                                        },
                                        fillColor: MaterialStateProperty.all(
                                            Theme.of(context).primaryColor),
                                      )
                                    : const SizedBox()
                                : const SizedBox(),
                            widget.services.store.groupMembersList[index]
                                        .groupMember.type ==
                                    "2"
                                ? Text("Admin",
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor))
                                : Container(),
                          ],
                        );
                      }),
                ),
              );
  }

  void _onPressedDeleteIcon() {
    debugPrint('pressed delete icon');
    if (widget.services.store.groupMembersList.length == 1 &&
        widget.services.store.groupMembersList.first.groupMember.type == "2") {
      HttpExceptionNotifyUser.showError(
          "Other than admin, No members to remove");
    } else {
      setupDeleteModeView(true);
    }
  }

  void _onPressedDeleteButton() {
    List<String> userIds = [];
    for (var element in widget.services.store.groupMembersList) {
      if (element.checked) {
        userIds.add(element.groupMember.userId!);
      }
    }
    String userIdString = '';
    if (userIds.isNotEmpty) {
      userIdString = userIds.join(',');
    }
    if (userIdString == '') {
      HttpExceptionNotifyUser.showError("Select atleast one member to remove");
    } else {
      widget.services.store.removeMembersFromGroup(
          userIdList: userIdString, groupId: widget.params!.groupId);
    }
  }
}
