// ignore_for_file: implementation_imports

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/generated/l10n.dart';
import 'package:happy_place/ui/widgets/app_button.dart';
import 'package:happy_place/ui/widgets/app_edit_text.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:injectable/injectable.dart';

import 'create_edit_group_page.dart';

class BlockOrReportParams {
  const BlockOrReportParams({
    this.key,
    required this.groupId,
  });
  final Key? key;
  final int groupId;
}

@injectable
class BlockOrReportPage extends StatefulWidget {
  final GroupServices services;

  final BlockOrReportParams? params;

  BlockOrReportPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<BlockOrReportPage> createState() => _BlockOrReportPageState();
}

class _BlockOrReportPageState extends State<BlockOrReportPage> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  DropdownItems? _currentSelectedValue;

  List<DropdownItems> dropdownItemList = [
    DropdownItems(id: 1, title: "Harassment or bullying"),
    DropdownItems(id: 2, title: "Hateful or abusive content"),
    DropdownItems(id: 3, title: "Violent or repulsive content"),
    DropdownItems(id: 4, title: "Harmful or dangerous acts"),
    DropdownItems(id: 5, title: "Sexual content"),
    DropdownItems(id: 6, title: "Child abuse")
  ];

  final TextEditingController _messageTextController = TextEditingController();

  @override
  void initState() {
    widget.services.store.groupBlockValue = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Icon(Icons.close),
        ),
        title: const Text('Block or Report'),
        centerTitle: true,
      ),
      body: Observer(builder: (context) {
        if (widget.services.store.isLoading) {
          return const Loader();
        }
        return Container(
          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                const SizedBox(height: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "Select a Reason",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: 2),
                    DropdownButtonFormField<DropdownItems>(
                      icon: const Icon(Icons.keyboard_arrow_down,
                          size: 30, color: Colors.grey),
                      isDense: true,
                      items: dropdownItemList.map((DropdownItems entityType) {
                        return DropdownMenuItem<DropdownItems>(
                          value: entityType,
                          child: Text(entityType.title),
                        );
                      }).toList(),
                      onChanged: (DropdownItems? newValue) {
                        _currentSelectedValue = newValue!;
                      },
                      value: _currentSelectedValue,
                      decoration: const InputDecoration(
                        // filled: true,
                        border: OutlineInputBorder(),
                        fillColor: Colors.white,
                        hintText: 'Select a Reason',
                        hintStyle: TextStyle(color: Colors.grey),
                        contentPadding: EdgeInsets.only(
                            left: 20, right: 10, top: 15, bottom: 15),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 15),
                AppEditText(
                    textController: _messageTextController,
                    headingText: 'Message',
                    hint: "",
                    isMultiLine: true,
                    filled: false,
                    validator: (value) {
                      if (value == null || value.trim().isEmpty) {
                        return S.of(context).enter_post_caption;
                      } else {
                        return null;
                      }
                    },
                    borderColor: Colors.grey.shade400,
                    radius: 2),
                const SizedBox(height: 16),
                _blocUser(context),
                const SizedBox(height: 24),
                AppButton(
                    title: widget.services.store.groupBlockValue
                        ? 'Report and Block'
                        : 'Report',
                    onPressed: () {
                      String _reason = "";
                      if (_currentSelectedValue != null) {
                        _reason = _currentSelectedValue!.title;
                      }
                      widget.services.store.reportGroup(
                        groupId: widget.params!.groupId,
                        reason: _reason,
                        message: _messageTextController.text,
                      );
                    }),
              ],
            ),
          ),
        );
      }),
    );
  }

  Widget _blocUser(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Row(
            children: [
              SizedBox(
                height: 32,
                width: 32,
                child: Checkbox(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  value: widget.services.store.groupBlockValue,
                  onChanged: (value) {
                    widget.services.store.groupBlockValue = value!;
                  },
                  fillColor:
                      MaterialStateProperty.all(Theme.of(context).primaryColor),
                ),
              ),
              const SizedBox(width: 4),
              const Text('Block this group'),
            ],
          ),
        ),
      ],
    );
  }

  void onPressedReportButton() {}
}

class DropdownItems {
  DropdownItems({
    required this.id,
    required this.title,
  });
  int id;
  String title;
}
