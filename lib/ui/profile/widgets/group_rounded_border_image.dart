import 'dart:io' as io;

import 'package:flutter/material.dart';
// ignore: implementation_imports
import 'package:flutter/src/widgets/image.dart' as image;

class GroupRoundedBordeImage extends StatelessWidget {
  final double size;
  final double borderRadius;
  final Color borderColor;
  final double borderWidth;
  final String? profileUrl;
  final double defaultIconSize;
  final bool isPickedImage;
  const GroupRoundedBordeImage({
    Key? key,
    required this.size,
    required this.borderRadius,
    this.borderColor = Colors.white,
    this.borderWidth = 4.0,
    required this.profileUrl,
    required this.defaultIconSize,
    required this.isPickedImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(2.0),
      height: size,
      width: size,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(borderRadius),
        border: Border.all(color: borderColor, width: borderWidth),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(borderRadius - 4),
        child: profileUrl == null
            ? Container(
                color: Theme.of(context).primaryColor.withOpacity(0.2),
                child: Icon(
                  Icons.group,
                  color: Theme.of(context).primaryColor,
                  size: defaultIconSize,
                ),
              )
            : isPickedImage
                ? image.Image.file(
                    io.File(
                      profileUrl!,
                    ),
                    fit: BoxFit.cover,
                  )
                : Image.network(
                    profileUrl!,
                    fit: BoxFit.cover,
                    loadingBuilder: (BuildContext context, Widget child,
                        ImageChunkEvent? loadingProgress) {
                      if (loadingProgress == null) return child;
                      return Center(
                        child: CircularProgressIndicator(
                          value: loadingProgress.expectedTotalBytes != null
                              ? loadingProgress.cumulativeBytesLoaded /
                                  loadingProgress.expectedTotalBytes!
                              : null,
                        ),
                      );
                    },
                    errorBuilder:
                        (BuildContext context, Object exception, stackTrace) {
                      return Container(
                        color: Theme.of(context).primaryColor.withOpacity(0.2),
                        child: Icon(
                          Icons.group,
                          color: Theme.of(context).primaryColor,
                          size: defaultIconSize,
                        ),
                      );
                    },
                  ),
      ),
    );
  }
}
