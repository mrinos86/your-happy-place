import 'package:flutter/material.dart';

class SearchBar extends StatelessWidget {
  final TextEditingController textController;
  final Function searchCallBackFunction;
  const SearchBar({
    Key? key,
    required this.textController,
    required this.searchCallBackFunction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 35,
      child: TextFormField(
        onChanged: (s) {
          searchCallBackFunction(s);
        },
        controller: textController,
        style: Theme.of(context).textTheme.bodyText1?.copyWith(
              color: Colors.black,
              fontWeight: FontWeight.normal,
              fontSize: Theme.of(context).textTheme.bodyText2?.fontSize,
            ),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.zero,
          fillColor: Colors.white,
          focusColor: Colors.white,
          // isDense: true,
          prefixIcon: const Icon(Icons.search),
          hintText: 'Search',
          hintStyle: Theme.of(context).textTheme.bodyText1?.copyWith(
                color: Colors.grey,
              ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(24.0),
            borderSide: BorderSide(
              color: Colors.grey.shade100,
            ),
          ),
        ),
      ),
    );
  }
}
