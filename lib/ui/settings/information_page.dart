import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:url_launcher/url_launcher.dart';

class InformationPageParams {
  InformationPageParams({
    this.key,
    required this.type,
  });
  final Key? key;
  final double type;
  // types
  // 0: explore info
  // 1: my happines info
  // 1.1: Basic needs
  // 1.2: Love and Belonging
  // 1.3: Self_esteem
  // 1.4: Beauty and Culture
  // 1.5: Meaning of life
  // 2: hexaco result info
  // 2.1: hexaco test info
}

@injectable
class InformationPage extends StatefulWidget {
  final InformationPageParams? params;
  InformationPage({
    @factoryParam this.params,
  }) : super(key: params?.key);

  @override
  State<InformationPage> createState() => _InformationPageState();
}

class _InformationPageState extends State<InformationPage> {
  List<InfoDetailsModel> infoDisplayList = [];

  final List<InfoDetailsModel> myHappinessList = [
    InfoDetailsModel(
        info: "",
        title: "My Happiness",
        detail:
            "The daily Happiness record collects a series of snapshots of your general level of happiness. The results can also be seen as a running record of your overall and particular areas of general wellbeing. Happiness and Wellbeing are often seen as interchangeable. Wellbeing is often used to indicate a person’s pervading state of contentment, and happiness as a more moment by moment sense of fulfillment.\nIn this app we have modified questions that have been associated with Abraham Maslow’s Theory of Human Motivation and other theories surrounding happiness and wellbeing. These have been distilled to create a set of questions which create a record of your perceptions of how well your Basic Needs, Love and Belonging, Self-esteem, Beauty and Culture and Meaning of life are trending in your life.\nAll these measures are important areas of your life and are known to be essential to your sense of happiness and wellbeing. They are a way for you to understand your needs for self-fulfilment as well as identify areas in your life that could improve your sense of happiness and wellbeing.\nIf you want to know more about these measures take a look at their individual descriptions."),
  ];

  final List<InfoDetailsModel> myHappyLoveList = [
    InfoDetailsModel(
        info: "",
        title: "Love and Belonging",
        detail:
            "Selecting the 'Love and Belonging' option in 'My Happiness Results' will bring up a chart of your recent responses to the questions you were asked when you made a 'Happiness' entry. It will also show you the last responses you made to each individual question. As social beings, a sense of belonging and importance to significant others, groups or communities is an important contributor to our sense of happiness. Some personality traits such as extraversion and introversion may vary in the way they relate, but everyone benefits from good personal and social relationships with others. Feeling loved and being a part of others' lives, even on a casual social basis, makes us feel like we belong. Relationships have to be nurtured or they will wither, so pay attention to your relationships. Take time to let others know you care, pay forward by helping others, even if it is anonymous. Be a volunteer, join groups. Take time to listen, thank people who have helped you. Arrange to spend time with the people you care for. Try to smile even when you don't feel like it - a smile is infectious and makes others feel good as well.")
  ];

  final List<InfoDetailsModel> myHappySelfList = [
    InfoDetailsModel(
        info: "",
        title: "Self-esteem",
        detail:
            "Selecting the 'Self Esteem' option in 'My Happiness Results' will bring up a chart of your recent responses to the questions you were asked when you made a 'Happiness' entry. It will also show you the last responses you made to each individual question. Self-esteem has been identified by many people as important in the creation of happiness. Feeling valued by those around you at home, work or other places in your life allows you to create positive thoughts about yourself. This goes hand-in-hand with accepting yourself. Self-esteem is not reliant on money, possessions or status but on your relationship with yourself.")
  ];

  final List<InfoDetailsModel> myHappyBasicList = [
    InfoDetailsModel(
        info: "",
        title: "Basic Needs",
        detail:
            "Selecting the 'Basic Needs' option in 'My Happiness Results' will bring up a chart of your recent responses to the questions you were asked when you made a 'Happiness' entry. It will also show you the last responses you made to each individual question.\nEach of the measures of your happiness can be important for your motivation to improve your wellbeing and happiness; probably none more so than meeting your basic needs for physical and mental wellbeing, a sense of security and your overall standard of living. Your responses are designed to help you identify areas where you can best concentrate to improve your happiness. Once your basic needs have been met you can afford to concentrate on the next goals in the quest for happiness."),
  ];

  final List<InfoDetailsModel> myHappyBeautyList = [
    InfoDetailsModel(
        info: "",
        title: "Beauty and Culture",
        detail:
            "Selecting the 'Beauty and Culture' option in 'My Happiness Results' will bring up a chart of your recent responses to the questions you were asked when you made a 'Happiness' entry. It will also show you the last responses you made to each individual question. We all see beauty differently but there is no doubt that where we live affects our emotions. Natural environments are known to make people happy. Their restorative effect allows us to relax and recover from the stresses of life. Sunsets, parks, beaches, hilltops, forests, lakes, rivers, tree-lined streets, homes and gardens can all contribute to our personal happiness. Equally, dirty, boring streets, cookie-cutter developments, soulless concrete towers and run-down places can create depressive environments. The activities in our lives can also have profound effects on our levels of happiness. These cultural activities include walking with others in pleasant places, arts and crafts, music, theatre, sports, sharing meals, hobbies and much more. Our activities can give us a sense of belonging and purpose in our lives.")
  ];

  final List<InfoDetailsModel> myHappyMeaningList = [
    InfoDetailsModel(
        info: "",
        title: "Meaning of Life",
        detail:
            "Selecting the 'Meaning of Life' option in 'My Happiness Results' will bring up a chart of your recent responses to the questions you were asked when you made a 'Happiness' entry. It will also show you the last responses you made to each individual question. Once we have met our basic needs and we feel loved, our self-esteem is secure and our environment makes us happy, we will often seek to find deeper meaning in our lives. You can meet all those needs but still be unhappy because your spiritual or intellectual life lacks meaning.")
  ];

  final List<InfoDetailsModel> hexacoResultList = [
    InfoDetailsModel(
        info: "",
        title: "HEXACO Test",
        detail:
            "The HEXACO descriptions of our personality differences can be used in many ways to predict how people with different traits are likely to behave. In this app we intend to use them to help you understand your preferences for activity and places. In future we will develop app features which will help you identify the preferred places of people similar to you.\nThese different personality traits make us who we are and we all contribute to society with our own unique combination of personality traits and abilities. Any particular trait will be useful in some circumstances, harmful in others and neutral or irrelevant most of the time. Your personality traits are largely inherited, but they evolve as you experience life and engage with different social, cultural and physical environments. Please keep in mind that being in different places on the trait spectrum is neither good nor bad. We are better together. and we evolve solutions to the many challenges of life socially."),
  ];

  final List<InfoDetailsModel> hexacoTestList = [
    InfoDetailsModel(
      info: "The HEXACO personality traits can briefly be described as:",
      title: "Honesty (Style)",
      detail:
          "This is the dimension of your personality that relates to your moral compass. If you are high in this trait, you are likely to promote other peoples interests over your own. You will be humble, sincere and modest. At the other end of the spectrum, this trait is associated with being self-promoting and confident, and others are likely to see you as confident, bold or arrogant.",
    ),
    InfoDetailsModel(
      info: "",
      title: "Emotionality (Stability)",
      detail:
          "Everyone has a degree of sensitivity to social, cultural and physical environmental challenges. People differ in how they respond to those challenges. Sensitive people are likely to experience negative feelings such as anger, anxiety and depression more often than others. Those who are perceived as stable will be thought of as calm, emotionally detached and less prone to negative feeling.",
    ),
    InfoDetailsModel(
      info: "",
      title: "Extraversion (Relational)",
      detail:
          "If you are at one end of this spectrum, you are an Extrovert and likely to be seen as someone who enjoys being with other people, talkative, assertive, cheerful, enthusiastic, energetic. If you are at the opposite end, you possess the Introversion personality trait. Other people will perceive you as thoughtful, low key, quiet and less involved in social situations. If you are someone who has a balance of these traits, you are an Ambivert.",
    ),
    InfoDetailsModel(
      info: "",
      title: "Agreeableness (Friendliness)",
      detail:
          "If you have a high degree of Agreeableness, you have a desire for social harmony and getting along with others. Agreeable individuals are seen as friendly, optimistic, kind, generous, considerate, trusting and helpful. The other end of the scale is associated with being perceived as unfriendly, sceptical, tough talking, straightforward and more concerned with being objective than being popular.",
    ),
    InfoDetailsModel(
      info: "",
      title: "Conscientiousness (Focus)",
      detail:
          "Highly Conscientious people are likely to be perceived as focused on tasks, dependable, organised, careful, orderly, disciplined, purposeful and persistent. People rely on you to get things done. At the other end of the scale, you are seen as less focused, easy going, laid back, careless, impulsive, disorganised, playful and care free.",
    ),
    InfoDetailsModel(
      info: "",
      title: "Openness (Interests)",
      detail:
          "This dimension is known as the creative personality trait and indicates the level of interest in the beauty of art and nature. One end of the spectrum is associated with being imaginative, adventurous, curious, individualistic, self-aware, sensitive to beauty and curious. The other end is associated with being less curious or interested in creative pursuits. You are likely to be seen as straightforward, consistent, pragmatic, factual and consistent.",
    )
  ];

  void _onPressedUrl() async {
    final Uri params =
        Uri(scheme: 'mailto', path: 'help@personalityandplace.com', query: '');

    var url = params.toString();
    if (!await launchUrl(params)) throw 'Could not launch $url';
  }

  @override
  void initState() {
    setState(() {
      if (widget.params!.type == 1) {
        infoDisplayList = myHappinessList;
      } else if (widget.params!.type == 1.1) {
        infoDisplayList = myHappyBasicList;
      } else if (widget.params!.type == 1.2) {
        infoDisplayList = myHappyLoveList;
      } else if (widget.params!.type == 1.3) {
        infoDisplayList = myHappySelfList;
      } else if (widget.params!.type == 1.4) {
        infoDisplayList = myHappyBeautyList;
      } else if (widget.params!.type == 1.5) {
        infoDisplayList = myHappyMeaningList;
      } else if (widget.params!.type == 2) {
        infoDisplayList = hexacoResultList;
      } else if (widget.params!.type == 2.1) {
        infoDisplayList = hexacoTestList;
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        title: const Text('Information'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 20,
            right: 20,
            top: 25,
          ),
          child:
              widget.params!.type == 0 ? expolreInfo() : happinessInfoWidget(),
        ),
      ),
    );
  }

  Widget happinessInfoWidget() {
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: infoDisplayList.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              infoDisplayList[index].info != ""
                  ? Column(
                      children: [
                        Text(
                          infoDisplayList[index].info,
                        ),
                        const SizedBox(height: 20),
                      ],
                    )
                  : Container(),
              infoDisplayList[index].title != ""
                  ? Text(infoDisplayList[index].title,
                      style: const TextStyle(fontWeight: FontWeight.bold))
                  : Container(),
              const SizedBox(height: 10),
              infoDisplayList[index].detail != ""
                  ? Text(
                      infoDisplayList[index].detail,
                    )
                  : Container(),
              const SizedBox(height: 20),
            ],
          );
        });
  }

  Widget expolreInfo() {
    return Column(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            Text(
              "Explore Function Text",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            Text(
              "The 'Explore' function gives you a way of reviewing the Mood, Activity and Place entries made by you, family, friends and the public. You can search your neighbourhood town, city or the world for the places in your life that affect your happiness and wellbeing. Find out how your family and friends value different places and help you decide where you want to go. Through using the app, you can start to build a picture of the emotional landscape in which you, your family and friends and other people live. You will identify the boring, comfortable, exciting, happy, pleasant, quiet, unsafe, unpleasant or worrying places in your life. And you can check how often you visit them and how well those places met your needs. Using Your Happy Place can help you find those places in your life which suit your personality and make you feel good. Knowing your places and how you use them can help you to avoid some and visit others more often. In turn, this can improve your well-being and encourage an active healthy lifestyle.",
            ),
          ],
        ),
        const SizedBox(height: 20),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            Text(
              "Exploring the Map",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            Text(
                "The Explore map opens on your current location. It shows all the entries you and others family, friends and the public have made. You can also pan, zoom and search for the places you are interested in. Tapping on icons shows you who made the entries and when the were made."),
          ],
        ),
        const SizedBox(height: 20),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "The Filter",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 8),
            RichText(
              text: TextSpan(
                children: [
                  const TextSpan(
                    text:
                        "Selecting the 'Filter' feature allows you to refine your search. You can select all entries, or just yours, family or friends. You can also search by selecting specific moods, activities or places. As we develop the app, extra search feature will be added to improve your user experience. You can help us prioritise those feature and tell us what you would like to see by sending us a message ",
                    style: TextStyle(color: Colors.black),
                  ),
                  TextSpan(
                    text: 'help@personalityandplace.com',
                    style: const TextStyle(color: Colors.deepPurpleAccent),
                    recognizer: TapGestureRecognizer()..onTap = _onPressedUrl,
                  ),
                ],
              ),
            ),
          ],
        ),
        const SizedBox(height: 20),
      ],
    );
  }
}

class InfoDetailsModel {
  InfoDetailsModel({
    required this.info,
    required this.title,
    required this.detail,
  });
  final String info;
  final String title;
  final String detail;
}
