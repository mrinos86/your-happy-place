import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/data/stores/notification_store.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';

class NotificationPageParams {
  NotificationPageParams({
    this.key,
  });
  final Key? key;
}

@injectable
class NotificationPageServices {
  NotificationStore store;
  NotificationPageServices({
    required this.store,
  });
}

@injectable
class NotificationPage extends StatefulWidget {
  final NotificationPageServices services;
  final NotificationPageParams? params;
  NotificationPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  void initState() {
    widget.services.store.getNotifications();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Padding(
        padding: const EdgeInsets.only(
          left: 24,
          right: 24,
          top: 32,
        ),
        child: Observer(builder: (context) {
          if (widget.services.store.isLoading) {
            return const Loader();
          }
          return ListView.separated(
            separatorBuilder: (context, index) {
              return const SizedBox(height: 12);
            },
            itemCount: widget.services.store.notificationsList!.length,
            itemBuilder: (context, index) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(width: 1, color: Colors.grey.shade300),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.services.store.notificationsList![index].title!,
                        style: Theme.of(context).textTheme.bodyText1?.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                      const SizedBox(height: 5),
                      Text(
                        widget
                            .services.store.notificationsList![index].message!,
                        style:
                            Theme.of(context).textTheme.bodyText1?.copyWith(),
                      ),
                      const SizedBox(height: 15),
                      Row(children: [
                        Text(
                          getFormatDateTime(widget.services.store
                                  .notificationsList![index].sentAt ??
                              ""),
                          style:
                              Theme.of(context).textTheme.bodyText2?.copyWith(
                                    color: Colors.grey.shade500,
                                  ),
                        ),
                      ]),
                    ],
                  ),
                ),
              );
            },
          );
        }),
      ),
    );
  }

  static String getFormatDateTime(String dateTime) {
    String date = "";

    DateTime now = DateTime.now().toLocal();
    DateTime today = DateTime(now.year, now.month, now.day);

    DateTime receivedDate = DateTime.parse(dateTime).toLocal();
    receivedDate =
        DateTime(receivedDate.year, receivedDate.month, receivedDate.day);

    if (today == receivedDate) {
      date = "Today    " +
          DateFormat('hh:mm a').format(DateTime.parse(dateTime).toLocal());
    } else {
      date = DateFormat('yyyy-MM-dd    hh:mm a')
          .format(DateTime.parse(dateTime).toLocal());
    }
    return date;
  }
}
