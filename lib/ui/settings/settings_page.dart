import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:happy_place/data/stores/settings_store.dart';
import 'package:happy_place/generated/l10n.dart';
import 'package:happy_place/ui/widgets/loader.dart';
import 'package:injectable/injectable.dart';

import '../widgets/check_box.dart';

class SettingsPageParams {
  SettingsPageParams({
    this.key,
  });
  final Key? key;
}

@injectable
class SettingsPageServices {
  SettingsStore store;
  SettingsPageServices({
    required this.store,
  });
}

@injectable
class SettingsPage extends StatefulWidget {
  final SettingsPageServices services;
  final SettingsPageParams? params;
  SettingsPage({
    @factoryParam this.params,
    required this.services,
  }) : super(key: params?.key);

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool visibleFriend = false;
  bool visibleFamily = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 24,
            right: 24,
            top: 24,
          ),
          child: Observer(builder: (context) {
            if (widget.services.store.isLoading) {
              return const Loader();
            }
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Privacy',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.grey),
                ),
                const SizedBox(height: 8),
                Text(
                  'Your pins are private unless you choose to tag with your username.',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(height: 16),
                _visibleUserName(context),
                const SizedBox(height: 25),
                Text(
                  'Account',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.grey),
                ),
                const SizedBox(height: 8),
                SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: ElevatedButton(
                      style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24),
                          )),
                          foregroundColor:
                              MaterialStateProperty.all(Colors.red),
                          backgroundColor:
                              MaterialStateProperty.all(Colors.red)),
                      onPressed: () {
                        showDeleteAlertDialog(context);
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 0),
                        child: Text(
                          S.of(context).deleteAccount,
                          style:
                              Theme.of(context).textTheme.bodyText1?.copyWith(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                        ),
                      ),
                    )),
                const SizedBox(height: 16),
              ],
            );
          }),
        ),
      ),
    );
  }

  void showDeleteAlertDialog(BuildContext context) {
    Widget okButton = Padding(
      padding: const EdgeInsets.only(left: 64.5, right: 63.5, bottom: 24.5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: 36,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Theme.of(context).primaryColor.withOpacity(0.2),
                  blurRadius: 10.0,
                  spreadRadius: 0.0, //extend the shadow
                  offset: const Offset(
                    0.0, // Move to right 10  horizontally
                    4, // Move to bottom 10 Vertically
                  ),
                ),
              ],
            ),
            child: TextButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(Theme.of(context).primaryColor),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(9),
                  ),
                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                S.of(context).cancel,
                style: const TextStyle(color: Colors.white),
              ),
            ),
          ),
          Center(
            child: TextButton(
              onPressed: () async {
                Navigator.pop(context, true);
              },
              child: Text(
                S.of(context).delete,
              ),
            ),
          ),
        ],
      ),
    );
    var alert = AlertDialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
      title: Text(
        S.of(context).deleteAccount,
        textAlign: TextAlign.center,
      ),
      content: Text(
        S.of(context).deleteAlertMessage,
        textAlign: TextAlign.center,
      ),
      actions: [okButton],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        }).then((value) async {
      debugPrint('value: $value');
      if (value ?? false) {
        widget.services.store.deleteProfile();
        debugPrint("deleting accout");
      }
    });
  }

  Widget _visibleUserName(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        CheckBoxWidget(
          value: widget.services.store.visibleUNameAll,
          onChangedCallback: (i) {
            widget.services.store.visibleUNameAll = i;
            // if (i) {
            //   widget.services.store.visibleUNameFamily = true;
            //   widget.services.store.visibleUNameFriends = true;
            // }
            widget.services.store.updateSettings();
          },
          text: 'Make username visible on location pins',
        ),
        CheckBoxWidget(
          value: widget.services.store.visibleUNameFamily,
          onChangedCallback: (i) {
            widget.services.store.visibleUNameFamily = i;
            widget.services.store.updateSettings();
          },
          text: 'Visible user name for family',
        ),
        CheckBoxWidget(
          value: widget.services.store.visibleUNameFriends,
          onChangedCallback: (i) {
            widget.services.store.visibleUNameFriends = i;
            widget.services.store.updateSettings();
          },
          text: 'Visible user name for friends',
        ),
      ],
    );
  }
}
