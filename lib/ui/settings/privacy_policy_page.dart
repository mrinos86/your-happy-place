import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:injectable/injectable.dart';

class PrivacyPolicyPageParams {
  PrivacyPolicyPageParams({this.key, required this.isFromNav});
  final Key? key;
  final bool isFromNav;
}

@injectable
class PrivacyPolicyPage extends StatefulWidget {
  final PrivacyPolicyPageParams? params;

  PrivacyPolicyPage({@factoryParam this.params}) : super(key: params?.key);

  @override
  State<PrivacyPolicyPage> createState() => _PrivacyPolicyPageState();
}

class _PrivacyPolicyPageState extends State<PrivacyPolicyPage> {
  List<PrivacyPolicy> privacyDataList = [];

  @override
  void initState() {
    super.initState();
    _loadPrivacyData();
  }

  Future<void> _loadPrivacyData() async {
    String jsonString =
        await rootBundle.loadString("assets/data/privacy_policy.json");
    final jsonResponse = json.decode(jsonString);
    setState(() {
      privacyDataList = getPrivacyData(jsonResponse);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: !widget.params!.isFromNav
          ? AppBar(
              title: const Text('Privacy Policy'),
              centerTitle: true,
            )
          : null,
      body: SingleChildScrollView(
        child: Container(
          color: Theme.of(context).scaffoldBackgroundColor,
          child: Padding(
            padding: const EdgeInsets.all(19.0),
            child: ListView.builder(
                shrinkWrap: true,
                physics: const ScrollPhysics(),
                itemCount: privacyDataList.length,
                itemBuilder: (context, i) {
                  return Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            privacyDataList[i].number,
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(width: 3),
                          Text(
                            privacyDataList[i].title,
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 10, 10, 0),
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: const ScrollPhysics(),
                            itemCount: privacyDataList[i].subText.length,
                            itemBuilder: (context, x) {
                              return Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text:
                                              privacyDataList[i].subText[x].no,
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        privacyDataList[i].subText[x].no == ""
                                            ? const WidgetSpan(
                                                child: SizedBox())
                                            : const WidgetSpan(
                                                child: SizedBox(width: 10)),
                                        TextSpan(
                                            text: privacyDataList[i]
                                                .subText[x]
                                                .details),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(height: 10)
                                ],
                              );
                              // return Column(
                              //   crossAxisAlignment: CrossAxisAlignment.start,
                              //   children: [
                              //     Text(
                              //       privacyDataList[i].subText[x].no,
                              //       style: const TextStyle(
                              //           fontWeight: FontWeight.bold),
                              //     ),
                              //     Text(privacyDataList[i].subText[x].details),
                              //     const SizedBox(height: 10),
                              //   ],
                              // );
                            }),
                      ),
                      const SizedBox(height: 15),
                    ],
                  );
                }),
          ),
        ),
      ),
    );
  }
}

class PrivacyPolicy {
  String number;
  String title;
  List<SubText> subText;
  PrivacyPolicy(
      {required this.number, required this.subText, required this.title});
}

class SubText {
  String no;
  String details;
  SubText({required this.no, required this.details});
}

List<PrivacyPolicy> getPrivacyData(data) {
  List<PrivacyPolicy> _privacydata = [];
  for (var i in data) {
    _privacydata.add(
      PrivacyPolicy(
          number: i["number"],
          subText: getSubTextData(i["subText"]),
          title: i["title"]),
    );
  }
  return _privacydata;
}

List<SubText> getSubTextData(data) {
  List<SubText> _subData = [];
  for (var i in data) {
    _subData.add(
      SubText(no: i["no"], details: i["details"]),
    );
  }
  return _subData;
}
