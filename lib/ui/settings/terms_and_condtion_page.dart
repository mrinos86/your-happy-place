import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:happy_place/ui/settings/privacy_policy_page.dart';
import 'package:injectable/injectable.dart';

class TermsAndCondtionPageParams {
  TermsAndCondtionPageParams({
    this.key,
    required this.isFromNav,
  });
  final Key? key;
  final bool isFromNav;
}

@injectable
class TermsAndCondtionPage extends StatefulWidget {
  final TermsAndCondtionPageParams? param;
  TermsAndCondtionPage({@factoryParam this.param}) : super(key: param?.key);

  @override
  State<TermsAndCondtionPage> createState() => _TermsAndCondtionPageState();
}

class _TermsAndCondtionPageState extends State<TermsAndCondtionPage> {
  List<PrivacyPolicy> termsDataList = [];

  @override
  void initState() {
    super.initState();
    _loadPrivacyData();
  }

  Future<void> _loadPrivacyData() async {
    String jsonString =
        await rootBundle.loadString("assets/data/term_and_condition.json");
    final jsonResponse = json.decode(jsonString);
    setState(() {
      termsDataList = getPrivacyData(jsonResponse);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: !widget.param!.isFromNav
          ? AppBar(
              title: const Text('Terms & Conditions'),
              centerTitle: true,
            )
          : null,
      body: SingleChildScrollView(
        child: Container(
          color: Theme.of(context).scaffoldBackgroundColor,
          child: Padding(
            padding: const EdgeInsets.all(19.0),
            child: ListView.builder(
                shrinkWrap: true,
                physics: const ScrollPhysics(),
                itemCount: termsDataList.length,
                itemBuilder: (context, i) {
                  return Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            termsDataList[i].number,
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(width: 3),
                          Text(
                            termsDataList[i].title,
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 10, 10, 0),
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: const ScrollPhysics(),
                            itemCount: termsDataList[i].subText.length,
                            itemBuilder: (context, x) {
                              return Column(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text: termsDataList[i].subText[x].no,
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        termsDataList[i].subText[x].no == ""
                                            ? const WidgetSpan(
                                                child: SizedBox())
                                            : const WidgetSpan(
                                                child: SizedBox(width: 10)),
                                        TextSpan(
                                            text: termsDataList[i]
                                                .subText[x]
                                                .details),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(height: 10)
                                ],
                              );
                              // return Column(
                              //   crossAxisAlignment: CrossAxisAlignment.start,
                              //   children: [
                              //     termsDataList[i].subText[x].no == ""
                              //         ? Container()
                              //         : Text(
                              //             termsDataList[i].subText[x].no,
                              //             style: const TextStyle(
                              //                 fontWeight: FontWeight.bold),
                              //           ),
                              //     Text(termsDataList[i].subText[x].details),
                              //     const SizedBox(height: 10),
                              //   ],
                              // );
                            }),
                      ),
                      const SizedBox(height: 15),
                    ],
                  );
                }),
          ),
        ),
      ),
    );
  }
}
