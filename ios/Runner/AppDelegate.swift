 import UIKit
 import Flutter
 import GoogleMaps
 import Firebase
 import FirebaseMessaging

 @UIApplicationMain
 @objc class AppDelegate: FlutterAppDelegate {
   override func application(
     _ application: UIApplication,
     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
   ) -> Bool {
     FirebaseApp.configure()
     GMSServices.provideAPIKey("AIzaSyDcPcvMga2LhgQqGW6S2zfFj3ZCEfvP05I")

       if(!UserDefaults.standard.bool(forKey: "Notification")) {
               UIApplication.shared.cancelAllLocalNotifications()
               UserDefaults.standard.set(true, forKey: "Notification")
           }
       if #available(iOS 10.0, *) {
         UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
       }
       application.registerForRemoteNotifications()

     GeneratedPluginRegistrant.register(with: self)
     return super.application(application, didFinishLaunchingWithOptions: launchOptions)
   }
     override func application(_ application: UIApplication,
      didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

       Messaging.messaging().apnsToken = deviceToken
       super.application(application,
       didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
     }
 }